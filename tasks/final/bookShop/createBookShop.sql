CREATE DATABASE `book_shop` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `book_shop`;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `photo` blob,
  `order_id` int(11) DEFAULT NULL,
  `book_category` varchar(45) NOT NULL,
  `publication_year` int(4) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `number_of_pages` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idbook_UNIQUE` (`id`),
  KEY `fk_book_order1_idx` (`order_id`),
  CONSTRAINT `fk_book_order1` FOREIGN KEY (`order_id`) REFERENCES `book_order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `book_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `user_account_id` int(11) NOT NULL,
  `order_info` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_order_user_account1_idx` (`user_account_id`),
  CONSTRAINT `fk_order_user_account1` FOREIGN KEY (`user_account_id`) REFERENCES `user_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_info_id` int(11) NOT NULL,
  `role` enum('user','admin','visitor') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iduser_account_UNIQUE` (`id`),
  KEY `fk_user_account_user_info1_idx` (`user_info_id`),
  CONSTRAINT `fk_user_account_user_info1` FOREIGN KEY (`user_info_id`) REFERENCES `user_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(45) NOT NULL,
  `address` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iduser_info_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;





