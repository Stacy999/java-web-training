<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "language=" + name;
        window.location.reload(true);
    }
</script>

<fmt:setLocale value="${requestScope.get('language')}"/>
<fmt:setBundle basename="/localization" scope="application"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><fmt:message key="layout.title"/></title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a href="welcome_command" class="navbar-brand"><fmt:message key="layout.menu.title"/> </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="basicExampleNav">
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a href="view_all_books_command" class="nav-link waves-effect waves-light">
                            <fmt:message key="layout.menu.catalog"/>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#best-features" class="nav-link waves-effect waves-light">
                            <fmt:message key="layout.menu.news"/>
                        </a>
                    </li>
                    <c:set var="admin">admin</c:set>
                    <c:if test="${sessionScope.current_user.role == admin}">
                        <li class="nav-item">
                            <a href="view_book_addition_form_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.add-new-book"/>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="view_all_users_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.users"/>
                            </a>
                        </li>
                    </c:if>
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <c:choose>
                        <c:when test="${sessionScope.current_user == null}">
                            <li class="nav-item">
                                <a href="view_login_form_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-in"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_register_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.registration"/>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="nav-item">
                                <a href="logout_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-out"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_basket_command" class="nav-link waves-effect waves-light">
                                    <i class="fas fa-shopping-basket"></i>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                    <div class="list-group list-group-horizontal">
                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('en_US')"><fmt:message key="layout.lang.en"/></a>

                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('ru_RU')"><fmt:message key="layout.lang.ru"/></a>
                    </div>
                </ul>
            </div>
        </div>
    </nav>

    <div id="intro" class="view intro">
        <div class="mask rgba-black-strong">
            <div class="intro-preview container-fluid d-flex align-items-center justify-content-center h-100">
                <div class="row d-flex justify-content-center text-center">
                    <div class="col-md-10">
                        <h2 class="display-4 font-weight-bold white-text pt-5 mb-2">
                            <fmt:message key="layout.menu.title"/>
                        </h2>
                        <hr class="hr-light">
                        <h4 class="white-text my-4"><fmt:message key="layout.quote"/></h4>
                        <form method="get" action="view_all_books_command">
                            <button class="btn btn-outline-white waves-effect waves-light"
                                    value="view_all_books_command">
                                <fmt:message key="layout.catalog-button"/>
                                <i class="fa fa-book"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<main class="mt-5">
    <div class="container">
        <section id="best-features" class="text-center">
            <h2 class="mb-5 font-weigh-bold"><fmt:message key="layout.news"/></h2>
            <div class="row d-flex justify-content-center mb-4">
                <div class="col-md-8">
                    <p class="grey-text">
                        <fmt:message key="layout.news.value"/>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 md-5">
                    <i class="fas fa-snowman fa-4x grey-text"></i>
                    <h4 class="my-4 font-weigh-bold"><fmt:message key="layout.news.title1"/></h4>
                    <p class="grey-text">
                        <fmt:message key="layout.news.title1.value"/>
                    </p>
                </div>
                <div class="col-md-4 md-5">
                    <i class="fas fa-mitten fa-4x grey-text"></i>
                    <h4 class="my-4 font-weigh-bold"><fmt:message key="layout.news.title2"/></h4>
                    <p class="grey-text">
                        <fmt:message key="layout.news.title2.value"/>
                    </p>
                </div>
                <div class="col-md-4 md-5">
                    <i class="fas fa-gifts fa-4x grey-text"></i>
                    <h4 class="my-4 font-weigh-bold"><fmt:message key="layout.news.title3"/></h4>
                    <p class="grey-text">
                        <fmt:message key="layout.news.title3.value"/>
                    </p>
                </div>
            </div>
        </section>

        <hr class="my-5">

    </div>
</main>
<!-- End your project here-->

<!-- jQuery -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>
<!-- Your custom scripts (optional) -->
<script type="text/javascript"></script>

</body>
</html>