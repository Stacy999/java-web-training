<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "language=" + name;
        window.location.reload(true);
    }
</script>

<fmt:setLocale value="${requestScope.get('language')}"/>
<fmt:setBundle basename="/localization" scope="application"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Book shop</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="css/logInStyle.css">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a href="welcome_command" class="navbar-brand"><fmt:message key="layout.menu.title"/> </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="basicExampleNav">
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a href="view_all_books_command" class="nav-link waves-effect waves-light">
                            <fmt:message key="layout.menu.catalog"/>
                        </a>
                    </li>
                    <c:set var="admin">admin</c:set>
                    <c:if test="${sessionScope.current_user.role == admin}">
                        <li class="nav-item">
                            <a href="view_book_addition_form_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.add-new-book"/>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="view_all_users_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.users"/>
                            </a>
                        </li>
                    </c:if>
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <c:choose>
                        <c:when test="${sessionScope.current_user == null}">
                            <li class="nav-item">
                                <a href="view_login_form_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-in"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_register_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.registration"/>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="nav-item">
                                <a href="logout_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-out"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_basket_command" class="nav-link waves-effect waves-light">
                                    <i class="fas fa-shopping-basket"></i>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                    <div class="list-group list-group-horizontal">
                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('en_US')"><fmt:message key="layout.lang.en"/></a>

                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('ru_RU')"><fmt:message key="layout.lang.ru"/></a>
                    </div>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main class="mt-5">
    <div class="container">
        <div class="row justify-content-center">
        <div class="card card-container ">
            <i class="fa fa-user-circle " style="font-size: 170px; color: #BC8F8F!important;"></i>
        <form class="md-form text-center" action="registr_user_command">
            <fmt:message key="registration-form.title"/>
            <c:if test="${not empty new_user_input_errors}" >
                    <c:forEach items="${new_user_input_errors}" var="error">
                        <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                           <span>
                            <c:out value="${error}" />
                               <br>
                           </span>
                        </div>
                        </div>
                    </c:forEach>
            </c:if>
            <div class="md-form mt-3">
                <input type="text" id="UserName" class="form-control" name="user.firstName" required>
                <label for="UserName"><fmt:message key="registration-form.firstName"/> </label>
            </div>
            <div class="md-form mt-3">
                <input type="text" id="LastName" class="form-control" name="user.lastName" required>
                <label for="LastName"><fmt:message key="registration-form.lastName"/> </label>
            </div>

            <div class="md-form mt-3">
                <input type="text" id="Email" class="form-control" name="user.email" required
                       pattern="^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*.[a-z]{2,6}$">
                <label for="Email"><fmt:message key="registration-form.email"/> </label>
                <small id="emailHelpBlock" class="form-text text-muted">
                    Your email can contain only letters, must contain '@' and '.'characters.
                </small>
            </div>

            <div class="md-form mt-3">
                <input type="text" id="Phone" class="form-control" name="user.phone" required
                pattern="^((29)|(33)|(44)|(25))(([1-9]{1})([0-9]{6}))$">
                <label for="Phone"><fmt:message key="registration-form.phone"/> </label>
                <small id="phoneHelpBlock" class="form-text text-muted">
                    Your phone must be 9 characters long, only numbers, format +375 XXXXXXXXX
                </small>
            </div>

            <div class="md-form mt-3">
                <input type="text" id="Address" class="form-control" name="user.address" required>
                <label for="Address"><fmt:message key="registration-form.address"/></label>
            </div>

            <div class="md-form mt-3">
                <input type="text" id="Login" class="form-control" name="user.login" required
                pattern="^[a-zA-Z0-9]{4,16}$">
                <label for="Login"><fmt:message key="registration-form.login"/> </label>
                <small id="loginHelpBlock" class="form-text text-muted">
                    Your login must be 4-16 characters long, contain only letters and numbers.
                </small>
            </div>

            <div class="md-form mt-3">
                <input type="password" id="Password" class="form-control"name="user.password" required
                pattern="^[a-zA-Z0-9]{8,20}$">
                <label for="Password"><fmt:message key="registration-form.password"/> </label>
                <small id="passwordHelpBlock" class="form-text text-muted">
                    Your password must be 8-20 characters long, contain only letters and numbers.
                </small>
            </div>

            <button class="btn btn-lg btn-orange btn-block btn-sign-in" type="submit">
                <fmt:message key="registration-form.button"/>
            </button>
        </form>
        </div>
        </div>
    </div>

    <!-- jQuery -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Your custom scripts (optional) -->
    <script type="text/javascript"></script>
</main>
</body>
</html>