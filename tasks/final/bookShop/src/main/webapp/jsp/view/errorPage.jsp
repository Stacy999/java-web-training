<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "language=" + name;
        window.location.reload(true);
    }
</script>

<fmt:setLocale value="${requestScope.get('language')}"/>
<fmt:setBundle basename="/localization" scope="application"/>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Book shop</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="../../css/errorPage.css">
</head>

<body class="body-container">

        <div class="box">
            <i class="fa fa-frown-o f"></i>
            <h4><fmt:message key="error-page.title"/> </h4>
            <p><fmt:message key="error-page.msg"/> </p>
            <form class="" action="welcome_command" method="post">
                <button class="btn btn-into orange"><fmt:message key="error-page.button"/> </button>
            </form>
        </div>

</body>
<style>

    .body-container{
        display: flex;
        width: 100vw;
        height: 100vh;
        justify-content: center;
        align-items: center;
        background-color: orange;
    }

    .box{
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: 300px;
        height: 300px;
        background-color: white;
        border-radius: 7px;

    }
</style>
</html>
