<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "language=" + name;
        window.location.reload(true);
    }
</script>

<fmt:setLocale value="${requestScope.get('language')}"/>
<fmt:setBundle basename="/localization" scope="application"/>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Book shop</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="css/viewUsers.css">
</head>
<!-- Start your project here-->
<header>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a href="welcome_command" class="navbar-brand"><fmt:message key="layout.menu.title"/> </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="basicExampleNav">
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a href="view_all_books_command" class="nav-link waves-effect waves-light">
                            <fmt:message key="layout.menu.catalog"/>
                        </a>
                    </li>
                    <c:set var="admin">admin</c:set>
                    <c:if test="${sessionScope.current_user.role == admin}">
                        <li class="nav-item">
                            <a href="view_book_addition_form_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.add-new-book"/>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="view_all_users_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.users"/>
                            </a>
                        </li>
                    </c:if>
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <c:choose>
                        <c:when test="${sessionScope.current_user == null}">
                            <li class="nav-item">
                                <a href="view_login_form_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-in"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_register_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.registration"/>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="nav-item">
                                <a href="logout_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-out"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_basket_command" class="nav-link waves-effect waves-light">
                                    <i class="fas fa-shopping-basket"></i>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                    <div class="list-group list-group-horizontal">
                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('en_US')"><fmt:message key="layout.lang.en"/></a>

                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('ru_RU')"><fmt:message key="layout.lang.ru"/></a>
                    </div>
                </ul>
            </div>
        </div>
    </nav>

    <div id="intro" class="view intro">
        <div class="mask rgba-black-strong">
            <div class="container-fluid d-flex align-items-center justify-content-center h-100">
                <div class="row d-flex justify-content-center text-center">
                    <div class="col-md-10">
                        <h2 class="display-4 font-weight-bold white-text pt-5 mb-2">
                            Book Shop
                        </h2>
                        <hr class="hr-light">
                        <h4 class="white-text my-4">"No two persons ever read the same book..."</h4>
                        <form method="get" action="view_all_books_command">
                            <button class="btn btn-outline-white waves-effect waves-light"
                                    value="view_all_books_command">
                                Open catalog
                                <i class="fa fa-book"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<body>
<div class="container">
    <br>
    <br>
    <br>
    <form action="view_sorted_books_command" method="post">
        <input type="hidden" name="sorted_order" value="directOrder">
        <input class="dropdown-item" type="submit" value="A-Z"/>
    </form>
    <form action="view_sorted_books_command">
        <input type="hidden" name="sorted_order" value="reverseOrder">
        <input class="dropdown-item" type="submit" value="Z-A"/>
    </form>

    <div class="row justify-content-center">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--12-col">
                <c:choose>
                <c:when test="${not empty books}">

                <c:forEach items="${books}" var="b">
                <p>
                <div class="card" style="width: 18rem;">
                    <!--<img src="" class="card-img-top" alt="...">-->
                    <div class="card-body">
                        <h5 class="card-title">${b.name}</h5>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Author: ${b.author}</li>
                        <li class="list-group-item">Category: ${b.category}</li>
                        <li class="list-group-item">Number of pages: ${b.numberOfPages}</li>
                        <li class="list-group-item">Publication year: ${b.publicationYear}</li>
                        <li class="list-group-item">Description: ${b.description}</li>
                        <li class="list-group-item">Price: ${b.price} rub</li>
                    </ul>
                    <div class="card-body">
                        <c:if test="${sessionScope.current_user != null}">
                            <form action="add_book_to_basket_command" method="POST">
                                <input type="hidden" name="book.id" value="${b.id}"/>
                                <button class="btn btn-lg btn-orange btn-block btn-sign-in" type="submit">
                                    <fmt:message key="view-books.to-basket-btn"/>
                                </button>
                            </form>
                        </c:if>
                        <c:set var="admin">admin</c:set>
                        <c:if test="${sessionScope.current_user.role == admin}">
                            <form action="view_edit_book_command" method="POST">
                                <input type="hidden" name="book.id" value="${b.id}"/>
                                <button class="btn btn-lg btn-orange btn-block btn-sign-in" type="submit">
                                    <fmt:message key="view-books.edit-btn"/>
                                </button>
                            </form>
                            <form action="delete_book_command" method="POST">
                                <input type="hidden" name="book.id" value="${b.id}"/>
                                <button class="btn btn-lg btn-orange btn-block btn-sign-in" type="submit">
                                    <fmt:message key="view-books.delete-btn"/>
                                </button>
                            </form>
                        </c:if>
                    </div>
                </div>
                <p>
                    </c:forEach>
                    </c:when>
                    <c:otherwise>
                    no data available
                    </c:otherwise>
                    </c:choose>

            </div>
        </div>
    </div>
</div>
</body>

