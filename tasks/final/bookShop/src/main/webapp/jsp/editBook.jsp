<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "language=" + name;
        window.location.reload(true);
    }
</script>

<fmt:setLocale value="${requestScope.get('language')}"/>
<fmt:setBundle basename="/localization" scope="application"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Book shop</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="css/logInStyle.css">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a href="welcome_command" class="navbar-brand"><fmt:message key="layout.menu.title"/> </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="basicExampleNav">
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a href="view_all_books_command" class="nav-link waves-effect waves-light">
                            <fmt:message key="layout.menu.catalog"/>
                        </a>
                    </li>
                    <c:set var="admin">admin</c:set>
                    <c:if test="${sessionScope.current_user.role == admin}">
                        <li class="nav-item">
                            <a href="view_book_addition_form_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.add-new-book"/>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="view_all_users_command" class="nav-link waves-effect waves-light">
                                <fmt:message key="layout.menu.users"/>
                            </a>
                        </li>
                    </c:if>
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <c:choose>
                        <c:when test="${sessionScope.current_user == null}">
                            <li class="nav-item">
                                <a href="view_login_form_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-in"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_register_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.registration"/>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="nav-item">
                                <a href="logout_user_command" class="nav-link waves-effect waves-light">
                                    <fmt:message key="layout.menu.sign-out"/>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="view_basket_command" class="nav-link waves-effect waves-light">
                                    <i class="fas fa-shopping-basket"></i>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                    <div class="list-group list-group-horizontal">
                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('en_US')"><fmt:message key="layout.lang.en"/></a>

                        <a href="#" type="radio" class="nav-link waves-effect waves-light"
                           onclick="changeLang('ru_RU')"><fmt:message key="layout.lang.ru"/></a>
                    </div>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main class="mt-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="card card-container ">
                <i class="fa fa-user-circle " style="font-size: 170px; color: #BC8F8F!important;"></i>
                <form class="md-form text-center" action="edit_book_command">
                    <c:if test="${not empty edit_book_input_errors}" >
                        <c:forEach items="${edit_book_input_errors}" var="error">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                           <span >
                            <c:out value="${error}" />
                               <br>
                           </span>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>
                    <fmt:message key="editBook-form.title"/> ${oldBook.name}
                    <div class="md-form mt-3">
                        <input type="text" id="Name" class="form-control" name="book.name"  value="${oldBook.name}" required
                        pattern="^[a-zA-Z0-9 ]{1,45}$">
                        <label for="Name"><fmt:message key="addBook-form.name"/> </label>
                        <small id="bookNameHelpBlock" class="form-text text-muted">
                            Name must be 1-45 characters long, contain only letters, numbers and spaces.
                        </small>
                    </div>

                    <div class="md-form mt-3">
                        <input type="text" id="Author" class="form-control" name="book.author"  value="${oldBook.author}" required
                        pattern="^[a-zA-Z0-9 ]{1,45}$">
                        <label for="Author"><fmt:message key="addBook-form.author"/> </label>
                        <small id="authorHelpBlock" class="form-text text-muted">
                            Author must be 1-45 characters long, contain only letters, numbers and spaces.
                        </small>
                    </div>

                    <div class="md-form mt-3">
                        <input type="text" id="Category" class="form-control" name="book.category" value="${oldBook.category}" required
                        pattern="^[a-zA-Z ]{1,45}$">
                        <label for="Category"><fmt:message key="addBook-form.category"/> </label>
                        <small id="categoryHelpBlock" class="form-text text-muted">
                            Category must be 1-45 characters long, contain only letters, numbers and spaces.
                        </small>
                    </div>

                    <div class="md-form mt-3">
                        <input type="text" id="NumberOfPages" class="form-control" name="book.numberOfPages" value="${oldBook.numberOfPages}" required
                        pattern="[0-9]{1,4}">
                        <label for="NumberOfPages"><fmt:message key="addBook-form.pages"/> </label>
                        <small id="numberOfPagesHelpBlock" class="form-text text-muted">
                            Number of pages must be 1-4 characters long, contain only numbers.
                        </small>
                    </div>

                    <div class="md-form mt-3">
                        <input type="text" id="PublicationYear" class="form-control" name="book.publicationYear" value="${oldBook.publicationYear}" required
                        pattern="([1-2])([09])([0-9])([0-9])">
                        <label for="PublicationYear"><fmt:message key="addBook-form.publicationYear"/> </label>
                        <small id="publicationYearHelpBlock" class="form-text text-muted">
                            Publication year must be 4 characters long, contain only numbers.
                        </small>
                    </div>

                    <div class="md-form mt-3">
                        <input type="text" id="Description" class="form-control" name="book.description" value="${oldBook.description}" required
                        pattern="^.{1,500}$">
                        <label for="Description"><fmt:message key="addBook-form.description"/> </label>
                        <small id="descriptionHelpBlock" class="form-text text-muted">
                            Description must be 1-500 characters long.
                        </small>
                    </div>

                    <div class="md-form mt-3">
                        <input type="text" id="Price" class="form-control"name="book.price" value="${oldBook.price}" required
                        pattern="([0-9]{1,6})([.]?)([0-9]{0,2})">
                        <label for="Price"><fmt:message key="addBook-form.price"/> </label>
                        <input type="hidden" name="book.id" value="${oldBook.id}">
                        <small id="priceHelpBlock" class="form-text text-muted">
                            Price must be 1-8 characters long, contain only numbers and '.' character.
                        </small>
                    </div>

                    <button class="btn btn-lg btn-orange btn-block btn-sign-in" type="submit">
                        <fmt:message key="addBook-form.button"/>
                    </button>
                </form>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Your custom scripts (optional) -->
    <script type="text/javascript"></script>
</main>
</body>
</html>