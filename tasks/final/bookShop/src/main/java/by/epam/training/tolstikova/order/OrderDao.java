package by.epam.training.tolstikova.order;

import by.epam.training.tolstikova.dao.CRUDDao;
import by.epam.training.tolstikova.dao.ConnectionManager;
import by.epam.training.tolstikova.dao.PersistException;
import by.epam.training.tolstikova.entity.OrderEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderDao implements CRUDDao<OrderEntity, Long> {
    private static final String SELECT_ALL_QUERY = "SELECT id, status, user_account_id, order_info FROM book_order";

    private static final String DELETE_QUERY = "DELETE FROM book_order WHERE id = ?";

    private static final String FIND_ORDER_BY = "SELECT id, status, user_account_id, order_info FROM book_order WHERE ";

    private static final String FIND_ORDER_BY_ID = FIND_ORDER_BY + "id = ?";

    private static final String UPDATE_QUERY = "UPDATE book_order SET status=? WHERE id = ?";

    private static final String INSERT_ORDER =
            "INSERT INTO book_order (status, user_account_id, order_info VALUES (?, ?, ?)";

    private Connection connection;

    public OrderDao(ConnectionManager connectionManager) throws PersistException {
        this.connection = connectionManager.getConnection();
    }

    @Override
    public List<OrderEntity> getAll() throws PersistException {
        List<OrderEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                OrderEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get all orders", e);
        }
        return result;
    }

    @Override
    public Optional<OrderEntity> getEntityById(Long id) throws PersistException {
        List<OrderEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_ORDER_BY_ID)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                OrderEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get order by id", e);
        }

        return result.stream().findFirst();
    }

    @Override
    public boolean update(OrderEntity entity) throws PersistException {
        try (PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setBoolean(++i, entity.isDelivered());
            updateStmt.setLong(++i, entity.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new PersistException("Could not update order status", e);
        }
    }

    @Override
    public boolean delete(OrderEntity entity) throws PersistException {
        try (PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, entity.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new PersistException("Could not delete order", e);
        }
    }

    @Override
    public Long create(OrderEntity entity) throws PersistException {
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_ORDER, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setBoolean(++i, entity.isDelivered());
            insertStmt.setLong(++i, entity.getUserAccountId());
            insertStmt.setString(++i,entity.getInfo());
            insertStmt.executeUpdate();

            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get create order");
        }
        return entity.getId();
    }

    private OrderEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long entityId = resultSet.getLong("id");
        boolean status = resultSet.getBoolean("status");
        long userId = resultSet.getLong("user_entity_id");
        String info = resultSet.getString("order_info");

        return OrderEntity.builder()
                .id(entityId)
                .isDelivered(status)
                .userAccountId(userId)
                .info(info)
                .build();
    }
}
