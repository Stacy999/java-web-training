package by.epam.training.tolstikova.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter
public class UrlActionFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(UrlActionFilter.class);

    @Override
    public void doFilter(final ServletRequest servletRequestNew,
                         final ServletResponse servletResponseNew,
                         final FilterChain filterChainNew)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequestNew;
        HttpServletResponse resp = (HttpServletResponse) servletResponseNew;
        String commandName = getActionFromURI(req);
        servletRequestNew.setAttribute("command", commandName);
        filterChainNew.doFilter(req, resp);
    }

    @Override
    public void init(final FilterConfig filterConfig) {

    }

    @Override
    public void destroy() {

    }

    private String getActionFromURI(final HttpServletRequest request) {
        LOGGER.debug("Request method: " + request.getMethod());
        String context = request.getContextPath();
        String requestURI = request.getRequestURI();
        // LOGGER.debug("Request URI: "+ requestURI);
        int contextLength = context.length();
        String action;

        action = requestURI.substring(contextLength);
        if (action.length() == 0) {
            action = "welcome_command";
        }
        LOGGER.debug("Requested action: " + action);
        return action;
    }

}
