package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import by.epam.training.tolstikova.service.ServiceException;

import java.util.List;
import java.util.Optional;

public interface UserService {
    boolean loginUser(UserEntity userEntity) throws ServiceException;

    boolean registerUser(UserEntity userEntity, UserInformationEntity userInformationEntity) throws ServiceException;

    List<UserEntity> getAllUsers();

    Optional<UserEntity> getEntityById(Long id) throws ServiceException;

    List<UserEntity> getUsersByRole(String role, int limit, int offset) throws ServiceException;

    Optional<UserEntity> getUserByUserName(String userName) throws ServiceException;

    boolean delete(UserEntity userEntity) throws ServiceException;

    boolean update(UserEntity userEntity) throws ServiceException;
}
