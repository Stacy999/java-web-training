package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.dao.CRUDDao;
import by.epam.training.tolstikova.dao.ConnectionManager;
import by.epam.training.tolstikova.dao.PersistException;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.entity.UserInformationEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDao implements CRUDDao<UserEntity, Long> {
    private static final String SELECT_ALL_QUERY = "SELECT ua.id, ua.login, ua.password, ua.role, c.first_name, " +
            "c.last_name, c.email, c.phone, c.address, c.id info_id FROM user_account ua JOIN user_info c ON ua.user_info_id = c.id";

    private static final String DELETE_QUERY = "DELETE FROM user_account WHERE id = ?";

    private static final String FIND_USER_BY = "SELECT ua.id, ua.login, ua.password, ua.role, c.first_name, " +
            "c.last_name, c.email, c.phone, c.address, c.id info_id FROM user_account ua JOIN user_info c ON ua.user_info_id = c.id WHERE ";

    private static final String UPDATE_QUERY = "UPDATE user_account SET login=?, password=?, role=?" +
            " WHERE id = ?";

    private static final String FIND_USER_BY_ID = FIND_USER_BY + "ua.id = ?";

    private static final String FIND_USER_BY_EMAIL = FIND_USER_BY + "c.email = ?";

    private static final String FIND_USER_BY_USERNAME = FIND_USER_BY + "ua.login = ?";

    private static final String FIND_USERS_BY_ROLE = FIND_USER_BY + "role LIKE ? LIMIT ? OFFSET ?";

    private static final String INSERT_USER =
            "INSERT INTO `user_account` (`login`, "
                    + "`password`,`role`, `user_info_id`) "
                    + "VALUES (?, ?, ?, ?)";

    private Connection connection;

    public UserDao(ConnectionManager connectionManager) throws PersistException {
        this.connection = connectionManager.getConnection();
    }

    public List<UserEntity> getUsersByRole(String role, int limit, int offset) throws PersistException {
        List<UserEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USERS_BY_ROLE)) {
            selectStmt.setString(1, role);
            selectStmt.setInt(2, limit);
            selectStmt.setInt(3, offset);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get users by role", e);
        }
        return result;
    }

    public Optional<UserEntity> getUserByUserName(String userName) throws PersistException {
        List<UserEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_BY_USERNAME)) {
            selectStmt.setString(1, userName);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get user by user name", e);
        }

        return result.stream().findFirst();
    }

    public Optional<UserEntity> getUserByEmail(String email) throws PersistException {
        List<UserEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_BY_EMAIL)) {
            selectStmt.setString(1, email);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get user by email", e);
        }

        return result.stream().findFirst();
    }

    @Override
    public List<UserEntity> getAll() throws PersistException {
        List<UserEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get all users", e);
        }
        return result;
    }

    @Override
    public Optional<UserEntity> getEntityById(Long id) throws PersistException {
        List<UserEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_BY_ID)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get user by id", e);
        }

        return result.stream().findFirst();
    }

    @Override
    public boolean delete(UserEntity entity) throws PersistException {
        boolean deleted = false;
        try (PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            if (entity.getId() != null) {
                updateStmt.setLong(1, entity.getId());
                deleted = updateStmt.executeUpdate() > 0;
            }

        } catch (SQLException e) {
            throw new PersistException("Could not delete user", e);
        }
        return deleted;
    }

    @Override
    public boolean update(UserEntity entity) throws PersistException {
        try (PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setString(++i, entity.getLogin());
            updateStmt.setString(++i, entity.getPassword());
            updateStmt.setString(++i, entity.getRole());
            updateStmt.setLong(++i, entity.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new PersistException("Could not update user", e);
        }
    }

    @Override
    public Long create(UserEntity entity) throws PersistException {
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setString(++i, entity.getLogin());
            insertStmt.setString(++i, entity.getPassword());
            insertStmt.setString(++i, entity.getRole());
            insertStmt.setLong(++i, entity.getUserInformationEntity().getId());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException e) {
            throw new PersistException("Could not create user");
        }

        return entity.getId();
    }

    private UserEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long entityId = resultSet.getLong("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String role = resultSet.getString("role");
        UserInformationEntity userInformationEntity = new UserInformationEntity();

        userInformationEntity.setFirstName(resultSet.getString("first_name"));
        userInformationEntity.setLastName(resultSet.getString("last_name"));
        userInformationEntity.setAddress(resultSet.getString("address"));
        userInformationEntity.setEmail(resultSet.getString("email"));
        userInformationEntity.setPhone(resultSet.getString("phone"));
        userInformationEntity.setId(resultSet.getLong("info_id"));

        return UserEntity.builder()
                .id(entityId)
                .login(login)
                .password(password)
                .role(role)
                .userInformationEntity(userInformationEntity)
                .build();
    }


}
