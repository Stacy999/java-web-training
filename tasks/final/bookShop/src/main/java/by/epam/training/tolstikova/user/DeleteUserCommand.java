package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.service.ServiceException;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class DeleteUserCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteUserCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting delete user.");
        boolean deleted;
        UserEntity userEntity;
        String id = req.getParameter("user.id");

        try {
            userEntity = userService.getEntityById(Long.parseLong(id)).orElse(new UserEntity());
            deleted = userService.delete(userEntity);
        } catch (ServiceException e) {
            LOGGER.error("Cannot delete user");
            throw new CommandException(e.getMessage(), e);
        }

        if (deleted) {
            try {
                resp.sendRedirect("view_all_users_command");
            } catch (IOException e) {
                throw new CommandException("Failed to redirect", e);
            }
        }
    }
}
