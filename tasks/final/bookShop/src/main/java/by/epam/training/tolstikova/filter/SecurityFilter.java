package by.epam.training.tolstikova.filter;

import by.epam.training.tolstikova.core.SecurityContext;
import by.epam.training.tolstikova.entity.UserEntity;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Set;

@WebFilter(urlPatterns = {"/"}, servletNames = {"bookShop"})
public class SecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
        SecurityContext.getInstance().init();
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        String command = (String) req.getAttribute("command");
        HttpServletRequest httpReq = (HttpServletRequest) req;
        String userRole;

        if (httpReq.getSession().getAttribute("current_user") == null) {
            userRole = "visitor";
        } else {
            UserEntity user = (UserEntity) httpReq.getSession().getAttribute("current_user");
            userRole = user.getRole();
        }

        if (command == null || command.isEmpty() || command.startsWith("/font")) {
            chain.doFilter(req, resp);
        } else {
            Set<String> rolesForCommand;

            rolesForCommand = SecurityContext.getInstance().getRolesForCommand(command);

            if (rolesForCommand != null) {
                if (rolesForCommand.contains(userRole)) {
                    chain.doFilter(req, resp);
                } else {
                    httpReq.getRequestDispatcher("jsp/view/errorPage403.jsp").forward(httpReq, resp);
                }
            } else {
                httpReq.getRequestDispatcher("jsp/view/errorPage.jsp").forward(httpReq, resp);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
