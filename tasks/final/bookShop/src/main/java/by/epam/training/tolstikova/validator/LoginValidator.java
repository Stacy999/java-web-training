package by.epam.training.tolstikova.validator;

import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
public class LoginValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(LoginValidator.class);
    private UserService userService;

    public static boolean invalidateData(String username, String password) {
        return username == null || password == null;
    }

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();
        String login = req.getParameter("user.login");
        String password = req.getParameter("user.password");

        UserEntity userEntity = new UserEntity();
        userEntity.setPassword(password);
        userEntity.setLogin(login);

        if (invalidateData(login, password)) {
            LOGGER.warn("incorrect login or password: " + login);
            validationResult.addErrorMessage("emptyFieldsError: ", "Fields can not be empty");
        } else {

            try {
                if (!userService.loginUser(userEntity)) {
                    validationResult.addErrorMessage("loginError: ", "Incorrect login or password");
                }
            } catch (ServiceException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        return validationResult;
    }
}
