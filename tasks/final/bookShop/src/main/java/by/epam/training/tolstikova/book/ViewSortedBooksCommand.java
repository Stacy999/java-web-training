package by.epam.training.tolstikova.book;

import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@AllArgsConstructor
public class ViewSortedBooksCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewSortedBooksCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting show sorted books.");
        List<BookEntity> allSortedBooks;
        if (req.getParameter("sorted_order").equals("reverseOrder")) {
            allSortedBooks = ((UserServiceImpl) userService).getAllSortedReverseOrder();
        } else {
            allSortedBooks = ((UserServiceImpl) userService).getAllSortedDirectOrder();
        }
        req.setAttribute("books", allSortedBooks);

        try {
            req.getRequestDispatcher("jsp/view/bookCatalogView.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandException(e);
        }
    }
}
