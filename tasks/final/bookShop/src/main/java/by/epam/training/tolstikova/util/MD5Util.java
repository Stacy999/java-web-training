package by.epam.training.tolstikova.util;

import org.apache.commons.codec.digest.DigestUtils;

public class MD5Util {
    public static String md5getString(String str){
        return DigestUtils.md5Hex(str);
    }
}
