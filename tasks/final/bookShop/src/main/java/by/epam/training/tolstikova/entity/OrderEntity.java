package by.epam.training.tolstikova.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity {
    private boolean isDelivered;
    private long id;
    private Long userAccountId;
    private String info;
}