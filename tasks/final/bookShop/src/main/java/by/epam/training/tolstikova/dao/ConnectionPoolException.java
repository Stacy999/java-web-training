package by.epam.training.tolstikova.dao;

public class ConnectionPoolException extends RuntimeException {

    public ConnectionPoolException() {
    }

    ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message,
                                   Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }
}
