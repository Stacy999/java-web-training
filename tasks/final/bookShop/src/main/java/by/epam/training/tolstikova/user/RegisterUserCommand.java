package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.validator.RegisterUserValidator;
import by.epam.training.tolstikova.validator.ValidationResult;
import by.epam.training.tolstikova.validator.Validator;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class RegisterUserCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(RegisterUserCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting register user.");
        Validator validator;
        ValidationResult validationResult;
        validator = new RegisterUserValidator(userService);
        validationResult = validator.validate(req);

        String login = req.getParameter("user.login");
        String password = req.getParameter("user.password");
        UserEntity userEntity = new UserEntity();
        userEntity.setPassword(password);
        userEntity.setLogin(login);

        userEntity.setUserInformationEntity(UserInformationEntity
                .builder()
                .firstName(req.getParameter("user.firstName"))
                .lastName(req.getParameter("user.lastName"))
                .phone(req.getParameter("user.phone"))
                .email(req.getParameter("user.email"))
                .address(req.getParameter("user.address"))
                .build());

        if (validationResult.isValid()) {

            final boolean saved;
            try {
                saved = userService.registerUser(userEntity, userEntity.getUserInformationEntity());
            } catch (ServiceException e) {
                LOGGER.error("Can not save user");
                throw new CommandException(e.getMessage(), e);
            }
            if (saved) {
                try {
                    resp.sendRedirect("welcome_command");
                } catch (IOException e) {
                    throw new CommandException("Failed to forward", e);
                }
            }
        } else {
            List<String> inputErrors = new ArrayList<>();
            for (Map.Entry<String, String> entry : validationResult.getResult().entrySet()) {
                inputErrors.add(entry.getValue());
            }
            req.setAttribute("new_user_input_errors", inputErrors);
            req.setAttribute("user", userEntity);

            try {
                req.getRequestDispatcher("jsp/view/registerUserView.jsp").forward(req, resp);
            } catch (ServletException | IOException e) {
                throw new CommandException(e.getMessage(), e);
            }
        }
    }
}
