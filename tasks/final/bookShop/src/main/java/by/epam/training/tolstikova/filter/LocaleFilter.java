package by.epam.training.tolstikova.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebFilter(urlPatterns = {"/"}, servletNames = {"bookShop"})
public class LocaleFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(LocaleFilter.class);
    private static final String COOKIE_NAME = "language";

    private static final String FILTER_INIT_MSG = "LocaleFilter was successful initialized. Default locale is: ";
    private String defaultLocale;

    @Override
    public void init(FilterConfig filterConfig) {
        defaultLocale = filterConfig.getInitParameter("defaultLocale");
        if (defaultLocale == null) {
            defaultLocale = "en_US";
        }
        LOGGER.info(FILTER_INIT_MSG + defaultLocale);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Cookie[] requestCookies = httpRequest.getCookies();
        Optional<Cookie> localeCookie = Optional.empty();

        if (requestCookies != null) {
            localeCookie = Arrays.stream(requestCookies)
                    .filter(cookie -> cookie.getName().equalsIgnoreCase(COOKIE_NAME))
                    .filter(cookie -> cookie.getValue().equalsIgnoreCase("ru_ru") ||
                            cookie.getValue().equalsIgnoreCase("en_us"))
                    .findFirst();
        }
        String lang;
        if (localeCookie.isPresent()) {
            LOGGER.debug("locale cookie found");
            LOGGER.debug(localeCookie.get().getName() + " " + localeCookie.get().getValue());
            lang = localeCookie.get().getValue();
        } else {
            LOGGER.debug("locale cookie wasn't found");
            ((HttpServletResponse) response).addCookie(new Cookie(COOKIE_NAME, defaultLocale));
            lang = defaultLocale;
        }
        request.setAttribute(COOKIE_NAME, lang);
        chain.doFilter(request, response);
    }


    @Override
    public void destroy() {

    }
}
