package by.epam.training.tolstikova.validator;



import by.epam.training.tolstikova.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface Validator {
    ValidationResult validate(HttpServletRequest req);
}
