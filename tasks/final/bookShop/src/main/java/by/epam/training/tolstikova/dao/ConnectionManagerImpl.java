package by.epam.training.tolstikova.dao;

import java.sql.Connection;

public class ConnectionManagerImpl implements ConnectionManager {
    private TransactionManager transactionManager;

    public ConnectionManagerImpl(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    @Override
    public Connection getConnection() {

        return transactionManager.getConnection();
    }
}
