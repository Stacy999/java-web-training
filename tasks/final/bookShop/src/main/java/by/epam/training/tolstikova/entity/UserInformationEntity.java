package by.epam.training.tolstikova.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInformationEntity {

    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String address;
}
