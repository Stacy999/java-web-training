package by.epam.training.tolstikova.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Blob;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookEntity {
    private Long id;
    private Blob photo;
    private String description;
    private String category;
    private int numberOfPages;
    private String name;
    private int publicationYear;
    private String author;
    private BigDecimal price;
    private Long orderId;
}
