package by.epam.training.tolstikova.book;

import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class ViewEditBookCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewEditBookCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting show book edition form.");
        BookEntity bookEntity;
        try {
            Long bookId = Long.parseLong(req.getParameter("book.id"));
            bookEntity = ((UserServiceImpl) userService).getBookById(bookId).orElse(new BookEntity());
        } catch (ServiceException e) {
            LOGGER.error("Cannot edit book");
            throw new CommandException(e.getMessage(), e);
        }
        req.setAttribute("oldBook", bookEntity);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/editBook.jsp");
        try {
            requestDispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
