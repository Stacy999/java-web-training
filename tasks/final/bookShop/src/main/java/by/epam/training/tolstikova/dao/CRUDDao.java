package by.epam.training.tolstikova.dao;

import java.util.List;
import java.util.Optional;

public interface CRUDDao<T, K> {
    List<T> getAll() throws PersistException;

    Optional<T> getEntityById(K id) throws PersistException;

    boolean update(T entity) throws PersistException;

    boolean delete(T entity) throws PersistException;

    K create(T entity) throws PersistException;
}
