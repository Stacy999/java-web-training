package by.epam.training.tolstikova.dao;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static final ConnectionPool INSTANCE = new ConnectionPool();

    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

    private BlockingQueue<ConnectionImpl> pool
            = new LinkedBlockingQueue<>();
    private Set<ConnectionImpl> used = ConcurrentHashMap.newKeySet();

    private ReentrantLock locker = new ReentrantLock();

    private MysqlDataSource source;

    private int maxPoolSize;

    private AtomicInteger connectionNumber;

    private ConnectionPool() {
    }

    public static ConnectionPool getInstance() {
        return INSTANCE;
    }


    public void init(ResourceBundle bundle) {
        try {
            LOGGER.info("Initializing connection pool.");
            String driver = bundle.getString("db.driver");
            String url = bundle.getString("db.url");
            String user = bundle.getString("db.user");
            String pass = bundle.getString("db.pass");
            Class.forName(driver);
            int initPoolSize = Integer
                    .parseInt(bundle.getString("db.init-pool-size"));
            maxPoolSize = Integer
                    .parseInt(bundle.getString("db.max-pool-size"));
            if (maxPoolSize < 1
                    || initPoolSize < 1
                    || initPoolSize > maxPoolSize) {
                throw new ConnectionPoolException("Invalid parameters"
                        + " for pool initialization");
            }
            source = new MysqlDataSource();
            source.setURL(url);
            source.setUser(user);
            source.setPassword(pass);
            for (int i = 0; i < initPoolSize; i++) {
                pool.add(new ConnectionImpl(source.getConnection()));
            }
            connectionNumber = new AtomicInteger(initPoolSize);
            LOGGER.info("Connection pool is successfully initialized."
                    + " Vacant connections: " + pool.size());
        } catch (SQLException | MissingResourceException
                | ClassNotFoundException | NumberFormatException e) {
            LOGGER.error("Connection pool initialization error." +
                    e.getMessage());
            throw new ConnectionPoolException(
                    "Cannot initialize connection pool.");
        }
    }

    public Connection getConnection() {
        try {
            locker.lock();
            if (!pool.isEmpty()) {
                ConnectionImpl connection = pool.poll();
                if (connection.isClosed()) {
                    used.remove(connection);
                    return createConnection();
                }
                used.add(connection);
                LOGGER.debug("Got a connection from pool. "
                        + "Vacant connections: " +
                        pool.size() + ", used connections:" + used.size());
                return connection;
            } else {
                if (connectionNumber.get() < maxPoolSize) {
                    return createConnection();
                }
                throw new ConnectionPoolException("No available connections.");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot create new connection. "
                    + "SQL state: " + e.getSQLState() + "SQL message: " + e.getMessage());
            throw new ConnectionPoolException("Cannot create new connection.");
        } finally {
            locker.unlock();
        }
    }

    void releaseConnection(final ConnectionImpl connection) {
        try {
            connection.clearWarnings();
            used.remove(connection);
            pool.add(connection);
            LOGGER.debug("Connection released. "
                    + "Vacant connections: " +
                    pool.size() + ", used connections:" + used.size());
        } catch (SQLException e) {
            LOGGER.error("Cannot release connection. Connection is invalid. "
                    + "SQL state: " + e.getSQLState() + "SQL message: " + e.getMessage());
            try {
                connection.getConnection().close();
            } catch (SQLException e1) {
                LOGGER.error("Cannot close connection. "
                        + "SQL state: " + e1.getSQLState() + "SQL message: " + e1.getMessage());
            }
        }
    }

    public void close() {
        pool.addAll(used);
        used.clear();
        for (ConnectionImpl con : pool) {
            try {
                con.getConnection().close();
            } catch (SQLException e) {
                LOGGER.error("Cannot close connection. "
                        + "SQL state: " + e.getSQLState() + "SQL message: " + e.getMessage());
            }
        }
    }

    private ConnectionImpl createConnection() throws SQLException {
        LOGGER.debug("Creating a connection.");
        ConnectionImpl connection
                = new ConnectionImpl(source.getConnection());
        used.add(connection);
        LOGGER.info("New connection is created. "
                + "Vacant connections: " +
                pool.size() + ", used connections:" + used.size());
        connectionNumber.incrementAndGet();
        return connection;
    }
}
