package by.epam.training.tolstikova.user.userInfo;


import by.epam.training.tolstikova.dao.CRUDDao;
import by.epam.training.tolstikova.dao.ConnectionManager;
import by.epam.training.tolstikova.dao.PersistException;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import by.epam.training.tolstikova.validator.RegisterUserValidator;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserInfoDao implements CRUDDao<UserInformationEntity, Long> {

    private static final String SELECT_ALL_QUERY = "SELECT id, first_name, last_name, phone, email, address " +
            "FROM user_info";

    private static final String DELETE_QUERY = "DELETE FROM user_info WHERE id = ?";
    private static final String FIND_USER_INFO_BY = "SELECT id, first_name, " +
            "last_name, email, phone, address FROM user_info WHERE ";

    private static final String UPDATE_QUERY = "UPDATE user_info SET first_name=?, last_name=?, phone=?, email=?, " +
            " address=? WHERE id = ?";

    private static final String FIND_USER_INFO_BY_ID = FIND_USER_INFO_BY + "id = ?";

    private static final String FIND_USER_INFO_BY_EMAIL = FIND_USER_INFO_BY + "email = ?";
    private static final String INSERT_USER_INFO =
            "INSERT INTO `user_info` (`first_name`, "
                    + "`last_name`,`phone`, `email`, `address`) "
                    + "VALUES (?, ?, ?, ?, ?)";

    private Connection connection;

    public UserInfoDao(ConnectionManager connectionManager) throws PersistException {
        this.connection = connectionManager.getConnection();
    }

    @Override
    public List<UserInformationEntity> getAll() throws PersistException {
        List<UserInformationEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserInformationEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get all user information", e);
        }
        return result;
    }

    public Optional<UserInformationEntity> getUserByEmail(String email) throws PersistException {
        List<UserInformationEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_INFO_BY_EMAIL)) {
            selectStmt.setString(1, email);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserInformationEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get user information by email", e);
        }

        return result.stream().findFirst();
    }

    @Override
    public Optional<UserInformationEntity> getEntityById(Long id) throws PersistException {
        List<UserInformationEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_USER_INFO_BY_ID)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserInformationEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get user information by id", e);
        }

        return result.stream().findFirst();
    }

    @Override
    public boolean update(UserInformationEntity entity) throws PersistException {
        try (PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setString(++i, entity.getFirstName());
            updateStmt.setString(++i, entity.getLastName());
            updateStmt.setString(++i, entity.getPhone());
            updateStmt.setString(++i, entity.getEmail());
            updateStmt.setString(++i, entity.getAddress());
            updateStmt.setLong(++i, entity.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new PersistException("Could not update user information", e);
        }
    }

    @Override
    public boolean delete(UserInformationEntity entity) throws PersistException {
        boolean deleted = false;
        try (PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            int i = 0;
            if (entity != null && entity.getId() != null) {
                updateStmt.setLong(++i, entity.getId());
                deleted = updateStmt.executeUpdate() > 0;
            }

        } catch (SQLException e) {
            throw new PersistException("Could not delete user information", e);
        }
        return deleted;
    }

    @Override
    public Long create(UserInformationEntity entity) throws PersistException {
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_USER_INFO, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setString(++i, entity.getFirstName());
            insertStmt.setString(++i, entity.getLastName());
            insertStmt.setString(++i, entity.getPhone());
            insertStmt.setString(++i, entity.getEmail());
            insertStmt.setString(++i, entity.getAddress());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException e) {
            throw new PersistException("Could not create user information");
        }

        return entity.getId();
    }

    private UserInformationEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long entityId = resultSet.getLong("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String address = resultSet.getString("address");
        String email = resultSet.getString("email");
        String phone = resultSet.getString("phone");

        return UserInformationEntity.builder()
                .id(entityId)
                .firstName(firstName)
                .lastName(lastName)
                .address(address)
                .email(email)
                .phone(phone)
                .build();
    }
}
