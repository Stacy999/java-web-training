package by.epam.training.tolstikova.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    private Long id;
    private String login;
    private String password;
    private String role;
    private UserInformationEntity userInformationEntity;
}
