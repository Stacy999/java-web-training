package by.epam.training.tolstikova.dao;

import java.sql.Connection;

public interface TransactionManager {
    void beginTransaction() throws PersistException;

    void disableAutoCommit() throws PersistException;

    void enableAutoCommit() throws PersistException;

    void commitTransaction() throws PersistException;

    void rollbackTransaction() throws PersistException;

    Connection getConnection();
}
