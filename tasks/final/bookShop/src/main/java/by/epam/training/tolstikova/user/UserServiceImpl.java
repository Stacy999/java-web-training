package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.book.BookDao;
import by.epam.training.tolstikova.dao.CRUDDao;
import by.epam.training.tolstikova.dao.PersistException;
import by.epam.training.tolstikova.dao.TransactionManager;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.entity.OrderEntity;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import by.epam.training.tolstikova.order.OrderDao;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.userInfo.UserInfoDao;
import by.epam.training.tolstikova.util.MD5Util;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    private TransactionManager transactionManager;
    private UserDao userDao;
    private UserInfoDao userInfoDao;
    private BookDao bookDao;
    private OrderDao orderDao;

    public UserServiceImpl(CRUDDao userDao, CRUDDao userInfoDao, CRUDDao bookDao, CRUDDao orderDao,
                           TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
        this.userDao = (UserDao) userDao;
        this.userInfoDao = (UserInfoDao) userInfoDao;
        this.bookDao = (BookDao) bookDao;
        this.orderDao = (OrderDao) orderDao;
    }

    @Override
    public boolean loginUser(UserEntity userEntity) throws ServiceException {

        Optional<UserEntity> byLogin;
        try {
            byLogin = userDao.getUserByUserName(userEntity.getLogin());
            if (byLogin.isPresent()) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return byLogin.filter(dto -> dto.getPassword().equals(userEntity.getPassword())).isPresent();
    }

    @Override
    public boolean registerUser(UserEntity userEntity, UserInformationEntity userInformationEntity) throws ServiceException {
        boolean result = false;
        try {
            if (!isNotValidLogin(userEntity.getLogin())) {
                userInfoDao.create(userInformationEntity);
                UserInformationEntity savedUserInfo = userInfoDao.getUserByEmail(userInformationEntity.getEmail()).get();
                userEntity.setUserInformationEntity(savedUserInfo);
                userEntity.setPassword(MD5Util.md5getString(userEntity.getPassword()));
                userEntity.setRole("user");
                Long saved = userDao.create(userEntity);
                transactionManager.commitTransaction();
                result = saved > 0;
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public List<UserEntity> getAllUsers() {
        try {
            return userDao.getAll();
        } catch (PersistException e) {
            LOGGER.error("Failed to get all users", e);
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<UserEntity> getEntityById(Long id) throws ServiceException {
        Optional<UserEntity> byId;
        try {
            byId = userDao.getEntityById(id);

            if (byId.isPresent()) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return byId;

    }

    @Override
    public List<UserEntity> getUsersByRole(String role, int limit, int offset) throws ServiceException {
        List<UserEntity> byRole;
        try {
            byRole = userDao.getUsersByRole(role, limit, offset);

            if (!byRole.isEmpty()) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return byRole;
    }

    @Override
    public Optional<UserEntity> getUserByUserName(String userName) throws ServiceException {

        Optional<UserEntity> byUserName;
        try {
            byUserName = userDao.getUserByUserName(userName);

            if (byUserName.isPresent()) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return byUserName;

    }

    @Override
    public boolean delete(UserEntity userEntity) throws ServiceException {
        boolean userDeleted;
        boolean userInfoDeleted;
        try {
            userDeleted = userDao.delete(userEntity);
            userInfoDeleted = userInfoDao.delete(userEntity.getUserInformationEntity());

            if (userInfoDeleted && userDeleted) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return userDeleted;
    }

    @Override
    public boolean update(UserEntity userEntity) throws ServiceException {
        boolean userUpdated;
        boolean userInfoUpdated;
        try {
            userInfoUpdated = userInfoDao.update(userEntity.getUserInformationEntity());
            userUpdated = userDao.update(userEntity);

            if (userInfoUpdated && userUpdated) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return userUpdated;
    }

    public boolean editBook(BookEntity bookEntity) throws ServiceException {
        boolean bookUpdated;
        try {
            bookUpdated = bookDao.update(bookEntity);
            if (bookUpdated) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return bookUpdated;
    }

    public boolean deleteBook(BookEntity bookEntity) throws ServiceException {
        boolean bookDeleted;
        try {
            bookDeleted = bookDao.delete(bookEntity);
            if (bookDeleted) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return bookDeleted;
    }

    public Long createBook(BookEntity bookEntity) throws ServiceException {
        Long bookCreatedId;
        try {
            bookCreatedId = bookDao.create(bookEntity);
            if (bookCreatedId > 0) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return bookCreatedId;
    }

    public List<BookEntity> getAllBooks() {
        try {
            return bookDao.getAll();
        } catch (PersistException e) {
            LOGGER.error("Failed to get all books", e);
            return new ArrayList<>();
        }
    }

    public Optional<BookEntity> getBookById(Long id) throws ServiceException {
        Optional<BookEntity> byId;
        try {
            byId = bookDao.getEntityById(id);
            if (byId.isPresent()) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }

        } catch (PersistException e) {
            throw new ServiceException("Failed to get book by id", e);
        }
        return byId;
    }

    public boolean isNotValidLogin(String login) throws ServiceException {
        return getUserByUserName(login).isPresent();
    }

    public List<BookEntity> getAllSortedDirectOrder() {
        try {
            return bookDao.getAllSortedDirectOrder();
        } catch (PersistException e) {
            LOGGER.error("Failed to get all sorted users", e);
            return new ArrayList<>();
        }
    }

    public List<BookEntity> getAllSortedReverseOrder() {
        try {
            return bookDao.getAllSortedReverseOrder();
        } catch (PersistException e) {
            LOGGER.error("Failed to get all sorted users", e);
            return new ArrayList<>();
        }
    }

    public Long createOrder(OrderEntity orderEntity) throws ServiceException {
        Long orderCreated;
        try {
            orderCreated = orderDao.create(orderEntity);
            if (orderCreated > 1) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException("Failed to create new order", e);
        }
        return orderCreated;
    }

    public boolean updateOrder(OrderEntity orderEntity) throws ServiceException {
        boolean orderUpdated;
        try {
            orderUpdated = orderDao.update(orderEntity);
            if (orderUpdated) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException("Failed to update order", e);
        }
        return orderUpdated;
    }

    public boolean deleteOrder(OrderEntity orderEntity) throws ServiceException {
        boolean orderDeleted;
        try {
            orderDeleted = orderDao.delete(orderEntity);
            if (orderDeleted) {
                transactionManager.commitTransaction();
            } else {
                transactionManager.rollbackTransaction();
            }
        } catch (PersistException e) {
            throw new ServiceException("Failed to delete user", e);
        }
        return orderDeleted;
    }

    public List<OrderEntity> getAllOrders() {
        try {
            return orderDao.getAll();
        } catch (PersistException e) {
            //log.error("Failed to get all orders", e);
            return new ArrayList<>();
        }
    }
}
