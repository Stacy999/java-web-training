package by.epam.training.tolstikova.listener;

import by.epam.training.tolstikova.core.ApplicationContext;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationContextListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ApplicationContextListener.class);

    private static final String APP_CONTEXT_INIT_MESSAGE = "ApplicationContext has been initialized.";
    private static final String APP_CONTEXT_DESTROY_MESSAGE = "ApplicationContext has been destroyed.";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext.getInstance().init();
        LOGGER.info(APP_CONTEXT_INIT_MESSAGE);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ApplicationContext.getInstance().destroy();
        LOGGER.info(APP_CONTEXT_DESTROY_MESSAGE);
    }
}
