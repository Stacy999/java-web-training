package by.epam.training.tolstikova.servlet;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.core.ApplicationContext;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "bookShop")
public class ApplicationServlet extends HttpServlet {

    private static final long serialVersionUID = -898419077104540041L;
    private static final String MAIN_PAGE = "jsp/index.jsp";
    private final static Logger LOGGER = Logger.getLogger(ApplicationServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {

        String commandName = (String) req.getAttribute("command");
        ServletCommand command = ApplicationContext.getInstance().getCommand(commandName);
        try {
            if (commandName == null || commandName.length() == 0) {
                req.getRequestDispatcher(MAIN_PAGE).forward(req, resp);
            } else {
                try {
                    command.execute(req, resp);
                } catch (CommandException e) {
                    throw new ServletException(e.getMessage());
                }
            }

        } catch (IOException e) {
            LOGGER.error("Cannot forward user", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        doGet(req, resp);
    }
}
