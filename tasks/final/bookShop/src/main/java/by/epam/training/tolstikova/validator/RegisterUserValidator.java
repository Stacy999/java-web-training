package by.epam.training.tolstikova.validator;


import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
public class RegisterUserValidator implements Validator {

    private static final Logger LOGGER = Logger.getLogger(RegisterUserValidator.class);

    private static final String USER_USERNAME_REGEX = "^[a-zA-Z0-9]{4,16}$";

    private static final String USER_PASSWORD_REGEX = "^[a-zA-Z0-9]{8,20}$";

    private static final String USER_EMAIL_REGEX =
            "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\"
                    + ".[a-z]{2,6}$";

    private static final String USER_PHONE_REGEX =
            "^((29)|(33)|(44)|(25))(([1-9]{1})([0-9]{6}))$";

    private UserService userService;


    public static boolean invalidateUsername(String username) {
        return username == null || !username.matches(USER_USERNAME_REGEX);
    }

    public static boolean invalidatePassword(String password) {
        return password == null || !password.matches(USER_PASSWORD_REGEX);
    }

    public static boolean invalidateEmail(String email) {
        return email == null || !email.matches(USER_EMAIL_REGEX);
    }

    public static boolean invalidatePhone(String phone) {
        return phone == null || !phone.matches(USER_PHONE_REGEX);
    }

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();

        try {
            if (((UserServiceImpl) userService).isNotValidLogin(req.getParameter("user.login"))) {
                validationResult.addErrorMessage("loginError", "Incorrect login: The username is not unique");
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(),e);
        }

        if (invalidateUsername(req.getParameter("user.login"))) {
            LOGGER.warn("incorrect login: "+ req.getParameter("user.login"));
            validationResult.addErrorMessage("loginError", "Incorrect login: Username can contain" +
                    "only letters and numbers. It must be from 4 to 16 characters");
        }
        if (invalidatePassword(req.getParameter("user.password"))) {
            LOGGER.warn("incorrect password");
            validationResult.addErrorMessage("passwordError", "Incorrect password: Password can contain" +
                    "only letters and numbers. It must be from 8 to 20 characters");
        }
        if (invalidateEmail(req.getParameter("user.email"))) {
            LOGGER.warn("incorrect email: "+ req.getParameter("user.email"));
            validationResult.addErrorMessage("emailError", "Incorrect e-mail: E-mail must contain" +
                    " '@' and '.' symbols");
        }
        if (invalidatePhone(req.getParameter("user.phone"))) {
            LOGGER.warn("incorrect phone: "+ req.getParameter("user.phone"));

            validationResult.addErrorMessage("phoneError", "Incorrect phone: Phone can contain only " +
                    "numbers. It must have 9-symbols length");
        }
        return validationResult;
    }
}
