package by.epam.training.tolstikova.dao;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class TransactionManagerImpl implements TransactionManager {
    private ConnectionPool connectionPool;
    private Connection localConnection;

    private static final Logger LOGGER = Logger.getLogger(TransactionManagerImpl.class);

    public TransactionManagerImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public void beginTransaction() throws PersistException {
        if (localConnection == null) {
            localConnection = connectionPool.getConnection();
            disableAutoCommit();
        } else {
            LOGGER.warn("Transaction already started");
        }
    }

    @Override
    public void disableAutoCommit() throws PersistException {
        try {
            if (localConnection.getAutoCommit()) {
                localConnection.setAutoCommit(false);
            }
        } catch (SQLException e) {
            throw new PersistException("could not set auto commit to false",
                    e);
        }
    }

    @Override
    public void enableAutoCommit() throws PersistException {
        try {
            if (!localConnection.getAutoCommit()) {
                localConnection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new PersistException("could not set auto commit to true",
                    e);
        }
    }

    @Override
    public void commitTransaction() throws PersistException {
        Connection connection = localConnection;
        try {
            if (connection != null) {
                connection.commit();
                connection.close();
            }
        } catch (SQLException e) {
            throw new PersistException("could not commit transaction",
                    e);
        }
        localConnection = null;
    }

    @Override
    public void rollbackTransaction() throws PersistException {
        Connection connection = localConnection;
        try {
            if (connection != null) {
                connection.rollback();
                connection.close();
            }
        } catch (SQLException e) {
            throw new PersistException("could not rollback transaction",
                    e);
        }
        localConnection = null;
    }

    @Override
    public Connection getConnection() {

        return localConnection;
    }
}
