package by.epam.training.tolstikova.book;

import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import by.epam.training.tolstikova.validator.AddNewBookValidator;
import by.epam.training.tolstikova.validator.ValidationResult;
import by.epam.training.tolstikova.validator.Validator;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class EditBookCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(EditBookCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting edit book.");
        Validator validator;
        ValidationResult validationResult;
        validator = new AddNewBookValidator();
        validationResult = validator.validate(req);

        if (validationResult.isValid()) {

            String name = req.getParameter("book.name");
            String author = req.getParameter("book.author");
            String category = req.getParameter("book.category");
            int numberOfPages = Integer.parseInt(req.getParameter("book.numberOfPages"));
            int publicationYear = Integer.parseInt(req.getParameter("book.publicationYear"));
            String description = req.getParameter("book.description");
            BigDecimal price = BigDecimal.valueOf(Double.parseDouble(req.getParameter("book.price")));
            Long id = Long.parseLong(req.getParameter("book.id"));

            BookEntity newBook = new BookEntity();
            newBook.setId(id);
            newBook.setName(name);
            newBook.setAuthor(author);
            newBook.setCategory(category);
            newBook.setNumberOfPages(numberOfPages);
            newBook.setPublicationYear(publicationYear);
            newBook.setDescription(description);
            newBook.setPrice(price);

            boolean edited;
            try {
                edited = ((UserServiceImpl) userService).editBook(newBook);
            } catch (ServiceException e) {
                LOGGER.error("Cannot edit book");
                throw new CommandException(e.getMessage(), e);
            }
            if (edited) {
                try {
                    resp.sendRedirect("view_all_books_command");
                } catch (IOException e) {
                    throw new CommandException("Failed to forward", e);
                }

            }
        } else {
            List<String> inputErrors = new ArrayList<>();
            for (Map.Entry<String, String> entry : validationResult.getResult().entrySet()) {
                inputErrors.add(entry.getValue());
            }
            req.setAttribute("edit_book_input_errors", inputErrors);

            try {
                req.getRequestDispatcher("jsp/editBook.jsp").forward(req, resp);
            } catch (ServletException | IOException e) {
                throw new CommandException(e.getMessage(), e);
            }
        }
    }
}
