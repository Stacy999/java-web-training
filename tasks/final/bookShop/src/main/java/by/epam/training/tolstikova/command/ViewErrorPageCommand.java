package by.epam.training.tolstikova.command;

import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewErrorPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewErrorPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting show error page.");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/view/errorPage.jsp");
        try {
            requestDispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
