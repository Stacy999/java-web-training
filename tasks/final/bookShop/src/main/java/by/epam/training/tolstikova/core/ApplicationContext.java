package by.epam.training.tolstikova.core;

import by.epam.training.tolstikova.ApplicationConstants;
import by.epam.training.tolstikova.book.*;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.command.ViewBasketCommand;
import by.epam.training.tolstikova.command.ViewErrorPageCommand;
import by.epam.training.tolstikova.command.WelcomeCommand;
import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.order.OrderDao;
import by.epam.training.tolstikova.user.*;
import by.epam.training.tolstikova.user.userInfo.UserInfoDao;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext {
    private static final String DB_PROPERTY_NAME = "database";

    private static final String CONNECTION_POOL_ERR_MSG = "Cannot initialize connection pool";

    private static final String PERSIST_ERR_MSG = "Error while working with transaction";

    private static ReentrantLock locker = new ReentrantLock();

    private static ApplicationContext instance;

    private Map<String, ServletCommand> commands = new HashMap<>();

    private final static Logger LOGGER = Logger.getLogger(ApplicationContext.class);

    private ApplicationContext() {

    }

    public static ApplicationContext getInstance() {
        if (instance == null) {
            try {
                locker.lock();
                if (instance == null) {
                    instance = new ApplicationContext();
                }
            } finally {
                locker.unlock();
            }
        }
        return instance;
    }

    public void init(){
        try {

            ConnectionPool connectionPool = ConnectionPool.getInstance();
            connectionPool.init(ResourceBundle.getBundle(DB_PROPERTY_NAME));
            TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
            transactionManager.beginTransaction();
            ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
            CRUDDao userDao = new UserDao(connectionManager);
            CRUDDao userInfoDao = new UserInfoDao(connectionManager);
            CRUDDao bookDao = new BookDao(connectionManager);
            CRUDDao orderDao = new OrderDao(connectionManager);

            UserService userService = new UserServiceImpl(userDao, userInfoDao, bookDao, orderDao, transactionManager);

            ServletCommand addBookToBasketCommand = new AddBookToBasketCommand(userService);
            ServletCommand addNewBookCommand = new AddNewBookCommand(userService);
            ServletCommand deleteBookCommand = new DeleteBookCommand(userService);
            ServletCommand deleteBookFromBasketCommand = new DeleteBookFromBasketCommand(userService);
            ServletCommand editBookCommand = new EditBookCommand(userService);
            ServletCommand viewAllBooksCommand = new ViewAllBooksCommand(userService);
            ServletCommand viewBookAdditionFormCommand = new ViewBookAdditionFormCommand();
            ServletCommand viewEditBookCommand = new ViewEditBookCommand(userService);
            ServletCommand viewSortedBooksCommand = new ViewSortedBooksCommand(userService);
            ServletCommand viewBasketCommand = new ViewBasketCommand();
            ServletCommand viewErrorPageCommand = new ViewErrorPageCommand();
            ServletCommand welcomeCommand = new WelcomeCommand();
            ServletCommand deleteUserCommand = new DeleteUserCommand(userService);
            ServletCommand loginUserCommand = new LoginUserCommand(userService);
            ServletCommand logoutUserCommand = new LogoutUserCommand();
            ServletCommand registerUserCommand = new RegisterUserCommand(userService);
            ServletCommand viewAllUsersCommand = new ViewAllUsersCommand(userService);
            ServletCommand viewEditUserCommand = new ViewEditUserCommand(userService);
            ServletCommand viewLoginFormCommand = new ViewLoginFormCommand();
            ServletCommand viewRegisterFormCommand = new ViewRegisterFormCommand();

            commands.put(ApplicationConstants.ADD_BOOK_TO_BASKET_COMMAND, addBookToBasketCommand);
            commands.put(ApplicationConstants.ADD_NEW_BOOK_COMMAND, addNewBookCommand);
            commands.put(ApplicationConstants.DELETE_BOOK_COMMAND, deleteBookCommand);
            commands.put(ApplicationConstants.DELETE_BOOK_FROM_BASKET_COMMAND, deleteBookFromBasketCommand);
            commands.put(ApplicationConstants.EDIT_BOOK_COMMAND, editBookCommand);
            commands.put(ApplicationConstants.VIEW_ALL_BOOKS_COMMAND, viewAllBooksCommand);
            commands.put(ApplicationConstants.VIEW_BOOK_ADDITION_FORM_COMMAND, viewBookAdditionFormCommand);
            commands.put(ApplicationConstants.VIEW_EDIT_BOOK_COMMAND, viewEditBookCommand);
            commands.put(ApplicationConstants.VIEW_SORTED_BOOKS_COMMAND, viewSortedBooksCommand);
            commands.put(ApplicationConstants.VIEW_BASKET_COMMAND, viewBasketCommand);
            commands.put(ApplicationConstants.VIEW_ERROR_PAGE_COMMAND, viewErrorPageCommand);
            commands.put(ApplicationConstants.WELCOME_COMMAND, welcomeCommand);
            commands.put(ApplicationConstants.DELETE_USER_COMMAND, deleteUserCommand);
            commands.put(ApplicationConstants.LOGIN_USER_COMMAND, loginUserCommand);
            commands.put(ApplicationConstants.LOGOUT_USER_COMMAND, logoutUserCommand);
            commands.put(ApplicationConstants.REGISTER_USER_COMMAND, registerUserCommand);
            commands.put(ApplicationConstants.VIEW_ALL_USERS_COMMAND, viewAllUsersCommand);
            commands.put(ApplicationConstants.VIEW_EDIT_USER_COMMAND, viewEditUserCommand);
            commands.put(ApplicationConstants.VIEW_LOGIN_FORM_COMMAND, viewLoginFormCommand);
            commands.put(ApplicationConstants.VIEW_REGISTER_USER_COMMAND, viewRegisterFormCommand);
            commands.put(ApplicationConstants.MAIN_COMMAND, welcomeCommand);

        } catch (ConnectionPoolException e) {
            LOGGER.error(CONNECTION_POOL_ERR_MSG, e);
        } catch (PersistException e) {
            LOGGER.error(PERSIST_ERR_MSG,e);
        }
    }

    public void destroy() {
        ConnectionPool.getInstance().close();
    }

    public ServletCommand getCommand(String commandName) {
        return commands.get(commandName);
    }
}


