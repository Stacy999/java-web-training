package by.epam.training.tolstikova.book;

import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BasketEntity;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@AllArgsConstructor
public class DeleteBookFromBasketCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteBookFromBasketCommand.class);
    UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting delete book from basket.");
        HttpSession session = req.getSession();
        BasketEntity basket = (BasketEntity) session.getAttribute("current_basket");
        Long id = Long.parseLong(req.getParameter("book.id"));
        BookEntity book;
        try {
            book = ((UserServiceImpl) userService).getBookById(id).orElse(new BookEntity());
        } catch (ServiceException e) {
            LOGGER.error("Cannot delete book from basket");
            throw new CommandException(e.getMessage(), e);
        }
        basket.getBooks().remove(book);
        try {
            resp.sendRedirect("view_basket_command");
        } catch (IOException e) {
            throw new CommandException("Failed to redirect", e);
        }
    }
}
