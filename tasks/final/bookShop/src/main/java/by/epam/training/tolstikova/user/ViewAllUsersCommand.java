package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class ViewAllUsersCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewAllUsersCommand.class);
    private UserService userService;

    public ViewAllUsersCommand(UserService userService) {

        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting show users catalog.");
        List<UserEntity> allUsers;
        try {
            allUsers = userService.getUsersByRole("user", 100, 0);
        } catch (ServiceException e) {
            LOGGER.error("Cannot show users catalog");
            throw new CommandException(e.getMessage(), e);
        }
        req.setAttribute("users", allUsers);

        try {
            req.getRequestDispatcher("jsp/view/viewAllUsers.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandException(e);
        }
    }
}
