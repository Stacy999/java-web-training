package by.epam.training.tolstikova.book;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import by.epam.training.tolstikova.validator.AddNewBookValidator;
import by.epam.training.tolstikova.validator.ValidationResult;
import by.epam.training.tolstikova.validator.Validator;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class AddNewBookCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(AddNewBookCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Adding new book.");
        Validator validator;
        ValidationResult validationResult;
        validator = new AddNewBookValidator();
        validationResult = validator.validate(req);

        if (validationResult.isValid()) {

            final String name = req.getParameter("book.name");
            final String author = req.getParameter("book.author");
            final String category = req.getParameter("book.category");
            final int numberOfPages = Integer.parseInt(req.getParameter("book.numberOfPages"));
            final int publicationYear = Integer.parseInt(req.getParameter("book.publicationYear"));
            final String description = req.getParameter("book.description");
            final BigDecimal price = BigDecimal.valueOf(Double.parseDouble(req.getParameter("book.price")));

            BookEntity newBook = new BookEntity();
            newBook.setName(name);
            newBook.setAuthor(author);
            newBook.setCategory(category);
            newBook.setNumberOfPages(numberOfPages);
            newBook.setPublicationYear(publicationYear);
            newBook.setDescription(description);
            newBook.setPrice(price);


            boolean saved;
            try {
                saved = ((UserServiceImpl) userService).createBook(newBook) > 1;
            } catch (ServiceException e) {
                LOGGER.error("Cannot save book");
                throw new CommandException(e.getMessage(), e);
            }
            if (saved) {
                try {
                    resp.sendRedirect("view_all_books_command");
                } catch (IOException e) {
                    throw new CommandException("Failed to forward", e);
                }

            }
        } else {
            List<String> inputErrors = new ArrayList<>();
            for (Map.Entry<String, String> entry : validationResult.getResult().entrySet()) {
                inputErrors.add(entry.getValue());
            }
            req.setAttribute("new_book_input_errors", inputErrors);

            try {
                req.getRequestDispatcher("jsp/addNewBook.jsp").forward(req, resp);
            } catch (ServletException | IOException e) {
                throw new CommandException(e.getMessage(), e);
            }
        }
    }
}