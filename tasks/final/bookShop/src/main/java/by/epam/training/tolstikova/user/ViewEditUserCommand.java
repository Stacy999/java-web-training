package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.service.ServiceException;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class ViewEditUserCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewEditUserCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting show edit user form.");
        UserEntity userEntity;
        try {
            userEntity = userService.getEntityById(Long.parseLong(req.getParameter("user.id"))).orElse(new UserEntity());

        } catch (ServiceException e) {
            LOGGER.error("Cannot show user edit form");
            throw new CommandException(e.getMessage(), e);
        }
        req.setAttribute("oldUser", userEntity);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/editUser.jsp");
        try {
            requestDispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
