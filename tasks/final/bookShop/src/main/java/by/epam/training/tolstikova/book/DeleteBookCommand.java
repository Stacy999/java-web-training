package by.epam.training.tolstikova.book;

import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class DeleteBookCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteBookCommand.class);
    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting delete book.");
        boolean deleted;
        BookEntity bookEntity;
        String id = req.getParameter("book.id");

        try {
            bookEntity = ((UserServiceImpl) userService).getBookById(Long.parseLong(id)).orElse(new BookEntity());

            deleted = ((UserServiceImpl) userService).deleteBook(bookEntity);
        } catch (ServiceException e) {
            LOGGER.error("Cannot delete book");
            throw new CommandException(e.getMessage(), e);
        }

        if (deleted) {
            try {
                resp.sendRedirect("view_all_books_command");
            } catch (IOException e) {
                throw new CommandException("Failed to redirect", e);
            }
        }
    }
}
