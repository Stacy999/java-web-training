package by.epam.training.tolstikova.book;


import by.epam.training.tolstikova.dao.CRUDDao;
import by.epam.training.tolstikova.dao.ConnectionManager;
import by.epam.training.tolstikova.dao.PersistException;
import by.epam.training.tolstikova.entity.BookEntity;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookDao implements CRUDDao<BookEntity, Long> {
    private static final String SELECT_ALL_QUERY = "SELECT id, book_name, author, price, photo, " +
            "book_category, publication_year, description, number_of_pages, order_id FROM book ";

    private static final String DELETE_QUERY = "DELETE FROM book WHERE id = ?";

    private static final String FIND_BOOK_BY = "SELECT id, book_name, author, price, photo, " +
            "book_category, publication_year, description, number_of_pages, order_id FROM book WHERE ";

    private static final String FIND_BOOK_BY_ID = FIND_BOOK_BY + "id = ?";

    private static final String UPDATE_QUERY = "UPDATE book SET book_name=?, author=?, price=?," +
            "book_category=?, publication_year=?, description=?, number_of_pages=?" +
            " WHERE id = ?";

    private static final String INSERT_BOOK =
            "INSERT INTO book (book_name, "
                    + "author,price, photo, book_category, publication_year, description,number_of_pages) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SELECT_ALL_SORTED_BY_NAME_DIRECT_ORDER = SELECT_ALL_QUERY + "ORDER BY book_name";

    private static final String SELECT_ALL_SORTED_BY_NAME_REVERSE_ORDER = SELECT_ALL_QUERY + "ORDER BY book_name DESC";

    private Connection connection;

    public BookDao(ConnectionManager connectionManager) throws PersistException {
        this.connection = connectionManager.getConnection();
    }

    @Override
    public List<BookEntity> getAll() throws PersistException {
        List<BookEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                BookEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get all books", e);
        }
        return result;
    }

    public List<BookEntity> getAllSortedDirectOrder() throws PersistException {
        List<BookEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_SORTED_BY_NAME_DIRECT_ORDER)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                BookEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get all sorted in direct order books", e);
        }
        return result;
    }

    public List<BookEntity> getAllSortedReverseOrder() throws PersistException {
        List<BookEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_SORTED_BY_NAME_REVERSE_ORDER)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                BookEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get all sorted in reverse order books", e);
        }
        return result;
    }

    @Override
    public Optional<BookEntity> getEntityById(Long id) throws PersistException {
        List<BookEntity> result = new ArrayList<>();
        try (PreparedStatement selectStmt = connection.prepareStatement(FIND_BOOK_BY_ID)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                BookEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new PersistException("Could not get book by id", e);
        }

        return result.stream().findFirst();
    }

    @Override
    public boolean update(BookEntity entity) throws PersistException {
        try (PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setString(++i, entity.getName());
            updateStmt.setString(++i, entity.getAuthor());
            updateStmt.setBigDecimal(++i, entity.getPrice());
            updateStmt.setString(++i, entity.getCategory());
            updateStmt.setInt(++i, entity.getPublicationYear());
            updateStmt.setString(++i, entity.getDescription());
            updateStmt.setInt(++i, entity.getNumberOfPages());

            updateStmt.setLong(++i, entity.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new PersistException("Could not update book", e);
        }
    }

    @Override
    public boolean delete(BookEntity entity) throws PersistException {
        try (PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, entity.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new PersistException("Could not delete book", e);
        }
    }

    @Override
    public Long create(BookEntity entity) throws PersistException {
        try (PreparedStatement insertStmt = connection.prepareStatement(INSERT_BOOK, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setString(++i, entity.getName());
            insertStmt.setString(++i, entity.getAuthor());
            insertStmt.setBigDecimal(++i, entity.getPrice());
            insertStmt.setBlob(++i, entity.getPhoto());
            insertStmt.setString(++i, entity.getCategory());
            insertStmt.setInt(++i, entity.getPublicationYear());
            insertStmt.setString(++i, entity.getDescription());
            insertStmt.setInt(++i, entity.getNumberOfPages());


            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException e) {
            throw new PersistException("Could not create book",e);
        }
        return entity.getId();
    }

    private BookEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long entityId = resultSet.getLong("id");
        String name = resultSet.getString("book_name");
        String author = resultSet.getString("author");
        String category = resultSet.getString("book_category");
        String description = resultSet.getString("description");
        int numberOfPages = resultSet.getInt("number_of_pages");
        int publicationYear = resultSet.getInt("publication_year");
        BigDecimal price = resultSet.getBigDecimal("price");

        return BookEntity.builder()
                .id(entityId)
                .name(name)
                .author(author)
                .category(category)
                .description(description)
                .numberOfPages(numberOfPages)
                .publicationYear(publicationYear)
                .price(price)
                .build();
    }
}
