package by.epam.training.tolstikova;

import java.util.HashSet;
import java.util.Set;

public class ApplicationConstants {
    public static final Set<String> ALL_COMMANDS = new HashSet<>();

    public static final String LOGIN_USER_COMMAND = "/login_user_command";
    public static final String LOGOUT_USER_COMMAND = "/logout_user_command";
    public static final String VIEW_LOGIN_FORM_COMMAND = "/view_login_form_command";
    public static final String REGISTER_USER_COMMAND = "/registr_user_command";
    public static final String VIEW_REGISTER_USER_COMMAND = "/view_register_user_command";
    public static final String VIEW_ALL_USERS_COMMAND = "/view_all_users_command";
    public static final String DELETE_USER_COMMAND = "/delete_user_command";
    public static final String VIEW_ALL_BOOKS_COMMAND = "/view_all_books_command";
    public static final String VIEW_EDIT_USER_COMMAND = "/view_edit_user_command";
    public static final String VIEW_BASKET_COMMAND = "/view_basket_command";
    public static final String ADD_BOOK_TO_BASKET_COMMAND = "/add_book_to_basket_command";
    public static final String DELETE_BOOK_FROM_BASKET_COMMAND = "/delete_book_from_basket_command";
    public static final String ADD_NEW_BOOK_COMMAND = "/add_new_book_command";
    public static final String VIEW_BOOK_ADDITION_FORM_COMMAND = "/view_book_addition_form_command";
    public static final String DELETE_BOOK_COMMAND = "/delete_book_command";
    public static final String WELCOME_COMMAND = "/welcome_command";
    public static final String VIEW_EDIT_BOOK_COMMAND = "/view_edit_book_command";
    public static final String EDIT_BOOK_COMMAND = "/edit_book_command";
    public static final String VIEW_SORTED_BOOKS_COMMAND = "/view_sorted_books_command";
    public static final String VIEW_ERROR_PAGE_COMMAND = "/view_error_page_command";
    public static final String MAIN_COMMAND = "/";

    static {
        ALL_COMMANDS.add(LOGIN_USER_COMMAND);
        ALL_COMMANDS.add(LOGOUT_USER_COMMAND);
        ALL_COMMANDS.add(VIEW_LOGIN_FORM_COMMAND);
        ALL_COMMANDS.add(REGISTER_USER_COMMAND);
        ALL_COMMANDS.add(VIEW_REGISTER_USER_COMMAND);
        ALL_COMMANDS.add(VIEW_ALL_USERS_COMMAND);
        ALL_COMMANDS.add(DELETE_USER_COMMAND);
        ALL_COMMANDS.add(VIEW_ALL_BOOKS_COMMAND);
        ALL_COMMANDS.add(VIEW_EDIT_USER_COMMAND);
        ALL_COMMANDS.add(VIEW_BASKET_COMMAND);
        ALL_COMMANDS.add(ADD_BOOK_TO_BASKET_COMMAND);
        ALL_COMMANDS.add(DELETE_BOOK_FROM_BASKET_COMMAND);
        ALL_COMMANDS.add(ADD_NEW_BOOK_COMMAND);
        ALL_COMMANDS.add(VIEW_BOOK_ADDITION_FORM_COMMAND);
        ALL_COMMANDS.add(DELETE_BOOK_COMMAND);
        ALL_COMMANDS.add(WELCOME_COMMAND);
        ALL_COMMANDS.add(VIEW_EDIT_BOOK_COMMAND);
        ALL_COMMANDS.add(EDIT_BOOK_COMMAND);
        ALL_COMMANDS.add(VIEW_SORTED_BOOKS_COMMAND);
        ALL_COMMANDS.add(VIEW_ERROR_PAGE_COMMAND);
        ALL_COMMANDS.add(MAIN_COMMAND);
    }

}
