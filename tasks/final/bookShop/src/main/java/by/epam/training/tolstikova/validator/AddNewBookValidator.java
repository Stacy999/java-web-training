package by.epam.training.tolstikova.validator;


import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class AddNewBookValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(AddNewBookValidator.class);


    private static final String BOOK_NAME_REGEX = "^[a-zA-Z0-9 ]{1,45}$";
    private static final String BOOK_AUTHOR_REGEX = "^[a-zA-Z0-9 ]{1,45}$";
    private static final String BOOK_CATEGORY_REGEX = "^[a-zA-Z ]{1,45}$";
    private static final String BOOK_PUBLICATION_YEAR_REGEX = "([1-2])([09])([0-9])([0-9])";
    private static final String BOOK_DESCRIPTION_REGEX = "^.{1,500}$";
    private static final String BOOK_PRICE_REGEX = "([0-9]{1,6})([.]?)([0-9]{0,2})";
    private static final String BOOK_PAGES_NUMBER_REGEX = "[0-9]{1,4}";

    public static boolean invalidateBookName(String bookName) {
        return bookName == null || !bookName.matches(BOOK_NAME_REGEX);
    }

    public static boolean invalidateAuthor(String author) {
        return author == null || !author.matches(BOOK_AUTHOR_REGEX);
    }

    public static boolean invalidateCategory(String category) {
        return category == null || !category.matches(BOOK_CATEGORY_REGEX);
    }

    public static boolean invalidatePublicationYear(String publicationYear) {
        return publicationYear == null || !publicationYear.matches(BOOK_PUBLICATION_YEAR_REGEX);
    }

    public static boolean invalidateDescription(String description) {
        return description == null || !description.matches(BOOK_DESCRIPTION_REGEX);
    }

    public static boolean invalidatePrice(String price) {
        return price == null || !price.matches(BOOK_PRICE_REGEX);
    }

    public static boolean invalidatePagesNumber(String pagesNumber) {
        return pagesNumber == null || !pagesNumber.matches(BOOK_PAGES_NUMBER_REGEX);
    }

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();

        if (invalidateBookName(req.getParameter("book.name"))) {
            LOGGER.warn("incorrect book name: " + req.getParameter("book.name"));
            validationResult.addErrorMessage("bookNameError", "Incorrect name: Name can contain" +
                    " only letters and numbers. It must be from 1 to 45 characters");
        }
        if (invalidateAuthor(req.getParameter("book.author"))) {
            LOGGER.warn("incorrect author: " + req.getParameter("book.author"));
            validationResult.addErrorMessage("bookAuthorError", "Incorrect author: Author can contain" +
                    " only letters and numbers. It must be from 1 to 45 characters");
        }
        if (invalidateCategory(req.getParameter("book.category"))) {
            LOGGER.warn("incorrect category: " + req.getParameter("book.category"));
            validationResult.addErrorMessage("bookCategoryError", "Incorrect category: Category must contain" +
                    " only letters. It must be from 1 to 45 characters");
        }
        if (invalidatePagesNumber(req.getParameter("book.numberOfPages"))) {
            LOGGER.warn("incorrect number of pages: " + req.getParameter("book.numberOfPages"));
            validationResult.addErrorMessage("pagesNumberError", "Incorrect number of pages: " +
                    "Number of pages can contain only " +
                    "numbers. It must be from 1 to 4 characters.");
        }
        if (invalidatePublicationYear(req.getParameter("book.publicationYear"))) {
            LOGGER.warn("incorrect publication year: " + req.getParameter("book.publicationYear"));
            validationResult.addErrorMessage("publicationYearError", "Incorrect publication year:" +
                    "Publication year can contain" +
                    " only numbers. It must have 4-symbols length.");
        }
        if (invalidateDescription(req.getParameter("book.description"))) {
            LOGGER.warn("incorrect description: " + req.getParameter("book.description"));
            validationResult.addErrorMessage("descriptionError", "Incorrect description:" +
                    "Description can contain" +
                    " only letters and numbers. It must be from 1 to 500 characters.");
        }
        if (invalidatePrice(req.getParameter("book.price"))) {
            LOGGER.warn("incorrect price: " + req.getParameter("book.price"));
            validationResult.addErrorMessage("priceError", "Incorrect price:" +
                    "Price can contain" +
                    " only numbers and symbol '.'. It must have 2-symbols length.");
        }
        return validationResult;
    }
}
