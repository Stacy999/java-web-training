package by.epam.training.tolstikova.book;

import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewBookAdditionFormCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewBookAdditionFormCommand.class);
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting show addition form.");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/addNewBook.jsp");
        try {
            requestDispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
