package by.epam.training.tolstikova.core;

import by.epam.training.tolstikova.ApplicationConstants;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class SecurityContext {

    private final Map<String, Set<String>> commandsAndRoles = new HashMap<>();

    private static SecurityContext instance;

    private static ReentrantLock lock = new ReentrantLock();

    private final static Logger LOGGER = Logger.getLogger(SecurityContext.class);

    private SecurityContext() {

    }

    public static SecurityContext getInstance() {
        if (instance == null) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new SecurityContext();
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public void init() {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("security.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);

            for (String command : ApplicationConstants.ALL_COMMANDS) {
                String[] roles = properties.getProperty(command.toUpperCase()).split(",");
                if (roles.length == 0) {
                    LOGGER.warn("No properties for this command");
                }
                Set<String> commandRoles = new HashSet<>();
                for (String role : roles) {
                    commandRoles.add(role.trim());
                }
                commandsAndRoles.put(command, commandRoles);
            }
        } catch (IOException e) {
            LOGGER.error("security.properties file cannot be read.", e);
        }
    }

    public Set<String> getRolesForCommand(String commandName) {
        return commandsAndRoles.get(commandName);
    }
}
