package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import by.epam.training.tolstikova.entity.BasketEntity;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.validator.LoginValidator;
import by.epam.training.tolstikova.validator.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoginUserCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(LoginUserCommand.class);
    private LoginValidator loginValidator;
    private UserService userService;

    public LoginUserCommand(UserService service) {

        this.userService = service;
        this.loginValidator = new LoginValidator(service);
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting login user.");
        ValidationResult validationResult;

        validationResult = loginValidator.validate(req);

        if (validationResult.isValid()) {
            HttpSession session = req.getSession();

            UserEntity currentUser = (UserEntity) session.getAttribute("current_user");
            BasketEntity currentBasket = (BasketEntity) session.getAttribute("current_basket");

            if (currentUser == null) {
                try {
                    currentUser = userService.getUserByUserName(req.getParameter("user.login"))
                            .orElse(new UserEntity());
                } catch (ServiceException e) {
                    LOGGER.error("Cannot login user");
                    throw new CommandException(e.getMessage(), e);
                }
            }

            session.setAttribute("current_user", currentUser);

            if (currentBasket == null) {
                currentBasket = new BasketEntity();
            }

            session.setAttribute("current_basket", currentBasket);

            try {
                resp.sendRedirect("welcome_command");
            } catch (IOException e) {
                throw new CommandException("Failed to redirect", e);
            }
        } else {
            try {
                List<String> loginErrors = new ArrayList<>();
                for (Map.Entry<String, String> entry : validationResult.getResult().entrySet()) {
                    loginErrors.add(entry.getValue());
                }
                req.setAttribute("login_errors", loginErrors);

                req.getRequestDispatcher("jsp/logIn.jsp").forward(req, resp);
            } catch (ServletException | IOException e) {
                throw new CommandException("Failed to forward", e);
            }
        }
    }
}

