package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.command.CommandException;
import by.epam.training.tolstikova.command.ServletCommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutUserCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(LogoutUserCommand.class);
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        LOGGER.debug("Starting logout user.");
        HttpSession session = req.getSession();
        session.removeAttribute("current_user");
        session.removeAttribute("current_basket");
        session.invalidate();
        try {
            resp.sendRedirect("welcome_command");
        } catch (IOException e) {
            throw new CommandException("Failed to forward", e);
        }
    }
}
