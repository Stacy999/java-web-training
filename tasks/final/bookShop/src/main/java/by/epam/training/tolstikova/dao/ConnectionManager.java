package by.epam.training.tolstikova.dao;

import java.sql.Connection;

public interface ConnectionManager {

    Connection getConnection() throws PersistException;
}
