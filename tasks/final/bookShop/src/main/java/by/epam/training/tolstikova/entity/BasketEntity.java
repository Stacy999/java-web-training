package by.epam.training.tolstikova.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class BasketEntity {
    private List<BookEntity> books;
    public BasketEntity() {
        books = new ArrayList<>();
    }
}
