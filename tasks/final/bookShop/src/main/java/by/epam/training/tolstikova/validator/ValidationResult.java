package by.epam.training.tolstikova.validator;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ValidationResult {
    private Map<String, String> result;

    public ValidationResult() {
        result = new HashMap<>();
    }

    public boolean isValid() {
        return result.isEmpty();
    }

    public void addErrorMessage(String key, String message) {
        result.put(key, message);
    }

}
