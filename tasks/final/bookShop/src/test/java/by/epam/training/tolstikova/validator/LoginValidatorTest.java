package by.epam.training.tolstikova.validator;

import by.epam.training.tolstikova.book.BookDao;
import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.order.OrderDao;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserDao;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import by.epam.training.tolstikova.user.userInfo.UserInfoDao;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.ResourceBundle;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class LoginValidatorTest {

    private UserService userService;

    @BeforeClass
    public static void init(){
        ConnectionPool.getInstance().init(ResourceBundle.getBundle("database"));
    }

    @Before
    public void setup() throws PersistException {
        TransactionManager transactionManager = new TransactionManagerImpl(ConnectionPool.getInstance());
        transactionManager.beginTransaction();
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        CRUDDao userDao = new UserDao(connectionManager);
        CRUDDao userInfoDao = new UserInfoDao(connectionManager);
        CRUDDao bookDao = new BookDao(connectionManager);
        CRUDDao orderDao = new OrderDao(connectionManager);
        userService = new UserServiceImpl(userDao, userInfoDao, bookDao, orderDao, transactionManager);
    }

    @Test
    public void invalidateData_correctData_false() {
        //arrange
        String login = "Nastya";
        String password = "asdfg";
        boolean expected = false;

        //act
        boolean actual = LoginValidator.invalidateData(login,password);

        //assert
        assertEquals(expected,actual);
    }

    @Test
    public void invalidateData_nullLogin_true() {
        //arrange
        String login = null;
        String password = "asdfg";
        boolean expected = true;

        //act
        boolean actual = LoginValidator.invalidateData(login,password);

        //assert
        assertEquals(expected,actual);
    }

    @Test
    public void validate_correctData_emptyValidationResult() {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("Nastya");
        when(request.getParameter("user.password")).thenReturn("asdfg");
        ValidationResult expected = new ValidationResult();

        LoginValidator loginValidator = new LoginValidator(userService);

        //act
        ValidationResult actual = loginValidator.validate(request);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void validate_incorrectData_notEmptyValidationResult() {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("Nastya");
        when(request.getParameter("user.password")).thenReturn("ghygh66");
        ValidationResult expected = new ValidationResult();

        LoginValidator loginValidator = new LoginValidator(userService);

        //act
        ValidationResult actual = loginValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);
    }
}