package by.epam.training.tolstikova.book;


import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.entity.BookEntity;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class BookDaoTest {

    private BookDao bookDao;
    private TransactionManager transactionManager = new TransactionManagerImpl(ConnectionPool.getInstance());
    private ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);

    @BeforeClass
    public static void init(){
        ConnectionPool.getInstance().init(ResourceBundle.getBundle("database"));
    }

    @Before
    public void setUp() throws PersistException {
        transactionManager.beginTransaction();
        transactionManager.enableAutoCommit();
        bookDao = new BookDao(connectionManager);
    }

    @Test
    public void update() throws PersistException {
        BookEntity bookEntity = BookEntity
                .builder()
                .id(1L)
                .name("1984")
                .author("Georgre Orwell")
                .price(new BigDecimal(9))
                .category("novel")
                .publicationYear(2015)
                .description("The story takes place in an imagined future, the year 1984, when much of the world" +
                        " has fallen victim to perpetual war, omnipresent government surveillance, historical" +
                        " negationism, and propaganda. Great Britain, known as Airstrip One, has become" +
                        " a province of a superstate named Oceania that is ruled by the Party who employ" +
                        " the Thought Police to persecute individuality and independent thinking.")
                .numberOfPages(320)
                .build();

        boolean expected = true;
        boolean actual = bookDao.update(bookEntity);

        assertEquals(expected,actual);
    }
}