package by.epam.training.tolstikova.validator;

import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

public class AddNewBookValidatorTest {

    @Test
    public void invalidateBookName_correctName_false() {
        //arrange
        String name = "Harry Potter";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidateBookName(name);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateBookName_incorrectName_true() {
        //arrange
        String name = "";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidateBookName(name);

        //act
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateAuthor_correctAuthor_false() {
        //arrange
        String author = "William Shakespeare";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidateAuthor(author);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateAuthor_incorrectAuthor_true() {
        //arrange
        String author = "unknown...author";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidateAuthor(author);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateCategory_correct_category_false() {
        //arrange
        String category = "Novel";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidateCategory(category);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateCategory_incorrect_category_true() {
        //arrange
        String category = "***";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidateCategory(category);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePublicationYear_correctYear_false() {
        //arrange
        String publicationYear = "2010";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidatePublicationYear(publicationYear);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePublicationYear_incorrectYear_true() {
        //arrange
        String publicationYear = "hello";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidatePublicationYear(publicationYear);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateDescription_correctDescription_false() {
        //arrange
        String description = "This is very interesting book...";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidateDescription(description);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateDescription_incorrectDescription_true() {
        //arrange
        String description = "";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidateDescription(description);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePrice_correctPrice_false() {
        //arrange
        String price = "50.99";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidatePrice(price);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePrice_incorrectPrice_true() {
        //arrange
        String price = "50.999999999999";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidatePrice(price);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePagesNumber_correctNumber_false() {
        //arrange
        String number = "500";
        boolean expected = false;

        //act
        boolean actual = AddNewBookValidator.invalidatePrice(number);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePagesNumber_incorrectNumber_true() {
        //arrange
        String number = "-200";
        boolean expected = true;

        //act
        boolean actual = AddNewBookValidator.invalidatePrice(number);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void validate_allDataIsCorrect_emptyValidationResult() {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("book.name")).thenReturn("Jane Eyre");
        when(request.getParameter("book.author")).thenReturn("Charlotte Bronte");
        when(request.getParameter("book.category")).thenReturn("novel");
        when(request.getParameter("book.numberOfPages")).thenReturn("608");
        when(request.getParameter("book.publicationYear")).thenReturn("2013");
        when(request.getParameter("book.description")).thenReturn("one of the most touching books");
        when(request.getParameter("book.price")).thenReturn("16.33");

        AddNewBookValidator addNewBookValidator = new AddNewBookValidator();
        ValidationResult expected = new ValidationResult();

        //act
        ValidationResult actual = addNewBookValidator.validate(request);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void validate_DataIsIncorrect_notEmptyValidationResult() {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("book.name")).thenReturn("Jane Eyre");
        when(request.getParameter("book.author")).thenReturn("Charlotte Bronte");
        when(request.getParameter("book.category")).thenReturn("???");
        when(request.getParameter("book.numberOfPages")).thenReturn("many...");
        when(request.getParameter("book.publicationYear")).thenReturn("2013");
        when(request.getParameter("book.description")).thenReturn("one of the most touching books");
        when(request.getParameter("book.price")).thenReturn("expensive!");

        AddNewBookValidator addNewBookValidator = new AddNewBookValidator();
        ValidationResult expected = new ValidationResult();

        //act
        ValidationResult actual = addNewBookValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);
    }
}