package by.epam.training.tolstikova.userInfo;


import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import by.epam.training.tolstikova.user.userInfo.UserInfoDao;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class UserInfoDaoTest {

    private UserInfoDao userInfoDao;
    private TransactionManager transactionManager = new TransactionManagerImpl(ConnectionPool.getInstance());
    private ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);

    @BeforeClass
    public static void init(){
        ConnectionPool.getInstance().init(ResourceBundle.getBundle("database"));
    }

    @Before
    public void setUp() throws PersistException {
        transactionManager.beginTransaction();
        transactionManager.enableAutoCommit();
        userInfoDao = new UserInfoDao(connectionManager);
    }

    @Test
    public void update_existInformation_true() throws PersistException {
        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        boolean expected = true;
        boolean actual = userInfoDao.update(info);

        assertEquals(expected,actual);
    }
}