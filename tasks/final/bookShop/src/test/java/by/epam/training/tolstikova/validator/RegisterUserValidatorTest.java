package by.epam.training.tolstikova.validator;

import by.epam.training.tolstikova.book.BookDao;
import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.order.OrderDao;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.UserDao;
import by.epam.training.tolstikova.user.UserService;
import by.epam.training.tolstikova.user.UserServiceImpl;
import by.epam.training.tolstikova.user.userInfo.UserInfoDao;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

public class RegisterUserValidatorTest {

    private UserService userService;

    @BeforeClass
    public static void init() {
        ConnectionPool.getInstance().init(ResourceBundle.getBundle("database"));
    }

    @Before
    public void setup() throws PersistException {
        TransactionManager transactionManager = new TransactionManagerImpl(ConnectionPool.getInstance());
        transactionManager.beginTransaction();
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        CRUDDao userDao = new UserDao(connectionManager);
        CRUDDao userInfoDao = new UserInfoDao(connectionManager);
        CRUDDao bookDao = new BookDao(connectionManager);
        CRUDDao orderDao = new OrderDao(connectionManager);
        userService = new UserServiceImpl(userDao, userInfoDao, bookDao, orderDao, transactionManager);
    }

    @Test
    public void invalidateUsername_correctName_false() {
        //arrange
        boolean expected = false;
        String userName = "Nastya";

        //act
        boolean actual = RegisterUserValidator.invalidateUsername(userName);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateUsername_incorrectName_true() {
        //arrange
        boolean expected = true;
        String userName = ":)";

        //act
        boolean actual = RegisterUserValidator.invalidateUsername(userName);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePassword_correctPassword_false() {
        //arrange
        boolean expected = false;
        String password = "asd45gh0";

        //act
        boolean actual = RegisterUserValidator.invalidatePassword(password);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePassword_incorrectPassword_true() {
        //arrange
        boolean expected = true;
        String password = "+-*/aaa";

        //act
        boolean actual = RegisterUserValidator.invalidatePassword(password);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateEmail_correctEmail_false() {
        //arrange
        boolean expected = false;
        String email = "somebody@gmail.com";

        //act
        boolean actual = RegisterUserValidator.invalidateEmail(email);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidateEmail_incorrectEmail_true() {
        //arrange
        boolean expected = true;
        String email = "somebodygmail.com";

        //act
        boolean actual = RegisterUserValidator.invalidateEmail(email);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePhone_correctPhone_false() {
        //arrange
        boolean expected = false;
        String phone = "336587498";

        //act
        boolean actual = RegisterUserValidator.invalidatePhone(phone);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void invalidatePhone_incorrectPhone_true() {
        //arrange
        boolean expected = true;
        String phone = "88888";

        //act
        boolean actual = RegisterUserValidator.invalidatePhone(phone);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void validate_correctData_emptyValidationResult() throws ServiceException {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("Potato");
        when(request.getParameter("user.password")).thenReturn("lalala999");
        when(request.getParameter("user.email")).thenReturn("somebody@gmail.com");
        when(request.getParameter("user.phone")).thenReturn("298075623");
        ValidationResult expected = new ValidationResult();

        RegisterUserValidator registerUserValidator = new RegisterUserValidator(userService);

        //act
        ValidationResult actual = registerUserValidator.validate(request);

        //assert
        assertEquals(expected, actual);

    }

    @Test
    public void validate_incorrectData_notEmptyValidationResult() throws ServiceException {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("Potato");
        when(request.getParameter("user.password")).thenReturn("******");
        when(request.getParameter("user.email")).thenReturn("somebody@gmail.com");
        when(request.getParameter("user.phone")).thenReturn("298075623");
        ValidationResult expected = new ValidationResult();

        RegisterUserValidator registerUserValidator = new RegisterUserValidator(userService);

        //act
        ValidationResult actual = registerUserValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);

    }

    @Test
    public void validate_incorrectLogin_notEmptyValidationResult() throws ServiceException {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn(":(");
        when(request.getParameter("user.password")).thenReturn("lalala999");
        when(request.getParameter("user.email")).thenReturn("somebody@gmail.com");
        when(request.getParameter("user.phone")).thenReturn("298075623");
        ValidationResult expected = new ValidationResult();

        RegisterUserValidator registerUserValidator = new RegisterUserValidator(userService);

        //act
        ValidationResult actual = registerUserValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);

    }

    @Test
    public void validate_existingLogin_notEmptyValidationResult() throws ServiceException {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("Nastya");
        when(request.getParameter("user.password")).thenReturn("lalala999");
        when(request.getParameter("user.email")).thenReturn("somebody@gmail.com");
        when(request.getParameter("user.phone")).thenReturn("298075623");
        ValidationResult expected = new ValidationResult();

        RegisterUserValidator registerUserValidator = new RegisterUserValidator(userService);

        //act
        ValidationResult actual = registerUserValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);

    }

    @Test
    public void validate_incorrectEmail_notEmptyValidationResult() throws ServiceException {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("CorrectLogin");
        when(request.getParameter("user.password")).thenReturn("lalala999");
        when(request.getParameter("user.email")).thenReturn("somebodygmailcom");
        when(request.getParameter("user.phone")).thenReturn("298075623");
        ValidationResult expected = new ValidationResult();

        RegisterUserValidator registerUserValidator = new RegisterUserValidator(userService);

        //act
        ValidationResult actual = registerUserValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);

    }
    @Test
    public void validate_incorrectPhone_notEmptyValidationResult() throws ServiceException {
        //arrange
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getParameter("user.login")).thenReturn("CorrectLogin");
        when(request.getParameter("user.password")).thenReturn("lalala999");
        when(request.getParameter("user.email")).thenReturn("somebody@gmail.com");
        when(request.getParameter("user.phone")).thenReturn("");
        ValidationResult expected = new ValidationResult();

        RegisterUserValidator registerUserValidator = new RegisterUserValidator(userService);

        //act
        ValidationResult actual = registerUserValidator.validate(request);

        //assert
        assertNotEquals(expected, actual);

    }
}