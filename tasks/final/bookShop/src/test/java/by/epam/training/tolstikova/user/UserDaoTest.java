package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class UserDaoTest {

    private UserDao userDao;
    private TransactionManager transactionManager = new TransactionManagerImpl(ConnectionPool.getInstance());
    private ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);

    @BeforeClass
    public static void init(){
        ConnectionPool.getInstance().init(ResourceBundle.getBundle("database"));
    }

    @Before
    public void setUp() throws PersistException {
        transactionManager.beginTransaction();
        transactionManager.enableAutoCommit();
        userDao = new UserDao(connectionManager);
    }


    @Test
    public void update_existUser_true() throws PersistException {

        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        UserEntity userEntity = UserEntity
                .builder()
                .id(1L)
                .login("Kirill")
                .password("qwerty")
                .role("user")
                .userInformationEntity(info)
                .build();

        boolean expected = true;
        boolean actual = userDao.update(userEntity);

        assertEquals(expected,actual);
    }

    @Test
    public void update_InvalidUser_false() throws PersistException {

        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        UserEntity userEntity = UserEntity
                .builder()
                .id(2L)
                .login("kkk")
                .password("qwerty")
                .role("user")
                .userInformationEntity(info)
                .build();

        boolean expected = false;
        boolean actual = userDao.update(userEntity);

        assertEquals(expected,actual);
    }

    @After
    public void tearDown(){
        ConnectionPool.getInstance().close();
    }
}