package by.epam.training.tolstikova.user;


import by.epam.training.tolstikova.book.BookDao;
import by.epam.training.tolstikova.dao.*;
import by.epam.training.tolstikova.entity.BookEntity;
import by.epam.training.tolstikova.entity.UserEntity;
import by.epam.training.tolstikova.entity.UserInformationEntity;
import by.epam.training.tolstikova.order.OrderDao;
import by.epam.training.tolstikova.service.ServiceException;
import by.epam.training.tolstikova.user.userInfo.UserInfoDao;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(JUnit4.class)
public class UserServiceImplTest {

    private UserService userService;

    @BeforeClass
    public static void initPool() {
        ConnectionPool.getInstance().init(ResourceBundle.getBundle("database"));
    }

    @Before
    public void init() throws PersistException {
        TransactionManager transactionManager = new TransactionManagerImpl(ConnectionPool.getInstance());
        transactionManager.beginTransaction();
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        CRUDDao userDao = new UserDao(connectionManager);
        CRUDDao userInfoDao = new UserInfoDao(connectionManager);
        CRUDDao bookDao = new BookDao(connectionManager);
        CRUDDao orderDao = new OrderDao(connectionManager);
        userService = new UserServiceImpl(userDao, userInfoDao, bookDao, orderDao, transactionManager);
    }

    private UserEntity userEntity = Mockito.mock(UserEntity.class);
    private UserInformationEntity userInformationEntity = Mockito.mock(UserInformationEntity.class);


    @Test
    public void loginUser_Kirill_loggedIn() throws ServiceException {
        when(userEntity.getLogin()).thenReturn("Kirill");
        when(userEntity.getPassword()).thenReturn("qwerty");

        boolean expected = true;
        boolean actual = userService.loginUser(userEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void loginUser_Nastya_logInFailed() throws ServiceException {
        when(userEntity.getLogin()).thenReturn("Nastya");
        when(userEntity.getPassword()).thenReturn("qwerty");

        boolean expected = false;
        boolean actual = userService.loginUser(userEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void registerUser_newUser_registered() throws ServiceException {
        UserEntity userEntity = UserEntity
                .builder()
                .login("Stacy")
                .password("zxcvbnml")
                .role("user")
                .userInformationEntity(userInformationEntity)
                .build();

        when(userInformationEntity.getFirstName()).thenReturn("Nastya");
        when(userInformationEntity.getLastName()).thenReturn("Tolstikova");
        when(userInformationEntity.getPhone()).thenReturn("5558877");
        when(userInformationEntity.getEmail()).thenReturn("abc@mail.ru");
        when(userInformationEntity.getAddress()).thenReturn("Leschinskogo 17");

        boolean expected = true;
        boolean actual = userService.registerUser(userEntity, userInformationEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void deleteUser_existingUser_true() throws ServiceException {
        UserEntity userEntity = userService.getUserByUserName("Stacy").orElse(new UserEntity());

        boolean expected = true;
        boolean actual = userService.delete(userEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void deleteUser_notExistingUser_false() throws ServiceException {
        UserEntity userEntity = userService.getUserByUserName("Sun").orElse(new UserEntity());
        boolean expected = false;
        boolean actual = userService.delete(userEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void registerUser_existUser_notRegistered() throws ServiceException {
        when(userEntity.getLogin()).thenReturn("Kirill");
        when(userEntity.getPassword()).thenReturn("qwerty");
        when(userEntity.getRole()).thenReturn("admin");
        when(userEntity.getUserInformationEntity()).thenReturn(userInformationEntity);
        when(userEntity.getId()).thenReturn(5L);

        when(userInformationEntity.getFirstName()).thenReturn("Kirill");
        when(userInformationEntity.getLastName()).thenReturn("Kostylev");
        when(userInformationEntity.getPhone()).thenReturn("5558877");
        when(userInformationEntity.getEmail()).thenReturn("kk@gmail.com");
        when(userInformationEntity.getAddress()).thenReturn("Leschinskogo 17");
        when(userInformationEntity.getId()).thenReturn(12L);

        boolean expected = false;
        boolean actual = userService.registerUser(userEntity, userInformationEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void getUserByUserName_existUserName_user() throws ServiceException {
        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        UserEntity expected = UserEntity
                .builder()
                .id(1L)
                .login("Kirill")
                .password("qwerty")
                .role("admin")
                .userInformationEntity(info)
                .build();
        UserEntity actual = userService.getUserByUserName("Kirill").get();

        assertEquals(expected, actual);
    }

    @Test
    public void getUserByUserName_invalidUserName_optionalEmpty() throws ServiceException {

        Optional<UserEntity> expected = Optional.empty();

        Optional<UserEntity> actual = userService.getUserByUserName("kkk");

        assertEquals(expected, actual);
    }

    @Test
    public void getEntityById_existId_user() throws ServiceException {

        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        UserEntity expected = UserEntity
                .builder()
                .id(1L)
                .login("Kirill")
                .password("qwerty")
                .role("admin")
                .userInformationEntity(info)
                .build();

        UserEntity actual = userService.getEntityById(1L).get();

        assertEquals(expected, actual);
    }

    @Test
    public void getEntityById_invalidId_OptionalEmpty() throws ServiceException {
        Optional<UserEntity> expected = Optional.empty();

        Optional<UserEntity> actual = userService.getEntityById(2L);

        assertEquals(expected, actual);
    }

    @Test
    public void update_existUser_true() throws ServiceException {

        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        UserEntity userEntity = UserEntity
                .builder()
                .id(1L)
                .login("Kirill")
                .password("qwerty")
                .role("admin")
                .userInformationEntity(info)
                .build();

        boolean expected = true;
        boolean actual = userService.update(userEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void update_invalidUser_false() throws ServiceException {

        UserInformationEntity info = UserInformationEntity
                .builder()
                .id(1L)
                .firstName("Kirill")
                .lastName("Kostylev")
                .phone("2223366")
                .email("kk99@gmail.com")
                .address("Prititskogo 78")
                .build();

        UserEntity userEntity = UserEntity
                .builder()
                .id(2L)
                .login("kkk")
                .password("qwerty")
                .role("admin")
                .userInformationEntity(info)
                .build();

        boolean expected = false;
        boolean actual = userService.update(userEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void editBook_correctInformation_true() throws ServiceException {
        BookEntity bookEntity = BookEntity
                .builder()
                .id(1L)
                .name("1984")
                .author("Georgre Orwell")
                .price(new BigDecimal(9))
                .category("novel")
                .publicationYear(2015)
                .description("The story takes place in an imagined future, the year 1984, when much of the world" +
                        " has fallen victim to perpetual war, omnipresent government surveillance, historical" +
                        " negationism, and propaganda. Great Britain, known as Airstrip One, has become" +
                        " a province of a superstate named Oceania that is ruled by the Party who employ" +
                        " the Thought Police to persecute individuality and independent thinking.")
                .numberOfPages(320)
                .build();

        boolean expected = true;
        boolean actual = ((UserServiceImpl) userService).editBook(bookEntity);

        assertEquals(expected, actual);
    }

    @Test
    public void editBook_incorrectInformation_false() throws ServiceException {
        BookEntity bookEntity = BookEntity
                .builder()
                .id(10L)
                .name("1984")
                .author("Georgre Orwell")
                .price(new BigDecimal(9))
                .category("novel")
                .publicationYear(2015)
                .description("The story takes place in an imagined future, the year 1984, when much of the world" +
                        " has fallen victim to perpetual war, omnipresent government surveillance, historical" +
                        " negationism, and propaganda. Great Britain, known as Airstrip One, has become" +
                        " a province of a superstate named Oceania that is ruled by the Party who employ" +
                        " the Thought Police to persecute individuality and independent thinking.")
                .numberOfPages(320)
                .build();

        boolean expected = false;
        boolean actual = ((UserServiceImpl) userService).editBook(bookEntity);

        assertEquals(expected, actual);
    }
}