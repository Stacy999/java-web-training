package by.training.tolstikova.task2.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class WordTest {

    private Word word;

    @Before
    public void init() {
        word = new Word("", "Smile", "!");
    }

    @Test
    public void getText() {
        //assert
        assertEquals("Smile!", word.getText());
    }
}