package by.training.tolstikova.task2.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ParagraphTest {

    private Paragraph paragraph;

    @Before
    public void init() {
        paragraph = new Paragraph();
        List<TextLeaf> words = new LinkedList<>();
        words.add(new Word("", "Hello", ""));
        words.add(new Word("", "from", ""));
        words.add(new Word("", "Minsk", "!"));
        paragraph.addText(new Sentence(words));

        List<TextLeaf> words2 = new LinkedList<>();
        words2.add(new Word("", "This", ""));
        words2.add(new Word("", "is", ""));
        words2.add(new Word("", "Java", ")"));
        Sentence sentence = new Sentence(words2);
        paragraph.addText(sentence);
    }

    @Test
    public void addText() {
        List<TextLeaf> words = new LinkedList<>();
        words.add(new Word("", "How", ""));
        words.add(new Word("", "are", ""));
        words.add(new Word("", "you", "?"));
        paragraph.addText(new Sentence(words));

        assertEquals("\tHello from Minsk! This is Java) How are you?", paragraph.getText());
    }

    @Test
    public void getChild() {
        List<TextLeaf> parts = new LinkedList<>();
        parts.add(new Word("", "Hello", ""));
        parts.add(new Word("", "from", ""));
        parts.add(new Word("", "Minsk", "!"));
        Sentence expected = new Sentence(parts);
        assertEquals(expected, paragraph.getChild(0));
    }

    @Test
    public void getText() {
        assertEquals("\tHello from Minsk! This is Java)", paragraph.getText());
    }
}