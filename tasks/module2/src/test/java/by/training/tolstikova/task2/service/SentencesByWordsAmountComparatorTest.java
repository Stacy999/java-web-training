package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.Paragraph;
import by.training.tolstikova.task2.model.Sentence;
import by.training.tolstikova.task2.model.TextLeaf;
import by.training.tolstikova.task2.model.Word;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;

import static by.training.tolstikova.task2.service.SortOrders.DIRECT_ORDER;
import static by.training.tolstikova.task2.service.SortOrders.REVERSE_ORDER;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class SentencesByWordsAmountComparatorTest {

    private SentencesByWordsAmountComparator comparator;
    private SentencesByWordsAmountComparator comparator2;
    private Paragraph paragraph1;
    private Paragraph paragraph2;

    @Before
    public void init() {
        paragraph1 = new Paragraph();
        paragraph2 = new Paragraph();
        List<TextLeaf> words = new LinkedList<>();
        words.add(new Word("", "Hello", ""));
        words.add(new Word("", "from", ""));
        words.add(new Word("", "Minsk", "!"));
        paragraph1.addText(new Sentence(words));

        List<TextLeaf> words2 = new LinkedList<>();
        words2.add(new Word("", "This", ""));
        words2.add(new Word("", "is", ""));
        words2.add(new Word("", "Java", ")"));
        paragraph1.addText(new Sentence(words2));
        paragraph2.addText(new Sentence(words2));

        comparator = new SentencesByWordsAmountComparator(DIRECT_ORDER);
        comparator2 = new SentencesByWordsAmountComparator(REVERSE_ORDER);
    }

    @Test
    public void shouldReturnPositiveResult() {
        //test
        int result = comparator.compare(paragraph1, paragraph2);

        //assert
        assertTrue(result > 0);
    }

    @Test
    public void shouldReturnNegativeResult() {
        //test
        int result = comparator2.compare(paragraph1, paragraph2);

        //assert
        assertTrue(result < 0);
    }
}