package by.training.tolstikova.task2.parser;

import by.training.tolstikova.task2.controller.DataReader;
import by.training.tolstikova.task2.model.Text;
import by.training.tolstikova.task2.repository.ParagraphRepository;
import by.training.tolstikova.task2.repository.SentenceRepository;
import by.training.tolstikova.task2.repository.WordRepository;
import by.training.tolstikova.task2.service.ParagraphService;
import by.training.tolstikova.task2.service.SentenceService;
import by.training.tolstikova.task2.service.WordService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TextParserTest {

    private Parser textParser;

    @Before
    public void init() {
        textParser = new TextParser(new ParagraphService(new ParagraphRepository()));
        Parser paragraphParser = new ParagraphParser(new SentenceService(new SentenceRepository()));
        Parser sentenceParser = new SentenceParser(new WordService(new WordRepository()));

        textParser.setNext(paragraphParser);
        paragraphParser.setNext(sentenceParser);
    }

    @Test
    public void parse() {
        Text text = new Text();
        ClassLoader classLoader = getClass().getClassLoader();
        DataReader dataReader = new DataReader(
                new File(classLoader.getResource("textFromTask.txt").getFile()).toString());
        String allText = dataReader.readAllLines();
        String expected1 = "\tIt has survived not only five centuries, but also the leap into electronic " +
                "typesetting, remaining essentially unchanged. It was popularised in the with the " +
                "release of Letraset sheets containing Lorem Ipsum passages, and more recently with " +
                "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        String expected2 = "\tIt is a long established fact that a reader will be distracted by the readable " +
                "content of a page when looking at its layout. The point of using Ipsum is that it has a " +
                "more - or - less normal distribution of letters, as opposed to using 'Content here, content " +
                "here', making it look like readable English.";
        String expected3 = "\tIt is a established fact that a reader will be of a page when looking at its layout.";

        String expected4 = "\tBye.";

        //test
        textParser.parse(allText, text);

        //assert
        assertEquals(expected1, text.getChild(0).getText());
        assertEquals(expected2, text.getChild(1).getText());
        assertEquals(expected3, text.getChild(2).getText());
        assertEquals(expected4, text.getChild(3).getText());
    }
}