package by.training.tolstikova.task2.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TextTest {

    private Text text;

    @Before
    public void init() {
        text = new Text();
        Paragraph paragraph1 = new Paragraph();
        Paragraph paragraph2 = new Paragraph();

        List<TextLeaf> words = new LinkedList<>();
        words.add(new Word("", "Hello", ""));
        words.add(new Word("", "from", ""));
        words.add(new Word("", "Minsk", "!"));

        List<TextLeaf> words2 = new LinkedList<>();
        words2.add(new Word("", "This", ""));
        words2.add(new Word("", "is", ""));
        words2.add(new Word("", "Java", ")"));
        paragraph1.addText(new Sentence(words));
        paragraph1.addText(new Sentence(words2));
        paragraph2.addText(new Sentence(words2));
        paragraph2.addText(new Sentence(words));

        text.addText(paragraph1);
        text.addText(paragraph2);
    }

    @Test
    public void getChild() {
        Paragraph expected = new Paragraph();
        List<TextLeaf> words = new LinkedList<>();

        words.add(new Word("", "Hello", ""));
        words.add(new Word("", "from", ""));
        words.add(new Word("", "Minsk", "!"));

        List<TextLeaf> words2 = new LinkedList<>();
        words2.add(new Word("", "This", ""));
        words2.add(new Word("", "is", ""));
        words2.add(new Word("", "Java", ")"));
        expected.addText(new Sentence(words));
        expected.addText(new Sentence(words2));

        //assert
        assertEquals(expected, text.getChild(0));
    }

    @Test
    public void getText() {
        //assert
        assertEquals("\tHello from Minsk! This is Java)\n\tThis is Java) Hello from Minsk!", text.getText());
    }

}