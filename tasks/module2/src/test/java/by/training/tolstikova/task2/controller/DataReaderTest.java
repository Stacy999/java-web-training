package by.training.tolstikova.task2.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class DataReaderTest {

    private DataReader dataReader;

    @Before
    public void init() {
        ClassLoader classLoader = getClass().getClassLoader();
        dataReader = new DataReader(new File(classLoader.getResource("text.txt").getFile()).toString());
    }

    @Test
    public void readAllLines() {
        //test
        String actual = dataReader.readAllLines();
        String expected = "Anyone who reads Old and Middle English literary texts will be familiar with the mid-brown " +
                "volumes of the EETS,\nwith the symbol of Alfred's jewel embossed on the front cover. Most of the works " +
                "attributed to King Alfred or to Aelfric,\nalong with some of those by bishop Wulfstan and much anonymous " +
                "prose and verse from the pre-Conquest period,\nare to be found within the Society's three series; " +
                "all of the surviving medieval drama,\nmost of the Middle English romances, much religious " +
                "and secular prose and verse including the English works of John Gower,\nThomas Hoccleve and most of " +
                "Caxton's prints all find their place in the publications." +
                "\n\tWithout EETS editions, study of medieval English texts would hardly be possible.";
        //assert
        assertEquals(expected, actual);
    }
}