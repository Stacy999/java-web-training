package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.Sentence;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.model.TextLeaf;
import by.training.tolstikova.task2.model.Word;
import by.training.tolstikova.task2.repository.SentenceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;

import static by.training.tolstikova.task2.service.SortOrders.DIRECT_ORDER;
import static by.training.tolstikova.task2.service.SortOrders.REVERSE_ORDER;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class SentenceServiceTest {
    private SentenceService sentenceService;
    private List<TextComposite> sentences;
    private List<TextComposite> expected;
    private List<TextComposite> expected2;

    @Before
    public void init() {
        sentenceService = new SentenceService(new SentenceRepository());

        sentences = new LinkedList<>();
        List<TextLeaf> words = new LinkedList<>();
        words.add(new Word("", "Hello", ""));
        words.add(new Word("", "from", ""));
        words.add(new Word("", "Minsk", "!"));
        sentences.add(new Sentence(words));

        List<TextLeaf> words2 = new LinkedList<>();
//        words2.add(new Word("","This",""));
//        words2.add(new Word("","is",""));
        words2.add(new Word("", "Java", ")"));
        sentences.add(new Sentence(words2));

        expected = new LinkedList<>();
        expected.add(new Sentence(words2));
        expected.add(new Sentence(words));

        expected2 = new LinkedList<>();
        expected2.add(new Sentence(words));
        expected2.add(new Sentence(words2));
    }

    @Test
    public void sortTextPartsInDirectOrder() {
        sentenceService.sortTextParts(new SentencesByWordsAmountComparator(DIRECT_ORDER), sentences);

        //assert
        assertEquals(expected, sentences);
    }

    @Test
    public void sortTextPartsInReverseOrder() {
        sentenceService.sortTextParts(new SentencesByWordsAmountComparator(REVERSE_ORDER), sentences);

        //assert
        assertEquals(expected2, sentences);
    }
}