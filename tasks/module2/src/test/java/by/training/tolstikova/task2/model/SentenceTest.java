package by.training.tolstikova.task2.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class SentenceTest {
    private Sentence sentence;

    @Before
    public void init() {
        sentence = new Sentence();
        sentence.addText(new Word("", "Hello", ","));
        sentence.addText(new Word("", "world", ""));
    }

    @Test
    public void getChild() {
        TextLeaf word = sentence.getChild(1);

        //assert
        assertEquals(new Word("", "world", ""), word);
    }

    @Test
    public void getText() {
        String actual = sentence.getText();

        //assert
        assertEquals("Hello, world", actual);
    }

    @Test
    public void addText() {
        sentence.addText(new Word("", "This is Java", "!!!"));
        String actual = sentence.getText();

        //assert
        assertEquals("Hello, world This is Java!!!", actual);
    }
}