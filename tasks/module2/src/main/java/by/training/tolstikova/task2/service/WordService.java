package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.model.Word;
import by.training.tolstikova.task2.repository.WordRepository;

import java.util.Comparator;
import java.util.List;

public class WordService implements CommonService<Word> {
    private WordRepository wordRepository;

    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    @Override
    public void save(Word word) {
        wordRepository.add(word);
    }

    @Override
    public void remove(Word word) {
        wordRepository.remove(word);
    }

    @Override
    public List<Word> getAll() {
        return wordRepository.getAll();
    }

    @Override
    public void sortTextParts(Comparator<TextComposite> comparator, List<TextComposite> words) {
        words.sort(comparator);
    }
}
