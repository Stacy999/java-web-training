package by.training.tolstikova.task2.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Sentence implements TextComposite {
    private static final String SEPARATOR = " ";
    private List<TextLeaf> sentence = new LinkedList<>();
    private long sentenceId;

    public Sentence() {
    }

    public Sentence(List<TextLeaf> sentence) {
        this.sentence = sentence;
    }

    public TextLeaf getChild(int index) {
        return sentence.get(index);
    }

    public String getText() {
        StringJoiner stringJoiner = new StringJoiner(SEPARATOR);
        for (TextLeaf word : sentence) {
            stringJoiner.add(word.getText());
        }
        return stringJoiner.toString();
    }

    public void addText(TextLeaf component) {
        sentence.add(component);
    }

    public long getId() {
        return sentenceId;
    }

    public void setId(long id) {
        this.sentenceId = id;
    }

    @Override
    public List<TextLeaf> getLeaves() {
        return sentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sentence)) return false;
        Sentence sentence1 = (Sentence) o;
        return sentenceId == sentence1.sentenceId &&
                Objects.equals(sentence, sentence1.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence, sentenceId);
    }

}
