package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.TextComposite;

import java.util.Comparator;

import static by.training.tolstikova.task2.service.SortOrders.DIRECT_ORDER;
import static by.training.tolstikova.task2.service.SortOrders.REVERSE_ORDER;

public class SentencesByWordsAmountComparator implements Comparator<TextComposite> {
    private SortOrders sortOrder;

    SentencesByWordsAmountComparator(SortOrders sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(TextComposite o1, TextComposite o2) {
        if (sortOrder == DIRECT_ORDER) {
            return Integer.compare(o1.getLeaves().size(), o2.getLeaves().size());
        } else if (sortOrder == REVERSE_ORDER) {
            return Integer.compare(o2.getLeaves().size(), o1.getLeaves().size());
        }
        return 0;
    }

}
