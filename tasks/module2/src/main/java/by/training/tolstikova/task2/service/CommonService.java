package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.TextComposite;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public interface CommonService<T> {

    void save(T leaf);

    void remove(T leaf);

    List<T> getAll();

    default List<T> sortTextParts(Comparator<TextComposite> comparator) {
        return new LinkedList<>();
    }

    void sortTextParts(Comparator<TextComposite> comparator, List<TextComposite> sentences);
}
