package by.training.tolstikova.task2.parser;

import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.model.TextLeaf;
import by.training.tolstikova.task2.model.Word;
import by.training.tolstikova.task2.service.CommonService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser implements Parser {
    private static final String REGEX = "([\\'\\(]+)?(\\w+|[\\-])([\\.\\,\\'\\)\\:]+)?";
    private CommonService sentenceService;

    public SentenceParser(CommonService sentenceService) {
        this.sentenceService = sentenceService;
    }

    @Override
    public void setNext(Parser nextParser) {
    }

    @Override
    public void parse(String text, TextComposite newComponent) {
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String beforeWord = matcher.group(1);
            String wordText = matcher.group(2);
            String afterWord = matcher.group(3);
            if (beforeWord == null) {
                beforeWord = "";
            }
            if (afterWord == null) {
                afterWord = "";
            }
            TextLeaf word = new Word(beforeWord, wordText, afterWord);
            newComponent.addText(word);
            sentenceService.save(word);
        }
    }
}
