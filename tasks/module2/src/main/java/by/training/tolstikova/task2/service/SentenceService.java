package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.Sentence;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.repository.SentenceRepository;

import java.util.Comparator;
import java.util.List;

public class SentenceService implements CommonService<Sentence> {
    private SentenceRepository sentenceRepository;

    public SentenceService(SentenceRepository sentenceRepository) {
        this.sentenceRepository = sentenceRepository;
    }

    @Override
    public void save(Sentence sentence) {
        sentenceRepository.add(sentence);
    }

    @Override
    public List<Sentence> sortTextParts(Comparator<TextComposite> comparator) {
        List<Sentence> paragraphs = sentenceRepository.getAll();
        paragraphs.sort(comparator);
        return paragraphs;
    }

    public void sortTextParts(Comparator<TextComposite> comparator, List<TextComposite> sentences) {
        sentences.sort(comparator);
    }

    @Override
    public void remove(Sentence sentence) {
        sentenceRepository.remove(sentence);
    }

    @Override
    public List<Sentence> getAll() {
        return sentenceRepository.getAll();
    }
}
