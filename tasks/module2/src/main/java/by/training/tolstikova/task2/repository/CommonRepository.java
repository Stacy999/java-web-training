package by.training.tolstikova.task2.repository;

import java.util.List;
import java.util.Optional;

public interface CommonRepository<T> {
    void add(T item);

    void remove(T item);

    int size();

    List<T> getAll();

    Optional<T> find(long id);
}
