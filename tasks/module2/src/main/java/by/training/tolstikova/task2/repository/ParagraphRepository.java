package by.training.tolstikova.task2.repository;

import by.training.tolstikova.task2.model.Paragraph;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class ParagraphRepository implements CommonRepository<Paragraph> {
    private List<Paragraph> paragraphs;
    private AtomicLong id;

    public ParagraphRepository() {
        paragraphs = new LinkedList<>();
        id = new AtomicLong(0);
    }

    private void setIdToTextPart(Paragraph newParagraph) {
        newParagraph.setId(id.incrementAndGet());
    }

    @Override
    public void add(Paragraph paragraph) {
        if (paragraph.getId() == 0) {
            setIdToTextPart(paragraph);
        }
        paragraphs.add(paragraph);
    }

    @Override
    public void remove(Paragraph paragraph) {
        paragraphs.remove(paragraph);
    }

    @Override
    public int size() {
        return paragraphs.size();
    }

    @Override
    public List<Paragraph> getAll() {
        return new LinkedList<>(paragraphs);
    }

    @Override
    public Optional<Paragraph> find(long id) {
        return paragraphs.stream().filter(paragraph -> paragraph.getId() == id).findFirst();
    }
}
