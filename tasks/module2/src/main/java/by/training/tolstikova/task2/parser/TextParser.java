package by.training.tolstikova.task2.parser;

import by.training.tolstikova.task2.model.Paragraph;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.service.CommonService;

public class TextParser implements Parser {
    private static final String REGEX = "(?<=\\n)(\\s{4,}|\\t)(?=\\w)";
    private CommonService textService;

    private Parser next;

    public TextParser(CommonService textService) {
        this.textService = textService;
    }

    @Override
    public void setNext(Parser nextParser) {
        next = nextParser;
    }

    @Override
    public void parse(String text, TextComposite newComponent) {
        for (String elem : text.split(REGEX)) {
            TextComposite paragraph = new Paragraph();
            newComponent.addText(paragraph);
            if (next != null) {
                next.parse(elem.trim(), paragraph);
            }
            textService.save(paragraph);
        }
    }
}
