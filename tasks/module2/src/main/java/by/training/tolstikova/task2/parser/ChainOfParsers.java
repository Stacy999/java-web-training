package by.training.tolstikova.task2.parser;

import by.training.tolstikova.task2.controller.DataReader;
import by.training.tolstikova.task2.model.Text;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.service.CommonService;

public class ChainOfParsers {
    private static final ChainOfParsers INSTANCE = new ChainOfParsers();

    private ChainOfParsers() {

    }

    public static ChainOfParsers getINSTANCE() {
        return INSTANCE;
    }

    public TextComposite createText(String filePath, CommonService<Text> textService, CommonService paragraphService,
                                    CommonService sentenceService, CommonService wordService) {
        DataReader dataReader = new DataReader(filePath);
        String allText = dataReader.readAllLines();
        Text text = new Text();
        TextParser textParser = new TextParser(paragraphService);
        ParagraphParser paragraphParser = new ParagraphParser(sentenceService);
        SentenceParser sentenceParser = new SentenceParser(wordService);

        textParser.setNext(paragraphParser);
        paragraphParser.setNext(sentenceParser);
        textParser.parse(allText, text);
        textService.save(text);
        return text;
    }
}
