package by.training.tolstikova.task2.service;

public enum SortOrders {
    DIRECT_ORDER, REVERSE_ORDER
}
