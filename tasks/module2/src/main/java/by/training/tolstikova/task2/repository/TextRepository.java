package by.training.tolstikova.task2.repository;

import by.training.tolstikova.task2.model.Text;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class TextRepository implements CommonRepository<Text> {
    private List<Text> texts;
    private AtomicLong id;

    public TextRepository() {
        texts = new LinkedList<>();
        id = new AtomicLong(0);
    }

    private void setIdToTextPart(Text newText) {
        newText.setId(id.incrementAndGet());
    }

    @Override
    public void add(Text text) {
        if (text.getId() == 0) {
            setIdToTextPart(text);
        }
        texts.add(text);
    }

    @Override
    public void remove(Text text) {
        texts.remove(text);
    }

    @Override
    public int size() {
        return texts.size();
    }

    @Override
    public List<Text> getAll() {
        return new LinkedList<>(texts);
    }

    @Override
    public Optional<Text> find(long id) {
        return texts.stream().filter(text -> text.getId() == id).findFirst();
    }
}


