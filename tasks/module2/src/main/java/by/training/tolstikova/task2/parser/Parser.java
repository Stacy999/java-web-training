package by.training.tolstikova.task2.parser;

import by.training.tolstikova.task2.model.TextComposite;

public interface Parser {
    void setNext(Parser nextParser);

    void parse(String text, TextComposite newComponent);

}
