package by.training.tolstikova.task2.repository;

import by.training.tolstikova.task2.model.Word;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class WordRepository implements CommonRepository<Word> {
    private List<Word> words;
    private AtomicLong id;

    public WordRepository() {
        words = new LinkedList<>();
        id = new AtomicLong(0);
    }

    private void setIdToTextPart(Word newWord) {
        newWord.setId(id.incrementAndGet());
    }

    @Override
    public void add(Word newWord) {
        if (newWord.getId() == 0) {
            setIdToTextPart(newWord);
        }
        words.add(newWord);
    }

    @Override
    public void remove(Word newWord) {
        words.remove(newWord);
    }

    @Override
    public int size() {
        return words.size();
    }

    @Override
    public List<Word> getAll() {
        return new LinkedList<>(words);
    }

    @Override
    public Optional<Word> find(long id) {
        return words.stream().filter(word -> word.getId() == id).findFirst();
    }
}
