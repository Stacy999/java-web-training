package by.training.tolstikova.task2.controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataReader {

    private static final Logger LOGGER = Logger.getLogger(DataReader.class);
    private String fileName;

    public DataReader() {
    }

    public DataReader(String fileName) {
        this.fileName = fileName;
    }

    public String readAllLines() {
        String receivedString = "";
        try {
            receivedString = new String(Files.readAllBytes(Paths.get(fileName)),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return receivedString;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
