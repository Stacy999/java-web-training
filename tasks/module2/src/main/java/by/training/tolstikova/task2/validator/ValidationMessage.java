package by.training.tolstikova.task2.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ValidationMessage {
    private List<String> messageList;

    ValidationMessage() {
        messageList = new ArrayList<>();
    }

    void addMessage(String msg) {
        if (msg != null) {
            messageList.add(msg);
        }
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValidationMessage)) return false;
        ValidationMessage that = (ValidationMessage) o;
        return Objects.equals(messageList, that.messageList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageList);
    }

    @Override
    public String toString() {
        return "ValidationMessage{" +
                messageList +
                '}';
    }
}
