package by.training.tolstikova.task2.parser;

import by.training.tolstikova.task2.model.Sentence;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.service.CommonService;

public class ParagraphParser implements Parser {
    private static final String REGEX = "(?<=([.!?\\t]))\\s";

    private Parser next;
    private CommonService paragraphService;

    public ParagraphParser(CommonService paragraphService) {
        this.paragraphService = paragraphService;
    }

    @Override
    public void setNext(Parser nextParser) {
        next = nextParser;
    }

    @Override
    public void parse(String text, TextComposite newComponent) {
        for (String elem : text.split(REGEX)) {
            TextComposite sentence = new Sentence();
            newComponent.addText(sentence);
            if (next != null) {
                next.parse(elem.trim(), sentence);
            }
            paragraphService.save(sentence);
        }
    }
}
