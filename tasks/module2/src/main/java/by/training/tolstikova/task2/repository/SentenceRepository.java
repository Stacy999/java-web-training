package by.training.tolstikova.task2.repository;

import by.training.tolstikova.task2.model.Sentence;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class SentenceRepository implements CommonRepository<Sentence> {
    private List<Sentence> sentences;
    private AtomicLong id;

    public SentenceRepository() {
        sentences = new LinkedList<>();
        id = new AtomicLong(0);
    }

    private void setIdToTextPart(Sentence newSentence) {
        newSentence.setId(id.incrementAndGet());
    }

    @Override
    public void add(Sentence sentence) {
        if (sentence.getId() == 0) {
            setIdToTextPart(sentence);
        }
        sentences.add(sentence);
    }

    @Override
    public void remove(Sentence sentence) {
        sentences.remove(sentence);
    }

    @Override
    public int size() {
        return sentences.size();
    }

    @Override
    public List<Sentence> getAll() {
        return new LinkedList<>(sentences);
    }

    @Override
    public Optional<Sentence> find(long id) {
        return sentences.stream().filter(sentence -> sentence.getId() == id).findFirst();
    }
}
