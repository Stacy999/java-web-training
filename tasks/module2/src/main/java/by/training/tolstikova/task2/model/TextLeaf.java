package by.training.tolstikova.task2.model;

public interface TextLeaf {
    String getText();

    long getId();
}
