package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.Text;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.repository.TextRepository;

import java.util.Comparator;
import java.util.List;

public class TextService implements CommonService<Text> {
    private TextRepository repository;

    public TextService(TextRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Text text) {
        repository.add(text);
    }

    @Override
    public void remove(Text text) {
        repository.remove(text);
    }

    @Override
    public List<Text> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Text> sortTextParts(Comparator<TextComposite> comparator) {
        List<Text> texts = repository.getAll();
        texts.sort(comparator);
        return texts;
    }

    @Override
    public void sortTextParts(Comparator<TextComposite> comparator, List<TextComposite> texts) {
        texts.sort(comparator);
    }
}
