package by.training.tolstikova.task2.controller;

import by.training.tolstikova.task2.model.*;
import by.training.tolstikova.task2.parser.ChainOfParsers;
import by.training.tolstikova.task2.service.*;
import by.training.tolstikova.task2.validator.FileValidator;
import by.training.tolstikova.task2.validator.ValidationResult;

import java.util.stream.Stream;

public class TextController {
    private CommonService<Text> textService;
    private CommonService<Paragraph> paragraphService;
    private CommonService<Sentence> sentenceService;
    private CommonService<Word> wordService;
    private FileValidator fileValidator;
    private ChainOfParsers chainOfParsers;

    public TextController(TextService textService, ParagraphService paragraphService,
                          SentenceService sentenceService, WordService wordService,
                          FileValidator fileValidator, ChainOfParsers chainOfParsers) {
        this.textService = textService;
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
        this.wordService = wordService;
        this.fileValidator = fileValidator;
        this.chainOfParsers = chainOfParsers;

    }

    public void workWithText(String path) {
        ValidationResult validationResult = fileValidator.validateFile(path);

        if (validationResult.isValid()) {
            TextComposite newText = chainOfParsers.createText(
                    path, textService, paragraphService, sentenceService, wordService);
        }
    }
}
