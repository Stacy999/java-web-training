package by.training.tolstikova.task2.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;

public class Text implements TextComposite {
    private static final String SEPARATOR = "\n";
    private static AtomicLong id = new AtomicLong(0);
    private long textId = id.getAndIncrement();
    private List<TextLeaf> paragraphs = new LinkedList<>();

    public void addText(TextLeaf component) {
        paragraphs.add(component);
    }

    public TextLeaf getChild(int index) {
        return paragraphs.get(index);
    }

    public String getText() {
        StringJoiner stringJoiner = new StringJoiner(SEPARATOR);
        for (TextLeaf paragraph : paragraphs) {
            stringJoiner.add(paragraph.getText());
        }
        return stringJoiner.toString();
    }

    public long getId() {
        return textId;
    }

    public void setId(long id) {
        this.textId = id;
    }

    @Override
    public List<TextLeaf> getLeaves() {
        return paragraphs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Text)) return false;
        Text text = (Text) o;
        return textId == text.textId &&
                Objects.equals(paragraphs, text.paragraphs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(textId, paragraphs);
    }

}
