package by.training.tolstikova.task2.model;

import java.util.List;

public interface TextComposite extends TextLeaf {
    void addText(TextLeaf text);

    TextLeaf getChild(int index);

    List<TextLeaf> getLeaves();
}
