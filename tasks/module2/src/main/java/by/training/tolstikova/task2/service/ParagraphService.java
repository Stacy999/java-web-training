package by.training.tolstikova.task2.service;

import by.training.tolstikova.task2.model.Paragraph;
import by.training.tolstikova.task2.model.TextComposite;
import by.training.tolstikova.task2.repository.ParagraphRepository;

import java.util.Comparator;
import java.util.List;

public class ParagraphService implements CommonService<Paragraph> {
    private ParagraphRepository repository;

    public ParagraphService(ParagraphRepository repository) {
        this.repository = repository;
    }

    public void save(Paragraph paragraph) {
        repository.add(paragraph);
    }

    public void remove(Paragraph paragraph) {
        repository.remove(paragraph);
    }

    public List<Paragraph> getAll() {
        return repository.getAll();
    }

    public List<Paragraph> sortTextParts(Comparator<TextComposite> comparator) {
        List<Paragraph> paragraphs = repository.getAll();
        paragraphs.sort(comparator);
        return paragraphs;
    }

    @Override
    public void sortTextParts(Comparator<TextComposite> comparator, List<TextComposite> paragraphs) {
        paragraphs.sort(comparator);
    }
}
