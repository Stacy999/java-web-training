package by.training.tolstikova.task2.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Paragraph implements TextComposite {

    private static final String SEPARATOR = "\t";
    private long paragraphId;

    private List<TextLeaf> paragraph = new LinkedList<>();

    public void addText(TextLeaf component) {
        paragraph.add(component);
    }

    public TextLeaf getChild(int index) {
        return paragraph.get(index);
    }

    public String getText() {
        StringJoiner stringJoiner = new StringJoiner(" ", SEPARATOR, "");
        for (TextLeaf sentence : paragraph) {
            stringJoiner.add(sentence.getText());
        }
        return stringJoiner.toString();
    }

    public long getId() {
        return paragraphId;
    }

    public void setId(long id) {
        this.paragraphId = id;
    }

    @Override
    public List<TextLeaf> getLeaves() {
        return paragraph;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Paragraph)) return false;
        Paragraph paragraph1 = (Paragraph) o;
        return paragraphId == paragraph1.paragraphId &&
                Objects.equals(paragraph, paragraph1.paragraph);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paragraphId, paragraph);
    }

}
