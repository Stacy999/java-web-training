package by.training.tolstikova.task2.model;

import java.util.Objects;

public class Word implements TextLeaf {

    private String word;
    private String starting;
    private String ending;
    private long wordId;

    public Word(String starting, String word, String ending) {
        this.starting = starting;
        this.word = word;
        this.ending = ending;
    }

    public String getText() {
        return starting + word + ending;
    }

    public long getId() {
        return wordId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word1 = (Word) o;
        return wordId == word1.wordId &&
                Objects.equals(word, word1.word) &&
                Objects.equals(starting, word1.starting) &&
                Objects.equals(ending, word1.ending);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, starting, ending, wordId);
    }

    public void setId(long id) {
        this.wordId = id;
    }

}
