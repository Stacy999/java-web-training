package by.epam.training.tolstikova.multithreading.controller;

import by.epam.training.tolstikova.multithreading.model.*;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class LogisticsBaseController {
    public static void main(String[] args) {
        LinkedList<Terminal> terminals = new LinkedList<Terminal>() {
            {
                this.add(new Terminal(1));
                this.add(new Terminal(2));
                this.add(new Terminal(3));
                this.add(new Terminal(4));
            }
        };

        LogisticsBase logisticsBase = new LogisticsBase(terminals);
        Queue<Van> vanQueue = new PriorityQueue<>(10, new VanComparator());

        for (int i = 0; i < 5; i++) {
            Van van1 = new Van(logisticsBase, TerminalOperations.LOAD_THE_VAN, false);
            Van van2 = new Van(logisticsBase, TerminalOperations.UNLOAD_THE_VAN, true);
            vanQueue.add(van1);
            vanQueue.add(van2);
        }

        int vansCount = vanQueue.size();

        for (int i = 0; i < vansCount; i++) {
            Thread thread = new Thread(vanQueue.poll());
            thread.start();
        }
    }
}
