package by.epam.training.tolstikova.multithreading.model;

import java.util.Comparator;

public class VanComparator implements Comparator<Van> {
    @Override
    public int compare(Van van1, Van van2) {
        if (van1.isPerishable() && !van2.isPerishable()) {
            return -1;
        } else if (!van1.isPerishable() && van2.isPerishable()) {
            return 1;
        } else {
            return 0;
        }
    }
}
