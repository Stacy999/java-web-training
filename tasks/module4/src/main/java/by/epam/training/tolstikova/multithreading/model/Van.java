package by.epam.training.tolstikova.multithreading.model;

import org.apache.log4j.Logger;

import java.util.concurrent.atomic.AtomicLong;

public class Van implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Van.class);
    private static AtomicLong id = new AtomicLong(0);
    private long vanId = id.incrementAndGet();
    private TerminalOperations terminalOperation;
    private boolean perishable;
    private LogisticsBase logisticsBase;

    public Van(LogisticsBase logisticsBase, TerminalOperations terminalOperation, boolean perishable) {
        this.logisticsBase = logisticsBase;
        this.terminalOperation = terminalOperation;
        this.perishable = perishable;
    }

    public void run() {
        Terminal terminal = logisticsBase.getTerminal();
        LOGGER.info("The van #" + this.vanId + " took the terminal #" + terminal.getTerminalId());
        if (terminalOperation.equals(TerminalOperations.LOAD_THE_VAN)) {
            terminal.loadTheVan();
        } else if (terminalOperation.equals(TerminalOperations.UNLOAD_THE_VAN)) {
            terminal.unloadTheVan();
        }

        LOGGER.info("The van #" + this.vanId + " left the terminal #" + terminal.getTerminalId());
        logisticsBase.returnTerminal(terminal);
    }

    boolean isPerishable() {
        return perishable;
    }

}
