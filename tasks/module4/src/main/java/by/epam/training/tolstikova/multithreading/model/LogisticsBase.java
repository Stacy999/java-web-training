package by.epam.training.tolstikova.multithreading.model;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class LogisticsBase {
    private static final Logger LOGGER = Logger.getLogger(LogisticsBase.class);
    private static final int TERMINAL_COUNT = 4;
    private final Semaphore semaphore = new Semaphore(TERMINAL_COUNT, true);
    private final Queue<Terminal> terminals = new LinkedList<>();

    public LogisticsBase(Queue<Terminal> terminals) {
        this.terminals.addAll(terminals);
    }

    Terminal getTerminal() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
        return terminals.poll();
    }

    void returnTerminal(Terminal terminal) {
        terminals.add(terminal);
        semaphore.release();
    }
}
