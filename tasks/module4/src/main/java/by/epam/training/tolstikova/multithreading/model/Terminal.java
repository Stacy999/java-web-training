package by.epam.training.tolstikova.multithreading.model;

import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class Terminal {
    private static final Logger LOGGER = Logger.getLogger(Terminal.class);
    private long terminalId;

    public Terminal(long terminalId) {
        super();
        this.terminalId = terminalId;
    }

    long getTerminalId() {
        return terminalId;
    }


    void loadTheVan() {
        try {
            LOGGER.info("Terminal #" + terminalId + " is loading the van...");
            TimeUnit.SECONDS.sleep(new java.util.Random().nextInt(10));
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
    }

    void unloadTheVan() {
        try {
            LOGGER.info("Terminal #" + terminalId + " is unloading the van...");
            TimeUnit.SECONDS.sleep(new java.util.Random().nextInt(10));
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
