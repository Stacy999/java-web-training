package by.epam.training.tolstikova.task3.parser;

public class DOMParserException extends ParserException {
    public DOMParserException() {
    }

    public DOMParserException(String message) {
        super(message);
    }

    public DOMParserException(Throwable cause) {
        super(cause);
    }
}
