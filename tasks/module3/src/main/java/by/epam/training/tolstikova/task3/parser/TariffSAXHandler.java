package by.epam.training.tolstikova.task3.parser;

import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.entity.TariffWithPayroll;
import by.epam.training.tolstikova.task3.entity.TariffWithoutPayroll;
import by.epam.training.tolstikova.task3.entity.Tariffing;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedHashSet;
import java.util.Set;

public class TariffSAXHandler extends DefaultHandler {
    private static final Logger LOGGER = Logger.getLogger(TariffSAXHandler.class);

    private Set<Tariff> tariffs = new LinkedHashSet<>();
    private String thisElement;
    private Tariff tariff;

    @Override
    public void startDocument() {
        LOGGER.info("Start parsing document..");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        thisElement = qName;
        if (thisElement != null) {
            if (thisElement.equals("tariffWithoutPayroll")) {
                tariff = new TariffWithoutPayroll();
                tariff.setId(Integer.parseInt(attributes.getValue(
                        "id")));
                tariff.setOperatorName(attributes.getValue("operatorName"));
            } else if (thisElement.equals("tariffWithPayroll")) {
                tariff = new TariffWithPayroll();
                tariff.setId(Integer.parseInt(attributes.getValue(
                        "id")));
                tariff.setOperatorName(attributes.getValue("operatorName"));
            }
        } else {
            LOGGER.warn("Empty element");
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (thisElement != null) {
            String string = new String(ch, start, length);
            if (thisElement.equals("name")) {
                tariff.setName(string);
            }
            if (thisElement.equals("insideTheNetworkCallsPrice")) {
                tariff.setInsideTheNetworkCallsPrice(Double.parseDouble(string));
            }
            if (thisElement.equals("toOtherNetworksCallsPrice")) {
                tariff.setToOtherNetworksCallsPrice(Double.parseDouble(string));
            }
            if (thisElement.equals("landlinePhoneCallsPrice")) {
                tariff.setLandlinePhoneCallsPrice(Double.parseDouble(string));
            }
            if (thisElement.equals("smsPrice")) {
                tariff.setSmsPrise(Double.parseDouble(string));
            }
            if (thisElement.equals("tariffing")) {
                tariff.setTariffing(Tariffing.valueOf(string));
            }
            if (thisElement.equals("favouriteNumber")) {
                tariff.setFavouriteNumber(Boolean.parseBoolean(string));
            }
            if (thisElement.equals("connectionFee")) {
                tariff.setConnectionFee(Double.parseDouble(string));
            }
            if (thisElement.equals("subscriptionCost")) {
                ((TariffWithoutPayroll) tariff).setSubscriptionCost(Double.parseDouble(string));
            }
            if (thisElement.equals("subscriptionDays")) {
                ((TariffWithoutPayroll) tariff).setSubscriptionDays(Integer.parseInt(string));
            }
            if (thisElement.equals("payroll")) {
                ((TariffWithPayroll) tariff).setPayroll(Double.parseDouble(string));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        thisElement = null;
        if (qName.equals("tariffWithoutPayroll")
                || qName.equals("tariffWithPayroll")) {
            tariffs.add(tariff);
        }
    }

    @Override
    public void endDocument() {
        LOGGER.info("End parsing document..");
    }

    Set<Tariff> getTariffs() {
        return tariffs;
    }
}

