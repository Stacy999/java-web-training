package by.epam.training.tolstikova.task3.parser;

import by.epam.training.tolstikova.task3.entity.Tariff;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

public class SAXParser implements SimpleParser<Tariff> {
    private static final Logger LOGGER = Logger.getLogger(SAXParser.class);

    private Set<Tariff> tariffs = new LinkedHashSet<>();
    private TariffSAXHandler tariffSAXHandler;
    private XMLReader xmlReader;

    public SAXParser() {
        tariffSAXHandler = new TariffSAXHandler();
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            xmlReader = saxParserFactory.newSAXParser().getXMLReader();
            xmlReader.setContentHandler(tariffSAXHandler);
        } catch (SAXException | ParserConfigurationException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public Set<Tariff> parse(String pathToXml) throws SAXParserException {
        try {
            xmlReader.parse(pathToXml);
        } catch (IOException | SAXException e) {
            throw new SAXParserException(e);
        }
        tariffs = tariffSAXHandler.getTariffs();
        return tariffs;
    }
}
