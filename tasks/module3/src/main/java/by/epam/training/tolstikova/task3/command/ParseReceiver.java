package by.epam.training.tolstikova.task3.command;

import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.parser.*;
import by.epam.training.tolstikova.task3.validator.XmlByXsdValidator;

import java.util.Set;

public class ParseReceiver {
    private SimpleParser<Tariff> parser;
    private XmlByXsdValidator validator;

    public ParseReceiver(XmlByXsdValidator validator) {
        this.validator = validator;
    }

    Set<Tariff> parseDOM(String pathToXml) throws CommandException {
        if (!validator.validate()) {
            throw new CommandException("XML file is not valid");
        }

        try {
            parser = new DOMParser();
            return parser.parse(pathToXml);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }

    Set<Tariff> parseSAX(String pathToXml) throws CommandException {
        if (!validator.validate()) {
            throw new CommandException("XML file is not valid");
        }

        try {
            parser = new SAXParser();
            return parser.parse(pathToXml);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }

    Set<Tariff> parseStAX(String pathToXml) throws CommandException {
        if (!validator.validate()) {
            throw new CommandException("XML file is not valid");
        }

        try {
            parser = new StAXParser();
            return parser.parse(pathToXml);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }

}
