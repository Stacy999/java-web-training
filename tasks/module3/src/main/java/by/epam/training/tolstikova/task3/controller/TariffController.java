package by.epam.training.tolstikova.task3.controller;

import by.epam.training.tolstikova.task3.command.Command;
import by.epam.training.tolstikova.task3.command.CommandException;
import by.epam.training.tolstikova.task3.command.CommandType;
import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.service.Service;
import by.epam.training.tolstikova.task3.validator.FileValidator;
import by.epam.training.tolstikova.task3.validator.ValidationResult;
import org.apache.log4j.Logger;

import java.util.Set;

public class TariffController {
    private static final Logger LOGGER = Logger.getLogger(TariffController.class);
    private FileValidator fileValidator;
    private CommandProvider CommandProvider;
    private Service tariffService;

    public TariffController(FileValidator fileValidator, ParserCommandProvider parserCommandProvider, Service tariffService) {
        this.fileValidator = fileValidator;
        this.CommandProvider = parserCommandProvider;
        this.tariffService = tariffService;
    }

    public void controlTariffsLifeCycle(String pathToXml, CommandType commandType) {
        ValidationResult validationResult = fileValidator.validateFile(pathToXml);
        if (validationResult.isValid()) {
            Command command = CommandProvider.getCommand(commandType);
            try {
                Set<Tariff> tariffs = command.execute(pathToXml);
                tariffs.forEach(this::accept);
            } catch (CommandException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    private void accept(Tariff tariff) {
        tariffService.save(tariff);
    }
}
