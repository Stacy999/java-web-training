package by.epam.training.tolstikova.task3.service;

import java.util.Set;

public interface Service<T> {
    void save(T item);

    Set<T> getAll();
}
