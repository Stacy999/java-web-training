package by.epam.training.tolstikova.task3.controller;

import by.epam.training.tolstikova.task3.command.Command;
import by.epam.training.tolstikova.task3.command.CommandType;

import java.util.HashMap;
import java.util.Map;

public class ParserCommandProvider implements CommandProvider {

    private Map<CommandType, Command> commands;

    public ParserCommandProvider() {
        commands = new HashMap<>();
    }

    public ParserCommandProvider(Map<CommandType, Command> commands) {
        this.commands = commands;
    }

    @Override
    public Command getCommand(CommandType commandType) {
        return commands.get(commandType);
    }

    @Override
    public void addCommand(Command command, CommandType commandType) {
        commands.put(commandType, command);
    }
}
