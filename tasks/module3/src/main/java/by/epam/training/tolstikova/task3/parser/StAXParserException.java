package by.epam.training.tolstikova.task3.parser;

public class StAXParserException extends ParserException {
    public StAXParserException() {
    }

    public StAXParserException(String message) {
        super(message);
    }

    public StAXParserException(Throwable cause) {
        super(cause);
    }
}
