package by.epam.training.tolstikova.task3.parser;

import java.util.Set;

public interface SimpleParser<T> {
    Set<T> parse(String pathToXml) throws ParserException;
}
