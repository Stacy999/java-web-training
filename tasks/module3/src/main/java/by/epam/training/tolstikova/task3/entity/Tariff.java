package by.epam.training.tolstikova.task3.entity;

import java.util.Objects;

public class Tariff {
    private String name;
    private String operatorName;
    private double insideTheNetworkCallsPrice;
    private double toOtherNetworksCallsPrice;
    private double landlinePhoneCallsPrice;
    private double smsPrise;
    private Tariffing tariffing;
    private boolean favouriteNumber;
    private double connectionFee;
    private long id;

    public Tariff() {
    }

    public Tariff(String name, String operatorName, double insideTheNetworkCallsPrice,
                  double toOtherNetworksCallsPrice, double landlinePhoneCallsPrice, double smsPrise,
                  Tariffing tariffing, boolean favouriteNumber, double connectionFee) {
        this.name = name;
        this.operatorName = operatorName;
        this.insideTheNetworkCallsPrice = insideTheNetworkCallsPrice;
        this.toOtherNetworksCallsPrice = toOtherNetworksCallsPrice;
        this.landlinePhoneCallsPrice = landlinePhoneCallsPrice;
        this.smsPrise = smsPrise;
        this.favouriteNumber = favouriteNumber;
        this.connectionFee = connectionFee;
        this.tariffing = tariffing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public double getInsideTheNetworkCallsPrice() {
        return insideTheNetworkCallsPrice;
    }

    public void setInsideTheNetworkCallsPrice(double insideTheNetworkCallsPrice) {
        this.insideTheNetworkCallsPrice = insideTheNetworkCallsPrice;
    }

    public double getToOtherNetworksCallsPrice() {
        return toOtherNetworksCallsPrice;
    }

    public void setToOtherNetworksCallsPrice(double toOtherNetworksCallsPrice) {
        this.toOtherNetworksCallsPrice = toOtherNetworksCallsPrice;
    }

    public double getLandlinePhoneCallsPrice() {
        return landlinePhoneCallsPrice;
    }

    public void setLandlinePhoneCallsPrice(double landlinePhoneCallsPrice) {
        this.landlinePhoneCallsPrice = landlinePhoneCallsPrice;
    }

    public double getSmsPrise() {
        return smsPrise;
    }

    public void setSmsPrise(double smsPrise) {
        this.smsPrise = smsPrise;
    }

    public boolean isFavouriteNumber() {
        return favouriteNumber;
    }

    public void setFavouriteNumber(boolean favouriteNumber) {
        this.favouriteNumber = favouriteNumber;
    }

    public double getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(double connectionFee) {
        this.connectionFee = connectionFee;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Tariffing getTariffing() {
        return tariffing;
    }

    public void setTariffing(Tariffing tariffing) {
        this.tariffing = tariffing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tariff)) return false;
        Tariff tariff = (Tariff) o;
        return Double.compare(tariff.insideTheNetworkCallsPrice, insideTheNetworkCallsPrice) == 0 &&
                Double.compare(tariff.toOtherNetworksCallsPrice, toOtherNetworksCallsPrice) == 0 &&
                Double.compare(tariff.landlinePhoneCallsPrice, landlinePhoneCallsPrice) == 0 &&
                Double.compare(tariff.smsPrise, smsPrise) == 0 &&
                favouriteNumber == tariff.favouriteNumber &&
                Double.compare(tariff.connectionFee, connectionFee) == 0 &&
                id == tariff.id &&
                Objects.equals(name, tariff.name) &&
                Objects.equals(operatorName, tariff.operatorName) &&
                tariffing == tariff.tariffing;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, operatorName, insideTheNetworkCallsPrice, toOtherNetworksCallsPrice, landlinePhoneCallsPrice, smsPrise, tariffing, favouriteNumber, connectionFee, id);
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "name='" + name + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", insideTheNetworkCallsPrice=" + insideTheNetworkCallsPrice +
                ", toOtherNetworksCallsPrice=" + toOtherNetworksCallsPrice +
                ", landlinePhoneCallsPrice=" + landlinePhoneCallsPrice +
                ", smsPrise=" + smsPrise +
                ", tariffing=" + tariffing +
                ", favouriteNumber=" + favouriteNumber +
                ", connectionFee=" + connectionFee +
                ", id=" + id +
                '}';
    }
}
