package by.epam.training.tolstikova.task3.command;

import by.epam.training.tolstikova.task3.entity.Tariff;

import java.util.Set;

public class StAXParserCommand implements Command {
    ParseReceiver parseReceiver;

    public StAXParserCommand(ParseReceiver parseReceiver) {
        this.parseReceiver = parseReceiver;
    }

    @Override
    public Set<Tariff> execute(String path) throws CommandException {
        return parseReceiver.parseStAX(path);
    }
}
