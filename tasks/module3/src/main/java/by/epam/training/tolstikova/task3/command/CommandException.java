package by.epam.training.tolstikova.task3.command;

public class CommandException extends Exception {
    public CommandException() {
    }

    CommandException(String message) {
        super(message);
    }

    CommandException(Throwable cause) {
        super(cause);
    }
}
