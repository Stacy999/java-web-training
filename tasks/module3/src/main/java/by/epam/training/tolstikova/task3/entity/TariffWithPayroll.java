package by.epam.training.tolstikova.task3.entity;

import java.util.Objects;

public class TariffWithPayroll extends Tariff {
    private double payroll;

    public TariffWithPayroll() {
    }

    public TariffWithPayroll(String name, String operatorName, double insideTheNetworkCallsPrice,
                             double toOtherNetworksCallsPrice, double landlinePhoneCallsPrice, double smsPrise,
                             Tariffing tariffing, boolean favouriteNumber, double connectionFee, double payroll) {

        super(name, operatorName, insideTheNetworkCallsPrice, toOtherNetworksCallsPrice,
                landlinePhoneCallsPrice, smsPrise, tariffing, favouriteNumber, connectionFee);
        this.payroll = payroll;
    }

    public double getPayroll() {
        return payroll;
    }

    public void setPayroll(double payroll) {
        this.payroll = payroll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TariffWithPayroll)) return false;
        if (!super.equals(o)) return false;
        TariffWithPayroll that = (TariffWithPayroll) o;
        return Double.compare(that.payroll, payroll) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), payroll);
    }

    @Override
    public String toString() {
        return super.toString() + "\nTariffWithPayroll{" +
                "payroll=" + payroll +
                '}';
    }
}
