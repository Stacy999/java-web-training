package by.epam.training.tolstikova.task3.command;

public enum CommandType {
    DOM, SAX, StAX
}
