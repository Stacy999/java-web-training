package by.epam.training.tolstikova.task3.parser;

import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.entity.TariffWithPayroll;
import by.epam.training.tolstikova.task3.entity.TariffWithoutPayroll;
import by.epam.training.tolstikova.task3.entity.Tariffing;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class DOMParser implements SimpleParser<Tariff> {
    private static final Logger LOGGER = Logger.getLogger(DOMParser.class);
    private Set<Tariff> tariffs;
    private DocumentBuilder documentBuilder;

    public DOMParser() throws DOMParserException {
        this.tariffs = new HashSet<Tariff>();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new DOMParserException(e);
        }
    }


    @Override
    public Set<Tariff> parse(String pathToXml) throws DOMParserException {
        Document document = null;
        try {
            document = documentBuilder.parse(pathToXml);
            Element root = document.getDocumentElement();

            NodeList tariffsWithPayrollList = root.getElementsByTagName("tariffWithPayroll");
            for (int i = 0; i < tariffsWithPayrollList.getLength(); i++) {
                Element tariffElement = (Element) tariffsWithPayrollList.item(i);
                Tariff tariff = buildTariff(tariffElement);
                tariffs.add(tariff);
            }
            NodeList tariffsWithoutPayroll = root.getElementsByTagName("tariffWithoutPayroll");
            for (int i = 0; i < tariffsWithoutPayroll.getLength(); i++) {
                Element tariffElement = (Element) tariffsWithoutPayroll.item(i);
                Tariff tariff = buildTariff(tariffElement);
                tariffs.add(tariff);
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (SAXException e) {
            throw new DOMParserException(e);
        }
        return tariffs;
    }

    private Tariff buildTariff(Element tariffElement) {
        Tariff tariff;
        if (tariffElement.getTagName().equals("tariffWithPayroll")) {
            tariff = new TariffWithPayroll();
            ((TariffWithPayroll) tariff).setPayroll(
                    Double.parseDouble(getElementTextContent(tariffElement, "payroll")));
        } else {
            tariff = new TariffWithoutPayroll();
            ((TariffWithoutPayroll) tariff).setSubscriptionCost(
                    Double.parseDouble(getElementTextContent(tariffElement, "subscriptionCost"))
            );
            ((TariffWithoutPayroll) tariff).setSubscriptionDays(
                    Integer.parseInt(getElementTextContent(tariffElement, "subscriptionDays"))
            );
        }
        tariff.setId(
                Long.parseLong(tariffElement.getAttribute("id")));
        tariff.setOperatorName(tariffElement.getAttribute("operatorName"));
        tariff.setName(getElementTextContent(tariffElement, "name"));
        tariff.setInsideTheNetworkCallsPrice(
                Double.parseDouble(getElementTextContent(tariffElement, "insideTheNetworkCallsPrice")));
        tariff.setToOtherNetworksCallsPrice(
                Double.parseDouble(getElementTextContent(tariffElement, "toOtherNetworksCallsPrice")));
        tariff.setLandlinePhoneCallsPrice(
                Double.parseDouble(getElementTextContent(tariffElement, "landlinePhoneCallsPrice")));
        tariff.setSmsPrise(
                Double.parseDouble(getElementTextContent(tariffElement, "smsPrice")));
        tariff.setTariffing(Tariffing.valueOf(getElementTextContent(tariffElement, "tariffing")));
        tariff.setFavouriteNumber(
                Boolean.parseBoolean(getElementTextContent(tariffElement, "favouriteNumber")));
        tariff.setConnectionFee(
                Double.parseDouble(getElementTextContent(tariffElement, "connectionFee")));
        return tariff;
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }
}
