package by.epam.training.tolstikova.task3.entity;

import java.util.Objects;

public class TariffWithoutPayroll extends Tariff {
    private double subscriptionCost;
    private int subscriptionDays;

    public TariffWithoutPayroll() {
    }

    public TariffWithoutPayroll(String name, String operatorName, double insideTheNetworkCallsPrice,
                                double toOtherNetworksCallsPrice, double landlinePhoneCallsPrice, double smsPrise,
                                Tariffing tariffing, boolean favouriteNumber, double connectionFee,
                                double subscriptionCost, int subscriptionDays) {

        super(name, operatorName, insideTheNetworkCallsPrice, toOtherNetworksCallsPrice,
                landlinePhoneCallsPrice, smsPrise, tariffing, favouriteNumber, connectionFee);
        this.subscriptionCost = subscriptionCost;
        this.subscriptionDays = subscriptionDays;
    }

    public double getSubscriptionCost() {
        return subscriptionCost;
    }

    public void setSubscriptionCost(double subscriptionCost) {
        this.subscriptionCost = subscriptionCost;
    }

    public int getSubscriptionDays() {
        return subscriptionDays;
    }

    public void setSubscriptionDays(int subscriptionDays) {
        this.subscriptionDays = subscriptionDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TariffWithoutPayroll)) return false;
        if (!super.equals(o)) return false;
        TariffWithoutPayroll that = (TariffWithoutPayroll) o;
        return Double.compare(that.subscriptionCost, subscriptionCost) == 0 &&
                subscriptionDays == that.subscriptionDays;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), subscriptionCost, subscriptionDays);
    }

    @Override
    public String toString() {
        return super.toString() + "\nTariffWithoutPayroll{" +
                "subscriptionCost=" + subscriptionCost +
                ", subscriptionDays=" + subscriptionDays +
                '}';
    }
}
