package by.epam.training.tolstikova.task3.repository;

import java.util.Set;

public interface Repository<T> {
    void add(T item);

    Set<T> getAll();
}
