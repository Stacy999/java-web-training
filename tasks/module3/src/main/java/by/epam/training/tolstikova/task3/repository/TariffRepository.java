package by.epam.training.tolstikova.task3.repository;

import by.epam.training.tolstikova.task3.entity.Tariff;

import java.util.LinkedHashSet;
import java.util.Set;

public class TariffRepository implements Repository<Tariff> {
    private Set<Tariff> tariffs;

    public TariffRepository() {
        this.tariffs = new LinkedHashSet<>();
    }

    @Override
    public void add(Tariff tariff) {
        tariffs.add(tariff);
    }

    @Override
    public Set<Tariff> getAll() {
        return tariffs;
    }
}
