package by.epam.training.tolstikova.task3.controller;

import by.epam.training.tolstikova.task3.command.Command;
import by.epam.training.tolstikova.task3.command.CommandType;

public interface CommandProvider {
    Command getCommand(CommandType commandType);

    void addCommand(Command command, CommandType commandType);
}
