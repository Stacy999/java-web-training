package by.epam.training.tolstikova.task3.validator;

import org.apache.log4j.Logger;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;

public class XmlByXsdValidator {
    private static final Logger LOGGER = Logger.getLogger(XmlByXsdValidator.class);
    private String pathToXsd;
    private String pathToXml;

    public XmlByXsdValidator(String pathToXsd, String pathToXml) {
        this.pathToXsd = pathToXsd;
        this.pathToXml = pathToXml;
    }

    public boolean validate() {
        Source xmlFile = new StreamSource(new File(this.pathToXml));
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(new File(this.pathToXsd));
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            return true;
        } catch (Exception exc) {
            LOGGER.error(exc.getMessage());
            return false;
        }
    }
}
