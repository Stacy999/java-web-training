package by.epam.training.tolstikova.task3.parser;

public class SAXParserException extends ParserException {
    public SAXParserException() {
    }

    public SAXParserException(String message) {
        super(message);
    }

    public SAXParserException(Throwable cause) {
        super(cause);
    }
}
