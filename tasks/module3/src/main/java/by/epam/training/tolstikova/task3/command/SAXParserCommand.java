package by.epam.training.tolstikova.task3.command;

import by.epam.training.tolstikova.task3.entity.Tariff;

import java.util.Set;

public class SAXParserCommand implements Command {
    private ParseReceiver parseReceiver;

    public SAXParserCommand(ParseReceiver parseReceiver) {
        this.parseReceiver = parseReceiver;
    }

    @Override
    public Set<Tariff> execute(String path) throws CommandException {
        return parseReceiver.parseSAX(path);
    }
}
