package by.epam.training.tolstikova.task3.command;

import by.epam.training.tolstikova.task3.entity.Tariff;

import java.util.Set;

public interface Command {
    Set<Tariff> execute(String path) throws CommandException;
}
