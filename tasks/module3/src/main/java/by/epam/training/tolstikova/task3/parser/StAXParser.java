package by.epam.training.tolstikova.task3.parser;

import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.entity.TariffWithPayroll;
import by.epam.training.tolstikova.task3.entity.TariffWithoutPayroll;
import by.epam.training.tolstikova.task3.entity.Tariffing;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

public class StAXParser implements SimpleParser<Tariff> {
    private HashSet<Tariff> tariffs = new HashSet<>();
    private XMLInputFactory xmlInputFactory;

    public StAXParser() {
        xmlInputFactory = XMLInputFactory.newInstance();
    }

    public HashSet<Tariff> getTariffs() {
        return tariffs;
    }

    @Override
    public Set<Tariff> parse(String pathToXml) throws ParserException {
        FileInputStream inputStream;
        XMLStreamReader reader;
        try {
            inputStream = new FileInputStream(new File(pathToXml));
            reader = xmlInputFactory.createXMLStreamReader(inputStream);

            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    Tariff tariff = buildTariff(reader);
                    tariffs.add(tariff);
                }
            }
        } catch (XMLStreamException | FileNotFoundException ex) {
            throw new StAXParserException(ex);
        }
        return tariffs;
    }

    private Tariff buildTariff(XMLStreamReader reader) throws XMLStreamException {
        String name;
        name = reader.getLocalName();
        Tariff tariff = new Tariff();
        if (name.equals("tariffWithoutPayroll")) {
            tariff = new TariffWithoutPayroll();
            tariff.setId(Long.parseLong(reader.getAttributeValue(null, "id")));
            tariff.setOperatorName(reader.getAttributeValue(null, "operatorName"));

        } else if (name.equals("tariffWithPayroll")) {
            tariff = new TariffWithPayroll();
            tariff.setId(Long.parseLong(reader.getAttributeValue(null, "id")));
            tariff.setOperatorName(reader.getAttributeValue(null, "operatorName"));
        }

        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("name")) {
                        tariff.setName(getXMLText(reader));
                    }
                    if (name.equals("insideTheNetworkCallsPrice")) {
                        tariff.setInsideTheNetworkCallsPrice(
                                Double.parseDouble(getXMLText(reader))
                        );
                    }
                    if (name.equals("toOtherNetworksCallsPrice")) {
                        tariff.setToOtherNetworksCallsPrice(
                                Double.parseDouble(getXMLText(reader))
                        );
                    }
                    if (name.equals("landlinePhoneCallsPrice")) {
                        tariff.setLandlinePhoneCallsPrice(
                                Double.parseDouble(getXMLText(reader))
                        );
                    }
                    if (name.equals("smsPrice")) {
                        tariff.setSmsPrise(
                                Double.parseDouble(getXMLText(reader))
                        );
                    }
                    if (name.equals("tariffing")) {
                        tariff.setTariffing(Tariffing.valueOf(getXMLText(reader)));
                    }
                    if (name.equals("favouriteNumber")) {
                        tariff.setFavouriteNumber(
                                Boolean.parseBoolean(getXMLText(reader))
                        );
                    }
                    if (name.equals("connectionFee")) {
                        tariff.setConnectionFee(
                                Double.parseDouble(getXMLText(reader)));
                    }

                    if (tariff.getClass().isInstance(TariffWithoutPayroll.class)) {
                        if (name.equals("subscriptionCost")) {
                            ((TariffWithoutPayroll) tariff).setSubscriptionCost(
                                    Double.parseDouble(getXMLText(reader))
                            );
                        }
                        if (name.equals("subscriptionDays")) {
                            ((TariffWithoutPayroll) tariff).setSubscriptionDays(
                                    Integer.parseInt(getXMLText(reader))
                            );
                        }
                    }

                    if (tariff.getClass().isInstance(TariffWithPayroll.class)) {
                        if (name.equals("payroll")) {
                            ((TariffWithPayroll) tariff).setPayroll(
                                    Double.parseDouble(getXMLText(reader))
                            );
                        }
                    }
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("tariffWithoutPayroll") || name.equals("tariffWithPayroll")) {
                        return tariff;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
