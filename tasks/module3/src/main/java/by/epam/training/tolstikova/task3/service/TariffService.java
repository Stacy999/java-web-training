package by.epam.training.tolstikova.task3.service;

import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.repository.Repository;

import java.util.Set;

public class TariffService implements Service<Tariff> {
    Repository<Tariff> repository;

    public TariffService(Repository<Tariff> repository) {
        this.repository = repository;
    }

    @Override
    public void save(Tariff tariff) {
        repository.add(tariff);
    }

    @Override
    public Set<Tariff> getAll() {
        return repository.getAll();
    }
}
