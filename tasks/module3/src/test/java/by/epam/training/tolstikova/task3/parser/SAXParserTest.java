package by.epam.training.tolstikova.task3.parser;

import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.entity.TariffWithPayroll;
import by.epam.training.tolstikova.task3.entity.TariffWithoutPayroll;
import by.epam.training.tolstikova.task3.entity.Tariffing;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SAXParserTest {

    private SAXParser saxParser;

    @Before
    public void init(){
        saxParser = new SAXParser();
    }

    @Test
    public void parse() throws SAXParserException {
        Set<Tariff> expected = new LinkedHashSet<>();
        Tariff tariff1 = new TariffWithoutPayroll("Hello-calls", "A1",0.095,
                0.095,0.095,0.095, Tariffing.MINUTE,
                false,5.00,
                1.99,7);
        Tariff tariff2 = new TariffWithoutPayroll("Hello-tourist","A1",0.075,
                0.075,0.075,0.075,Tariffing.MINUTE,
                true,10.00,9.90,7);
        Tariff tariff3 = new TariffWithPayroll("Comfort-S","A1",0.0,
                0.075,0.075,0.075,Tariffing.MINUTE,
                false,5.00,12.40);
        tariff1.setId(1);
        tariff2.setId(2);
        tariff3.setId(3);
        expected.add(tariff1);
        expected.add(tariff2);
        expected.add(tariff3);

        ClassLoader classLoader = getClass().getClassLoader();
        Set<Tariff> actual = saxParser.parse(classLoader.getResource("testXML.xml").getFile());

        assertEquals(expected,actual);
    }
}