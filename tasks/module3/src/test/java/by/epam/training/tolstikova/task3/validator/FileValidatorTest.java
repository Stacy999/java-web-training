package by.epam.training.tolstikova.task3.validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class FileValidatorTest {

    private FileValidator fileValidator;
    private ValidationResult validationResult;

    @Before
    public void init() {
        fileValidator = new FileValidator();
        validationResult = new ValidationResult();
    }

    @Test
    public void shouldBeValidFile() {
        //test
        ClassLoader classLoader = getClass().getClassLoader();
        validationResult = fileValidator.validateFile(
                new File(classLoader.getResource("testXML.xml").getFile()).toString());

        //assert
        assertTrue(validationResult.isValid());
    }

    @Test(expected = NullPointerException.class)
    public void shouldBeNull() {
        //test
        ClassLoader classLoader = getClass().getClassLoader();
        validationResult = fileValidator.validateFile(
                new File(classLoader.getResource("isNotFile.txt").getFile()).toString());
    }
}