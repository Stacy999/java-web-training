package by.epam.training.tolstikova.task3.controller;

import by.epam.training.tolstikova.task3.command.DOMParserCommand;
import by.epam.training.tolstikova.task3.command.ParseReceiver;
import by.epam.training.tolstikova.task3.command.SAXParserCommand;
import by.epam.training.tolstikova.task3.command.StAXParserCommand;
import by.epam.training.tolstikova.task3.entity.Tariff;
import by.epam.training.tolstikova.task3.entity.TariffWithPayroll;
import by.epam.training.tolstikova.task3.entity.TariffWithoutPayroll;
import by.epam.training.tolstikova.task3.entity.Tariffing;
import by.epam.training.tolstikova.task3.repository.Repository;
import by.epam.training.tolstikova.task3.repository.TariffRepository;
import by.epam.training.tolstikova.task3.service.Service;
import by.epam.training.tolstikova.task3.service.TariffService;
import by.epam.training.tolstikova.task3.validator.FileValidator;
import by.epam.training.tolstikova.task3.validator.XmlByXsdValidator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedHashSet;
import java.util.Set;

import static by.epam.training.tolstikova.task3.command.CommandType.DOM;
import static by.epam.training.tolstikova.task3.command.CommandType.SAX;
import static by.epam.training.tolstikova.task3.command.CommandType.StAX;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TariffControllerTest {

    private TariffController tariffController;
    private Service service;
    private ClassLoader classLoader;
    private Set<Tariff> expected;

    @Before
    public void init() {
        classLoader = getClass().getClassLoader();
        XmlByXsdValidator xmlByXsdValidator = new XmlByXsdValidator(
                classLoader.getResource("tariffs.xsd").getFile(),
                classLoader.getResource("testXML.xml").getFile());

        FileValidator fileValidator = new FileValidator();
        ParseReceiver parseReceiver = new ParseReceiver(xmlByXsdValidator);
        ParserCommandProvider parserCommandProvider = new ParserCommandProvider();
        parserCommandProvider.addCommand(new DOMParserCommand(parseReceiver), DOM);
        parserCommandProvider.addCommand(new SAXParserCommand(parseReceiver), SAX);
        parserCommandProvider.addCommand(new StAXParserCommand(parseReceiver), StAX);
        Repository<Tariff> repository = new TariffRepository();
        service = new TariffService(repository);
        tariffController = new TariffController(fileValidator, parserCommandProvider, service);

        expected = new LinkedHashSet<>();
        Tariff tariff1 = new TariffWithoutPayroll("Hello-calls", "A1", 0.095,
                0.095, 0.095, 0.095, Tariffing.MINUTE,
                false, 5.00,
                1.99, 7);
        Tariff tariff2 = new TariffWithoutPayroll("Hello-tourist", "A1", 0.075,
                0.075, 0.075, 0.075, Tariffing.MINUTE,
                true, 10.00, 9.90, 7);
        Tariff tariff3 = new TariffWithPayroll("Comfort-S", "A1", 0.0,
                0.075, 0.075, 0.075, Tariffing.MINUTE,
                false, 5.00, 12.40);
        tariff1.setId(1);
        tariff2.setId(2);
        tariff3.setId(3);
        expected.add(tariff1);
        expected.add(tariff2);
        expected.add(tariff3);
    }

    @Test
    public void controlTariffsLifeCycle_DOMParse() {
        tariffController.controlTariffsLifeCycle(classLoader.getResource("testXML.xml").getFile(), DOM);

        assertEquals(expected, service.getAll());
    }

    @Test
    public void controlTariffsLifeCycle_SAXParse() {
        tariffController.controlTariffsLifeCycle(classLoader.getResource("testXML.xml").getFile(), SAX);

        assertEquals(expected, service.getAll());
    }

//    @Test
//    public void controlTariffsLifeCycle_StAXParse(){
//        tariffController.controlTariffsLifeCycle(classLoader.getResource("testXML.xml").getFile(),StAX);
//
//        assertEquals(expected,service.getAll());
//    }
}