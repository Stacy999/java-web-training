package by.epam.training.tolstikova.task3.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class XmlByXsdValidatorTest {

    @Test
    public void validateCorrectXMLReturnsTrue() {
        ClassLoader classLoader = getClass().getClassLoader();

        XmlByXsdValidator xmlByXsdValidator = new XmlByXsdValidator(
                classLoader.getResource("tariffs.xsd").getFile(),
                classLoader.getResource("tariffs.xml").getFile());
        boolean actual = xmlByXsdValidator.validate();

        assertTrue(actual);
    }

    @Test
    public void validateIncorrectXMLReturnsFalse() {
        ClassLoader classLoader = getClass().getClassLoader();

        XmlByXsdValidator xmlByXsdValidator = new XmlByXsdValidator(
                classLoader.getResource("tariffs.xsd").getFile(),
                classLoader.getResource("invalidXML.xml").getFile());
        boolean actual = xmlByXsdValidator.validate();

        assertFalse(actual);
    }
}