package by.training.tolstikova.airline.service;

import by.training.tolstikova.airline.entity.CargoPlane;
import by.training.tolstikova.airline.entity.PassengerPlane;
import by.training.tolstikova.airline.entity.Plane;
import by.training.tolstikova.airline.entity.WarPlane;
import by.training.tolstikova.airline.repository.Airline;
import by.training.tolstikova.airline.repository.specification.FindPlanesByFuelConsumptionSpecification;
import by.training.tolstikova.airline.repository.specification.IllegalSpecificationException;
import by.training.tolstikova.airline.repository.specification.SortByCapacityThenByCarryingCapacitySpecification;
import by.training.tolstikova.airline.repository.specification.SortPlanesByCapacitySpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class AirlineTest {

    private Airline airline;

    @Before
    public void init(){
        //prepare
        airline = new Airline();
        airline.add(new WarPlane(2,7000,12000,"missiles"));
        airline.add(new PassengerPlane(150,5000,10000,
                "large haul"));
        airline.add(new CargoPlane(3,20000,
                10000,"heavy metal"));
    }

    @Test
    public void query() throws IllegalSpecificationException {
        //test
        List<Plane> actual1 = new ArrayList<>(airline.query(new FindPlanesByFuelConsumptionSpecification(8000,10000)));
        List<Plane> expected1 = new ArrayList<>();
        expected1.add(airline.getAll().get(1));
        expected1.add(airline.getAll().get(2));

        //assert
        assertEquals(expected1,actual1);

    }

    @Test
    public void query2() throws IllegalSpecificationException {
        List<Plane> actual2 = new ArrayList<>(airline.query(new SortPlanesByCapacitySpecification()));
        List<Plane> expected2 = new ArrayList<>();
        expected2.add(airline.getAll().get(0));
        expected2.add(airline.getAll().get(2));
        expected2.add(airline.getAll().get(1));

        assertEquals(expected2,actual2);
    }

    @Test
    public void query3() throws IllegalSpecificationException {
        List<Plane> actual3 = new ArrayList<>(airline.query(new SortByCapacityThenByCarryingCapacitySpecification()));
        List<Plane> expected3 = new ArrayList<>();
        expected3.add(airline.getAll().get(0));
        expected3.add(airline.getAll().get(2));
        expected3.add(airline.getAll().get(1));
        assertEquals(expected3,actual3);
    }

    @Test
    public void size() {
        assertEquals(3,airline.size());
    }

}