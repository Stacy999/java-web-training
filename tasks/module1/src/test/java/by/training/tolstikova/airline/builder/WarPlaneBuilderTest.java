package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.WarPlaneBuilder;
import by.training.tolstikova.airline.entity.WarPlane;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class WarPlaneBuilderTest {

    private Map<String, String> possiblePlane;
    private WarPlaneBuilder warPlaneBuilder;

    @Before
    public void init() {
        warPlaneBuilder = new WarPlaneBuilder();
        possiblePlane = new HashMap<>();
        possiblePlane.put("Type", "War plane");
        possiblePlane.put("Capacity", "3");
        possiblePlane.put("Carrying capacity", "8500");
        possiblePlane.put("Fuel consumption", "11500");
        possiblePlane.put("Weapon", "missiles");
    }

    @Test
    public void createObject() {
        //test
        WarPlane actual = warPlaneBuilder.createObject(possiblePlane);
        WarPlane expected = new WarPlane(3, 8500, 11500, "missiles");

        //assert
        assertEquals(expected, actual);
    }
}