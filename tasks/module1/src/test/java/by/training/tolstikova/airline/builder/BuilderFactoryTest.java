package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class BuilderFactoryTest {

    private static final String CARGO_PLANE = "Cargo plane";
    private static final String WAR_PLANE = "War plane";
    private static final String NEW_PLANE = "New plane";

    private BuilderFactory builderFactory;

    @Before
    public void init(){
        builderFactory = new BuilderFactory();
    }

    @Test
    public void getCargoPlaneBuilder() throws NoSuchPlaneTypeException {
        //test
        Builder builder = builderFactory.getBuilder(CARGO_PLANE);

        //assert
        assertTrue(builder instanceof CargoPlaneBuilder);
    }

    @Test
    public void getWarPlaneBuilder() throws NoSuchPlaneTypeException {
        //test
        Builder builder = builderFactory.getBuilder(WAR_PLANE);

        //assert
        assertTrue(builder instanceof WarPlaneBuilder);
    }

    @Test(expected = NoSuchPlaneTypeException.class)
    public void getBuilder() throws NoSuchPlaneTypeException {
        //test
        Builder builder = builderFactory.getBuilder(NEW_PLANE);

        //assert
        assertTrue(builder instanceof WarPlaneBuilder);
    }
}