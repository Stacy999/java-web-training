package by.training.tolstikova.airline.parser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class LineParserTest {

    private LineParser lineParser;
    private String string;
    private Map<String,String> expected;

    @Before
    public void init(){
        lineParser = new LineParser();
        expected = new HashMap<>();
        string = "I:love,my:family";
        expected.put("I","love");
        expected.put("my","family");
    }

    @Test
    public void getData() {
        //test
        Map<String, String> result = lineParser.getData(string);

        //assert
        assertEquals(expected, result);
    }
}