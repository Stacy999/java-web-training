package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.CargoPlaneBuilder;
import by.training.tolstikova.airline.entity.CargoPlane;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class CargoPlaneBuilderTest {

    private Map<String, String> possiblePlane;
    private CargoPlaneBuilder cargoPlaneBuilder;

    @Before
    public void init() {
        cargoPlaneBuilder = new CargoPlaneBuilder();
        possiblePlane = new HashMap<>();
        possiblePlane.put("Type", "Cargo plane");
        possiblePlane.put("Capacity", "3");
        possiblePlane.put("Carrying capacity", "8500");
        possiblePlane.put("Fuel consumption", "11500");
        possiblePlane.put("Type of cargo", "Cars");
    }

    @Test
    public void createObject() {
        //test
        CargoPlane actual = cargoPlaneBuilder.createObject(possiblePlane);
        CargoPlane expected = new CargoPlane(3, 8500, 11500, "Cars");

        //assert
        assertEquals(expected, actual);
    }
}