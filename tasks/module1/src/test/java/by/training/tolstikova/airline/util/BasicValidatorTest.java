package by.training.tolstikova.airline.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class BasicValidatorTest {

    @Test
    public void isInteger() {
        assertTrue(BasicValidator.isInteger("25"));
    }

    @Test
    public void isNotInteger(){
        assertFalse(BasicValidator.isInteger("pancake"));
    }

    @Test
    public void isDouble() {
        assertTrue(BasicValidator.isDouble("3.14"));
    }

    @Test
    public void isNotDouble(){
        assertFalse(BasicValidator.isDouble("pancake"));
    }
}