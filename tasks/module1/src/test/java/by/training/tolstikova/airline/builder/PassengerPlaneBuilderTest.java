package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.PassengerPlaneBuilder;
import by.training.tolstikova.airline.entity.PassengerPlane;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PassengerPlaneBuilderTest {

    private Map<String, String> possiblePlane;
    private PassengerPlaneBuilder passengerPlaneBuilder;

    @Before
    public void init() {
        passengerPlaneBuilder = new PassengerPlaneBuilder();
        possiblePlane = new HashMap<>();
        possiblePlane.put("Type", "Cargo plane");
        possiblePlane.put("Capacity", "300");
        possiblePlane.put("Carrying capacity", "8500");
        possiblePlane.put("Fuel consumption", "8000");
        possiblePlane.put("Flight range", "Long haul");
    }

    @Test
    public void createObject() {
        //test
        PassengerPlane actual = passengerPlaneBuilder.createObject(possiblePlane);
        PassengerPlane expected = new PassengerPlane
                (300, 8500, 8000, "Long haul");

        //assert
        assertEquals(expected, actual);
    }
}