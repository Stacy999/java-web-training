package by.training.tolstikova.airline.service;

import by.training.tolstikova.airline.entity.CargoPlane;
import by.training.tolstikova.airline.entity.PassengerPlane;
import by.training.tolstikova.airline.entity.WarPlane;
import by.training.tolstikova.airline.repository.Airline;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AirlineServiceTest {

    private Airline airline;
    private AirlineService airlineService;

    @Before
    public void init(){
        airline = new Airline();

        //prepare
        airline.add(new WarPlane(2,7000,12000,"missiles"));
        airline.add(new PassengerPlane(150,5000,10000,
                "large haul"));
        airline.add(new CargoPlane(3,20000,10000,"heavy metal"));
        airlineService  = new AirlineService(airline);

    }

    @Test
    public void getAllCapacity() {

        //test
        int actual = airlineService.getAllCapacity();

        //assert
        assertEquals(155,actual);
    }

    @Test
    public void getAllCarryingCapacity() {

        //test
        double actual = airlineService.getAllCarryingCapacity();

        //assert
        assertEquals(32000,actual,0);
    }
}