package by.training.tolstikova.airline.validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PlaneValidatorTest {

    private PlaneValidator planeValidator;
    private Map<String, String> possiblePlane;
    private Map<String, String> possiblePlane2;
    private Map<String, String> possiblePlane3;

    @Before
    public void init() {
        planeValidator = new PlaneValidator();
        possiblePlane = new HashMap<>();
        possiblePlane.put("Type", "Cargo plane");
        possiblePlane.put("Capacity", "3");
        possiblePlane.put("Carrying capacity", "12000");
        possiblePlane.put("Fuel consumption", "9000");
        possiblePlane.put("Type of cargo", "Cars");

        possiblePlane2 = new HashMap<>();
        possiblePlane2.put("TYPe", "Cargo plane");
        possiblePlane2.put("Capacity", "3");
        possiblePlane2.put("Carrying capacity", "12000");
        possiblePlane2.put("Fuel consumption", "9000");
        possiblePlane2.put("Type of cargo", "Cars");

        possiblePlane3 = new HashMap<>();
        possiblePlane3.put("Type", "Cargo plane");
        possiblePlane3.put("Capacity", "Java");
        possiblePlane3.put("Carrying capacity", "12000");
        possiblePlane3.put("Fuel consumption", "-10");
        possiblePlane3.put("Type of cargo", "Cars");

    }

    @Test
    public void validatePlane() {
        //test
        ValidationResult actual = planeValidator.validatePlane(possiblePlane);
        ValidationResult expected = new ValidationResult();

        //assert
        assertEquals(expected.getValidationResultMap(), actual.getValidationResultMap());
    }

    @Test
    public void shouldBeInvalidPlane() {
        //test
        ValidationResult expected = planeValidator.validatePlane(possiblePlane2);
        ValidationResult actual = new ValidationResult();
        ValidationMessage message = new ValidationMessage();
        message.addMessage("Names of fields are not correct");
        message.addMessage("Type is invalid");
        actual.getValidationResultMap().put("ERROR #101", message);

        //assert
        assertEquals(expected.getValidationResultMap(), actual.getValidationResultMap());
    }

    @Test
    public void shouldBeInvalidData() {
        //test
        ValidationResult expected = planeValidator.validatePlane(possiblePlane3);
        ValidationResult actual = new ValidationResult();
        ValidationMessage message = new ValidationMessage();
        message.addMessage("Capacity is invalid");
        message.addMessage("Fuel consumption is invalid");
        actual.getValidationResultMap().put("ERROR #101", message);

        //assert
        assertEquals(expected.getValidationResultMap(), actual.getValidationResultMap());
    }
}