package by.training.tolstikova.airline.controller;

import by.training.tolstikova.airline.controller.DataReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class DataReaderTest {

    @Test
    public void readLines() {
        List<String> expected = Arrays.asList("Type:Cargo plane,Capacity:3,Carrying capacity:8500," +
                "Fuel consumption:11500,Type of cargo:Cars",
                "Type:War plane,Capacity:2,Carrying capacity:9000,Fuel consumption:10000,Weapon:Missiles",
                "Type:Cargo plane,Capacity:2,Carrying capacity:10000,Fuel consumption:11000," +
                        "Type of cargo:Bananas",
                "Type:Passenger plane,Capacity:200,Carrying capacity:9000,Fuel consumption:9000," +
                        "Flight range:Short haul",
                "Type:Passenger plane,Capacity:250,Carrying capacity:8300,Fuel consumption:9500," +
                        "Flight range:Long haul");

        //test
        ClassLoader classLoader = getClass().getClassLoader();
        DataReader dataReader =
                new DataReader(new File(classLoader.getResource("correctFile.txt").getFile()).toString());

        //assert
        assertEquals(expected,dataReader.readLines());
    }

    @Test
    public void shouldReturnEmptyList(){
        List<String> expected = new ArrayList<>();

        //test
        ClassLoader classLoader = getClass().getClassLoader();
        DataReader dataReader =
                new DataReader(new File(classLoader.getResource("emptyFile.txt").getFile()).toString());

        //assert
        assertEquals(expected,dataReader.readLines());
    }
}