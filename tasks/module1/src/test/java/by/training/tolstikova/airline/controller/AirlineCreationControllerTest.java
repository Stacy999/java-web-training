package by.training.tolstikova.airline.controller;

import by.training.tolstikova.airline.builder.BuilderFactory;
import by.training.tolstikova.airline.parser.LineParser;
import by.training.tolstikova.airline.repository.Airline;
import by.training.tolstikova.airline.service.AirlineService;
import by.training.tolstikova.airline.validator.FileValidator;
import by.training.tolstikova.airline.validator.PlaneValidator;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class AirlineCreationControllerTest {

    private FileValidator fileValidator;
    private PlaneValidator planeValidator;
    private LineParser lineParser;
    private DataReader dataReader;
    private BuilderFactory builderFactory;
    private AirlineService airlineService;
    private AirlineCreationController airlineCreationController;

    @Before
    public void init(){
        fileValidator = new FileValidator();
        planeValidator = new PlaneValidator();
        lineParser = new LineParser();
        dataReader = new DataReader();
        builderFactory = new BuilderFactory();
        airlineService = new AirlineService(new Airline());
    }

    @Test
    public void controlAirlineCreation() {
        //test
        airlineCreationController = new AirlineCreationController(fileValidator,lineParser,dataReader,
                planeValidator,builderFactory,airlineService);
        ClassLoader classLoader = getClass().getClassLoader();
        airlineCreationController.controlAirlineCreation(
                new File(classLoader.getResource("correctFile.txt").getFile()).toString());
    }

    @Test
    public void shouldCreateOnlyCorrectObjects(){
        //test
        airlineCreationController = new AirlineCreationController(fileValidator,lineParser,dataReader,
                planeValidator,builderFactory,airlineService);
        ClassLoader classLoader = getClass().getClassLoader();
        airlineCreationController.controlAirlineCreation(
                new File(classLoader.getResource("fileWithInvalidFields.txt").getFile()).toString());
    }

    @Test
    public void shouldCreateCorrectObjects(){
        //test
        airlineCreationController = new AirlineCreationController(fileValidator,lineParser,dataReader,
                planeValidator,builderFactory,airlineService);
        ClassLoader classLoader = getClass().getClassLoader();
        airlineCreationController.controlAirlineCreation(
                new File(classLoader.getResource("fileWithMixedFields.txt").getFile()).toString());
    }

    @Test
    public void shouldBeWithErrors(){
        //test
        airlineCreationController = new AirlineCreationController(fileValidator,lineParser,dataReader,
                planeValidator,builderFactory,airlineService);
        ClassLoader classLoader = getClass().getClassLoader();
        airlineCreationController.controlAirlineCreation(
                new File(classLoader.getResource("fileWithEmptyFields.txt").getFile()).toString());
    }
}