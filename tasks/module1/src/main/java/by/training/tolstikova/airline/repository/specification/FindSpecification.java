package by.training.tolstikova.airline.repository.specification;

import by.training.tolstikova.airline.entity.Plane;

public interface FindSpecification extends Specification {
    boolean find(Plane plane);
}
