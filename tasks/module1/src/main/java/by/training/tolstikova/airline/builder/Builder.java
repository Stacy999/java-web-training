package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.entity.Plane;

import java.util.Map;

public interface Builder {
    String CAPACITY = "Capacity";
    String CARRYING_CAPACITY = "Carrying capacity";
    String FUEL_CONSUMPTION = "Fuel Consumption";

    Plane createObject(Map<String, String> data);
}
