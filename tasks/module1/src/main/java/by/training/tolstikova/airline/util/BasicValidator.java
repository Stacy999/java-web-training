package by.training.tolstikova.airline.util;

import org.apache.log4j.Logger;

public final class BasicValidator {
    private static final Logger LOGGER;

    static {
        LOGGER = Logger.getRootLogger();
    }

    private BasicValidator(){}

    public static boolean isInteger(String line) {
        try {
            Integer.parseInt(line);
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Incorrect value: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean isDouble(String line) {
        try {
            Double.parseDouble(line);
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Incorrect value: " + ex.getMessage());
            return false;
        }
        return true;
    }


}
