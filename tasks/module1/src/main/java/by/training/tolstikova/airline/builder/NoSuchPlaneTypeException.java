package by.training.tolstikova.airline.builder;

public class NoSuchPlaneTypeException extends Exception {
    public NoSuchPlaneTypeException() {
    }

    public NoSuchPlaneTypeException(String message) {
        super(message);
    }

    public NoSuchPlaneTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchPlaneTypeException(Throwable cause) {
        super(cause);
    }
}
