package by.training.tolstikova.airline.validator;

import by.training.tolstikova.airline.util.BasicValidator;

import java.util.Map;
import java.util.Set;

public class PlaneValidator {
    private static final String[] PLANE_TYPES = {"Cargo plane", "War plane", "Passenger plane"};
    private static final String[] FIELD_NAMES = {"Type","Capacity","Carrying capacity","Fuel consumption",
    "Type of cargo","Flight range","Weapon"};
    private static final int MAX_CAPACITY = 500;
    private static final int MAX_CARRYING_CAPACITY = 50_000;
    private static final int MAX_FUEL_CONSUMPTION = 20_000;
    private static final int NUMBER_OF_FIELDS = 5;

    private static boolean isType(String type) {
        if (type != null) {
            for (String planeType : PLANE_TYPES) {
                if (type.equals(planeType))
                    return true;
            }
        }
        return false;
    }

    private static boolean isCapacity(String capacity) {
        return capacity != null
                && BasicValidator.isInteger(capacity)
                && Integer.parseInt(capacity) >= 0
                && Integer.parseInt(capacity) <= MAX_CAPACITY;
    }

    private static boolean isCarryingCapacity(String carryingCapacity) {
        return carryingCapacity != null
                && BasicValidator.isDouble(carryingCapacity)
                && Double.parseDouble(carryingCapacity) >= 0
                && Double.parseDouble(carryingCapacity) <= MAX_CARRYING_CAPACITY;
    }

    private static boolean isFuelConsumption(String fuelConsumption) {
        return fuelConsumption != null
                && BasicValidator.isDouble(fuelConsumption)
                && Double.parseDouble(fuelConsumption) >= 0
                && Double.parseDouble(fuelConsumption) <= MAX_FUEL_CONSUMPTION;
    }

    private boolean areValidFieldNames(Map<String, String> possiblePlane){
        Set<String> types = possiblePlane.keySet();
        int counter = 0;
        for (String fieldName: FIELD_NAMES) {
            for (String type: types){
                if(type.equals(fieldName)) counter++;
            }
        }
        return counter == NUMBER_OF_FIELDS;
    }

    public ValidationResult validatePlane(Map<String, String> possiblePlane) {
        ValidationResult validationResult = new ValidationResult();
        ValidationMessage validationMessage = new ValidationMessage();
        if (possiblePlane != null) {
            if (possiblePlane.size() != NUMBER_OF_FIELDS) {
                validationMessage.addMessage("Number of fields is not correct");
            }
            if(!areValidFieldNames(possiblePlane)){
                validationMessage.addMessage("Names of fields are not correct");
            }
            if (!isType(possiblePlane.get("Type"))) {
                validationMessage.addMessage("Type is invalid");
            }
            if (!isCapacity(possiblePlane.get("Capacity"))) {
                validationMessage.addMessage("Capacity is invalid");
            }
            if (!isCarryingCapacity(possiblePlane.get("Carrying capacity"))) {
                validationMessage.addMessage("Carrying capacity is invalid");
            }
            if (!isFuelConsumption(possiblePlane.get("Fuel consumption"))) {
                validationMessage.addMessage("Fuel consumption is invalid");
            }
            if (validationMessage.getMessageList().size() != 0) {
                validationResult.getValidationResultMap().put("ERROR #101", validationMessage);
            }else {
                validationResult.setValid(true);
            }
        }
        return validationResult;
    }

}
