package by.training.tolstikova.airline.builder;


public class BuilderFactory {
    private static final String CARGO_PLANE = "Cargo plane";
    private static final String PASSENGER_PLANE = "Passenger plane";
    private static final String WAR_PLANE = "War plane";

     public Builder getBuilder(String type) throws NoSuchPlaneTypeException {
        Builder builder;
        switch (type) {
            case CARGO_PLANE:
                builder = new CargoPlaneBuilder();
                break;
            case PASSENGER_PLANE:
                builder = new PassengerPlaneBuilder();
                break;
            case WAR_PLANE:
                builder = new WarPlaneBuilder();
                break;
            default:
                throw new NoSuchPlaneTypeException("This type is not allowed");

        }
        return builder;
    }

}
