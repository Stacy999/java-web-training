package by.training.tolstikova.airline.service;

import by.training.tolstikova.airline.entity.Plane;
import by.training.tolstikova.airline.repository.Airline;


public class AirlineService {

    private Airline airline;

    public AirlineService(Airline airline) {
        this.airline = airline;
    }

    public int getAllCapacity() {
        int allCapacity = 0;
        for (Plane plane : airline.getAll()) {
            allCapacity += plane.getCapacity();
        }
        return allCapacity;
    }

    public int getAllCarryingCapacity() {
        int allCarryingCapacity = 0;
        for (Plane plane : airline.getAll()) {
            allCarryingCapacity += plane.getCarryingCapacity();
        }
        return allCarryingCapacity;
    }

    public Airline getAirline() {
        return airline;
    }
}
