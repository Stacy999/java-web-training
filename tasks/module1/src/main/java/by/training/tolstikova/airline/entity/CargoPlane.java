package by.training.tolstikova.airline.entity;

import java.util.Objects;

public class CargoPlane extends Plane {
    private static final String TYPE = "Cargo plane";
    private static final int CAPACITY = 1;
    private static final double CARRYING_CAPACITY = 8000;
    private static final double FUEL_CONSUMPTION = 11500;
    private static final String TYPE_OF_CARGO = "Cars";

    private String typeOfCargo;

    public CargoPlane() {
        super(CAPACITY, CARRYING_CAPACITY, FUEL_CONSUMPTION, TYPE);
        typeOfCargo = TYPE_OF_CARGO;
    }

    public CargoPlane(int capacity, double carryingCapacity, double fuelConsumption, String typeOfCargo) {
        super(capacity, carryingCapacity, fuelConsumption, TYPE);
        this.typeOfCargo = typeOfCargo;
    }

    public CargoPlane(CargoPlane cargoPlane) {
        super(cargoPlane.getCapacity(), cargoPlane.getCarryingCapacity(), cargoPlane.getFuelConsumption(), TYPE);
        this.typeOfCargo = cargoPlane.typeOfCargo;
    }

    public String getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(String typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CargoPlane)) return false;
        if (!super.equals(o)) return false;
        CargoPlane that = (CargoPlane) o;
        return Objects.equals(typeOfCargo, that.typeOfCargo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), typeOfCargo);
    }

    @Override
    public String toString() {
        return "CargoPlane{" + super.toString() +
                ", type of cargo='" + typeOfCargo + '\'' +
                '}';
    }

    class Builder {
        private String typeOfCargo;


    }
}
