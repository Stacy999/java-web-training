package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.Builder;
import by.training.tolstikova.airline.entity.PassengerPlane;

import java.util.Map;
import java.util.Set;

public class PassengerPlaneBuilder implements Builder {

    private static final String FLIGHT_RANGE = "Flight range";

    @Override
    public PassengerPlane createObject(Map<String, String> data) {
        PassengerPlane passengerPlane = new PassengerPlane();
        Set<String> keys = data.keySet();
        for (String key : keys) {
            switch (key) {
                case FLIGHT_RANGE:
                    passengerPlane.setFlightRange(data.get(key));
                    break;
                case CAPACITY:
                    passengerPlane.setCapacity(Integer.parseInt(data.get(key)));
                    break;
                case CARRYING_CAPACITY:
                    passengerPlane.setCarryingCapacity(Double.parseDouble(data.get(key)));
                    break;
                case FUEL_CONSUMPTION:
                    passengerPlane.setFuelConsumption(Double.parseDouble(data.get(key)));
                    break;
            }

        }
        return passengerPlane;
    }
}
