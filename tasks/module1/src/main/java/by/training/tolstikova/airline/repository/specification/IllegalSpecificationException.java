package by.training.tolstikova.airline.repository.specification;

public class IllegalSpecificationException extends Exception {

    public IllegalSpecificationException() {
    }

    public IllegalSpecificationException(final String message) {
        super(message);
    }

    public IllegalSpecificationException(final String message,
                                         final Throwable cause) {
        super(message, cause);
    }

    public IllegalSpecificationException(Throwable cause) {
        super(cause);
    }
}
