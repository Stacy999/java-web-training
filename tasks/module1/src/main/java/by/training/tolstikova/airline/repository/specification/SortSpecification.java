package by.training.tolstikova.airline.repository.specification;

import by.training.tolstikova.airline.entity.Plane;

import java.util.Comparator;

public interface SortSpecification extends Specification {
    Comparator<Plane> specifiedComparator();
}
