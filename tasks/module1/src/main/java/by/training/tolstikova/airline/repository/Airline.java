package by.training.tolstikova.airline.repository;

import by.training.tolstikova.airline.entity.Plane;
import by.training.tolstikova.airline.repository.specification.FindSpecification;
import by.training.tolstikova.airline.repository.specification.IllegalSpecificationException;
import by.training.tolstikova.airline.repository.specification.SortSpecification;
import by.training.tolstikova.airline.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Airline implements Repository<Plane> {

    private List<Plane> airline;

    public Airline() {
        airline = new ArrayList<>();
    }

    @Override
    public void add(Plane item) {
        airline.add(item);

    }

    @Override
    public void remove(Plane item) {

        for (Plane plane : airline) {
            if (plane.equals(item)) {
                airline.remove(plane);
            }
        }

    }

    @Override
    public List<Plane> query(Specification newSpecification)
            throws IllegalSpecificationException {
        List<Plane> result;
        if (newSpecification instanceof FindSpecification) {
            result = new ArrayList<>();
            for (Plane plane : airline) {
                if (((FindSpecification) newSpecification).find(plane)) {
                    result.add(plane);
                }
            }
        } else if (newSpecification instanceof SortSpecification) {
            result = new ArrayList<>(airline);
            result.sort(((SortSpecification) newSpecification)
                    .specifiedComparator());
        } else {
            throw new IllegalSpecificationException(
                    "This object is not an available specification");
        }
        return result;
    }

    @Override
    public int size() {
        return airline.size();
    }

    @Override
    public List<Plane> getAll() {
        return new ArrayList<>(airline);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Airline)) {
            return false;
        }
        Airline airline1 = (Airline) o;
        return Objects.equals(airline, airline1.airline);
    }

    @Override
    public int hashCode() {
        return Objects.hash(airline);
    }

    @Override
    public String toString() {
        return "Airline{" +
                "airline=" + airline +
                '}';
    }
}
