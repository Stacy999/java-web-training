package by.training.tolstikova.airline.controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DataReader {

    private static final Logger LOGGER = Logger.getLogger(DataReader.class);
    private String fileName;

    public DataReader() {
    }

    public DataReader(String fileName) {
        this.fileName = fileName;
    }

    public List<String> readLines() {
        List<String> receivedStrings = new ArrayList<>();
        try {
            receivedStrings = Files.readAllLines(Paths.get(fileName),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return receivedStrings;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
