package by.training.tolstikova.airline.validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ValidationResult {
    private boolean isValid;
    private Map<String, ValidationMessage> validationResultMap;

    public ValidationResult() {
        this.validationResultMap = new HashMap<>();
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Map<String, ValidationMessage> getValidationResultMap() {
        return validationResultMap;
    }

    public void setValidationResultMap(Map<String, ValidationMessage>
                                               validationResultMap) {
        this.validationResultMap = validationResultMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValidationResult)) return false;
        ValidationResult that = (ValidationResult) o;
        return isValid == that.isValid &&
                Objects.equals(validationResultMap, that.validationResultMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isValid, validationResultMap);
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "validationResultMap=" + validationResultMap +
                '}';
    }
}
