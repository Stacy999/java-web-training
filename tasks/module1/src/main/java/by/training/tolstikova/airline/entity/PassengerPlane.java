package by.training.tolstikova.airline.entity;

import java.util.Objects;

public class PassengerPlane extends Plane {
    private static final String TYPE = "Passenger plane";
    private static final int CAPACITY = 200;
    private static final double CARRYING_CAPACITY = 6000;
    private static final double FUEL_CONSUMPTION = 8000;
    private static final String FLIGHT_RANGE = "Short haul";

    private String flightRange;

    public PassengerPlane() {
        super(CAPACITY, CARRYING_CAPACITY, FUEL_CONSUMPTION, TYPE);
        this.flightRange = FLIGHT_RANGE;
    }

    public PassengerPlane(int capacity, double carryingCapacity, double fuelConsumption, String flightRange) {
        super(capacity, carryingCapacity, fuelConsumption, TYPE);
        this.flightRange = flightRange;
    }

    public PassengerPlane(PassengerPlane passengerPlane) {
        super(passengerPlane.getCapacity(), passengerPlane.getCarryingCapacity(),
                passengerPlane.getFuelConsumption(), TYPE);
        this.flightRange = passengerPlane.flightRange;
    }

    public String getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(String flightRange) {
        this.flightRange = flightRange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PassengerPlane)) return false;
        if (!super.equals(o)) return false;
        PassengerPlane that = (PassengerPlane) o;
        return Objects.equals(flightRange, that.flightRange);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), flightRange);
    }

    @Override
    public String toString() {
        return "PassengerPlane{" + super.toString() +
                ", flight range='" + flightRange + '\'' +
                '}';
    }
}


