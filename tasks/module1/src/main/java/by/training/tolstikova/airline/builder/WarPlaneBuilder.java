package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.Builder;
import by.training.tolstikova.airline.entity.WarPlane;

import java.util.Map;
import java.util.Set;

public class WarPlaneBuilder implements Builder {
    private static final String WEAPON = "Weapon";

    @Override
    public WarPlane createObject(Map<String, String> data) {
        WarPlane warPlane = new WarPlane();
        Set<String> keys = data.keySet();
        for (String key : keys) {
            switch (key) {
                case WEAPON:
                    warPlane.setWeapon(data.get(key));
                    break;
                case CAPACITY:
                    warPlane.setCapacity(Integer.parseInt(data.get(key)));
                    break;
                case CARRYING_CAPACITY:
                    warPlane.setCarryingCapacity(Double.parseDouble(data.get(key)));
                    break;
                case FUEL_CONSUMPTION:
                    warPlane.setFuelConsumption(Double.parseDouble(data.get(key)));
                    break;
            }

        }
        return warPlane;
    }

}
