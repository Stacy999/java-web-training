package by.training.tolstikova.airline.repository;

import by.training.tolstikova.airline.repository.specification.IllegalSpecificationException;
import by.training.tolstikova.airline.repository.specification.Specification;

import java.util.List;

public interface Repository<T> {

    void add(T item);

    void remove(T item);

    List<T> query(Specification newSpecification) throws IllegalSpecificationException;

    int size();

    List<T> getAll();

}
