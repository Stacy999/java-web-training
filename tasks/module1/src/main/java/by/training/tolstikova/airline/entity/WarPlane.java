package by.training.tolstikova.airline.entity;

import java.util.Objects;

public class WarPlane extends Plane {
    private static final String TYPE = "War plane";
    private static final String WEAPON = "no weapon";
    private static final int CAPACITY = 1;
    private static final double CARRYING_CAPACITY = 8000;
    private static final double FUEL_CONSUMPTION = 11500;

    private String weapon;

    public WarPlane() {
        super(CAPACITY, CARRYING_CAPACITY, FUEL_CONSUMPTION, TYPE);
        this.weapon = WEAPON;
    }

    public WarPlane(int capacity, double carryingCapacity, double fuelConsumption, String weapon) {
        super(capacity, carryingCapacity, fuelConsumption, TYPE);
        this.weapon = weapon;
    }

    public WarPlane(WarPlane warPlain) {
        super(warPlain.getCapacity(), warPlain.getCarryingCapacity(), warPlain.getFuelConsumption(), TYPE);
        this.weapon = warPlain.weapon;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WarPlane)) return false;
        if (!super.equals(o)) return false;
        WarPlane warPlane = (WarPlane) o;
        return Objects.equals(weapon, warPlane.weapon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weapon);
    }

    @Override
    public String toString() {
        return "WarPlane{" + super.toString() +
                ", weapon='" + weapon + '\'' +
                '}';
    }
}
