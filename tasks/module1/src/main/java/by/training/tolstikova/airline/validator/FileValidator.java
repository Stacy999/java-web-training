package by.training.tolstikova.airline.validator;

import java.io.File;

public class FileValidator {

    public ValidationResult validateFile(String path) {
        ValidationResult validationResult = new ValidationResult();
        File file = new File(path);

        if (file.isFile()) {
            validationResult.setValid(true);
        } else {
            ValidationMessage validationMessage = new ValidationMessage();
            validationMessage.addMessage("Path to file is invalid");
            validationResult.setValid(false);
            validationResult.getValidationResultMap().put("FILE ERROR: ", validationMessage);
        }
        return validationResult;
    }
}
