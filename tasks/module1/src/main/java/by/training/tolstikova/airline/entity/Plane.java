package by.training.tolstikova.airline.entity;

import java.util.Objects;

public abstract class Plane {

    private String type;
    private int capacity;
    private double carryingCapacity;
    private double fuelConsumption;

    public Plane(int capacity, double carryingCapacity, double fuelConsumption, String type) {
        this.capacity = capacity;
        this.carryingCapacity = carryingCapacity;
        this.fuelConsumption = fuelConsumption;
        this.type = type;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(double carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuel_consumption) {
        this.fuelConsumption = fuel_consumption;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plane)) return false;
        Plane plane = (Plane) o;
        return capacity == plane.capacity &&
                Double.compare(plane.carryingCapacity, carryingCapacity) == 0 &&
                Double.compare(plane.fuelConsumption, fuelConsumption) == 0 &&
                Objects.equals(type, plane.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, capacity, carryingCapacity, fuelConsumption);
    }

    @Override
    public String toString() {
        return "Plane{" +
                "type='" + type + '\'' +
                ", capacity=" + capacity +
                ", carryingCapacity=" + carryingCapacity +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
