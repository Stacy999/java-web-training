package by.training.tolstikova.airline.repository.specification;

import by.training.tolstikova.airline.entity.Plane;

public class FindPlanesByFuelConsumptionSpecification implements FindSpecification {

    private int minFuelConsumption;
    private int maxFuelConsumption;

    public FindPlanesByFuelConsumptionSpecification(int minFuelConsumption, int maxFuelConsumption) {
        this.minFuelConsumption = minFuelConsumption;
        this.maxFuelConsumption = maxFuelConsumption;
    }

    @Override
    public boolean find(Plane plane) {
        return plane.getFuelConsumption() >= minFuelConsumption &&
                plane.getFuelConsumption() <= maxFuelConsumption;
    }
}
