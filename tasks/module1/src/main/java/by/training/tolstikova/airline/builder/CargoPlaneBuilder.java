package by.training.tolstikova.airline.builder;

import by.training.tolstikova.airline.builder.Builder;
import by.training.tolstikova.airline.entity.CargoPlane;

import java.util.Map;
import java.util.Set;

public class CargoPlaneBuilder implements Builder {
    private static final String TYPE_OF_CARGO = "Type of cargo";

    @Override
    public CargoPlane createObject(Map<String, String> data) {
        CargoPlane cargoPlane = new CargoPlane();
        Set<String> keys = data.keySet();
        for (String key : keys) {
            switch (key) {
                case TYPE_OF_CARGO:
                    cargoPlane.setTypeOfCargo(data.get(key));
                    break;
                case CAPACITY:
                    cargoPlane.setCapacity(Integer.parseInt(data.get(key)));
                    break;
                case CARRYING_CAPACITY:
                    cargoPlane.setCarryingCapacity(Double.parseDouble(data.get(key)));
                    break;
                case FUEL_CONSUMPTION:
                    cargoPlane.setFuelConsumption(Double.parseDouble(data.get(key)));
                    break;
            }

        }
        return cargoPlane;
    }
}
