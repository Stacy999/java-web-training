package by.training.tolstikova.airline.parser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LineParser {
    private static final String FIRST_SEPARATOR = ",";
    private static final String SECOND_SEPARATOR = ":";

    private List<String> buffer;

    private void parseLine(String line) {
        buffer = Arrays.asList(line.split(FIRST_SEPARATOR));
    }

    public Map<String, String> getData(String line) {
        Map<String, String> result = new HashMap<>();
        if (line != null) {
            String[] parsedString;
            parseLine(line);
            for (String str : buffer) {
                parsedString = str.split(SECOND_SEPARATOR);
                if (parsedString.length > 1) {
                    result.put(parsedString[0], parsedString[1]);
                }
            }
        }
        return result;
    }
}
