package by.training.tolstikova.airline.controller;

import by.training.tolstikova.airline.builder.Builder;
import by.training.tolstikova.airline.builder.BuilderFactory;
import by.training.tolstikova.airline.builder.NoSuchPlaneTypeException;
import by.training.tolstikova.airline.parser.LineParser;
import by.training.tolstikova.airline.service.AirlineService;
import by.training.tolstikova.airline.validator.FileValidator;
import by.training.tolstikova.airline.validator.PlaneValidator;
import by.training.tolstikova.airline.validator.ValidationResult;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;


public class AirlineCreationController {

    private static final Logger LOGGER = Logger.getLogger(AirlineCreationController.class);

    private FileValidator fileValidator;
    private PlaneValidator planeValidator;
    private LineParser lineParser;
    private DataReader dataReader;
    private BuilderFactory builderFactory;
    private AirlineService airlineService;

    public AirlineCreationController(
            FileValidator fileValidator, LineParser lineParser, DataReader dataReader, PlaneValidator planeValidator,
            BuilderFactory builderFactory, AirlineService airlineService) {
        this.fileValidator = fileValidator;
        this.lineParser = lineParser;
        this.dataReader = dataReader;
        this.planeValidator = planeValidator;
        this.builderFactory = builderFactory;
        this.airlineService = airlineService;
    }

    public void controlAirlineCreation(String path) {
        ValidationResult validationResult = fileValidator.validateFile(path);

        if (validationResult.isValid()) {
            dataReader = new DataReader(path);
            List<String> text = dataReader.readLines();
            for (String line : text) {
                Map<String, String> parserResult = lineParser.getData(line);
                validationResult = planeValidator.validatePlane(parserResult);
                if (validationResult.isValid()) {
                    try {
                        Builder builder = builderFactory.getBuilder(parserResult.get("Type"));
                        airlineService.getAirline().add(builder.createObject(parserResult));
                    } catch (NoSuchPlaneTypeException e) {
                        LOGGER.error(e.getMessage());
                    }
                }else {
                    LOGGER.error(validationResult.getValidationResultMap().toString());
                }
            }
            LOGGER.info("Objects created: " + airlineService.getAirline().getAll().toString());
        }
    }
}
