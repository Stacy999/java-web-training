package by.training.tolstikova.airline.repository.specification;

import by.training.tolstikova.airline.entity.Plane;

import java.util.Comparator;

public class SortByCapacityThenByCarryingCapacitySpecification implements SortSpecification {
    @Override
    public Comparator<Plane> specifiedComparator() {
        return Comparator.comparingInt(Plane::getCapacity).thenComparing(Plane::getCarryingCapacity);
    }
}
